import {createDrawerNavigator,createStackNavigator } from 'react-navigation'
import Home from "../containers/Home/Home"
import Slider from "../containers/Slider/Slider"
import Reservation from "../containers/Reservation/Reservation"
import Mesreservations from "../containers/Reservation/Mesreservations/Mesreservations"
import ReservationOption1 from "../containers/Reservation/ReservationOption1/ReservationOption1"
import ReservationOption2 from "../containers/Reservation/ReservationOption2/ReservationOption2"
import MonProfil from "../containers/MonProfil/MonProfil"
import ModifierProfil from "../containers/MonProfil/ModifierProfil/ModifierProfil"
import Parrainage from "../containers/Parrainage/Parrainage"
import Paiement from "../containers/Paiement/Paiement"
import ContactezNous from "../containers/ContactezNous/ContactezNous"
import Apropos from "../containers/Apropos/Apropos"
import Lieux from "../containers/Reservation/Lieux/Lieux"
import VotreCourse from "../containers/Reservation/VotreCourse/VotreCourse"
import DimScreen from "../configs/DimScreen"
import PaiementChoice from "../containers/Reservation/PaiementChoice/PaiementChoice"


const RootStackConnectNavigation = createStackNavigator(
{
    ReservationPage: {screen: Reservation},
    PaiementChoice: {screen: PaiementChoice},
    MesreservationsPage:{screen:Mesreservations},
    ReservationOption1Page:{screen:ReservationOption1},
    ReservationOption2Page:{screen:ReservationOption2},
    HomePage:{screen:Home},
    MonProfilPage: {screen: MonProfil},
    ModifierProfilPage:{screen:ModifierProfil},
    ParrainagePage:{screen:Parrainage},
    ContactezNousPage:{screen:ContactezNous},
    AproposPage:{screen:Apropos},
    PaiementPage:{screen:Paiement},
    LieuxPage:{screen:Lieux},
    VotreCoursePage:{screen:VotreCourse}
})


const RootConnecteStackNav =createDrawerNavigator(
{
    RootStackConnectNavigation: {screen: RootStackConnectNavigation}, 
},
{
    contentComponent: Slider,
    drawerWidth: DimScreen.widthScreen*0.75,   
})
export default RootConnecteStackNav