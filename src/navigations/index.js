import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import Authloading from "../containers/Authloading/Authloading"
//import Auth from "../navigations/AuthanticatedNav"
import RootStackNav from "../navigations/RootStackNav"
import RootConnecteStackNav from "../navigations/RootConnecteStackNav"
const AppNavigator = createSwitchNavigator(
    {
        Authloading: Authloading,
     //   Auth: Auth,
        RootConnecteApp:RootConnecteStackNav,
        RootNav:RootStackNav
    },
    {
      initialRouteName: 'Authloading',
    }
)
export default AppNavigator