import { createStackNavigator } from "react-navigation";
import Login from "../containers/Login/Login"
import Inscription from "../containers/Inscription/Inscription"
const AuthanticatedNav = createStackNavigator
(
    {
        Login: {screen: Login},
        Inscription:{screen:Inscription},
    },
    {
        initialRouteName: "Login",
        navigationOptions: {
            header: null,
        }
    }
);
export default AuthanticatedNav