import {createDrawerNavigator,createStackNavigator } from 'react-navigation'
import Home from "../containers/Home/Home"
import Slider from "../containers/Slider/Slider"
import Login from "../containers/Login/Login"
import Inscription from "../containers/Inscription/Inscription"
import MotdepassOublier from "../containers/MotdepassOublier/MotdepassOublier"
import Reservation from "../containers/Reservation/Reservation"
import Mesreservations from "../containers/Reservation/Mesreservations/Mesreservations"
import ReservationOption1 from "../containers/Reservation/ReservationOption1/ReservationOption1"
import ReservationOption2 from "../containers/Reservation/ReservationOption2/ReservationOption2"

import Parrainage from "../containers/Parrainage/Parrainage"
import Paiement from "../containers/Paiement/Paiement"
import ContactezNous from "../containers/ContactezNous/ContactezNous"
import Apropos from "../containers/Apropos/Apropos"
import Lieux from "../containers/Reservation/Lieux/Lieux"
import VotreCourse from "../containers/Reservation/VotreCourse/VotreCourse"
import DimScreen from "../configs/DimScreen"


const RootStackNavigation = createStackNavigator(
{
    ReservationPage: {screen: Reservation},
    MesreservationsPage:{screen:Mesreservations},
    ReservationOption1Page:{screen:ReservationOption1},
    ReservationOption2Page:{screen:ReservationOption2},
    LoginPage: {screen: Login},
    MotdepassOublierPage: {screen: MotdepassOublier},
    InscriptionPage:{screen:Inscription},
    HomePage:{screen:Home},
    ParrainagePage:{screen:Parrainage},
    ContactezNousPage:{screen:ContactezNous},
    AproposPage:{screen:Apropos},
    PaiementPage:{screen:Paiement},
    LieuxPage:{screen:Lieux},
    VotreCoursePage:{screen:VotreCourse}
})


const RootStackNav =createDrawerNavigator(
{
    RootStackNavigation: {screen: RootStackNavigation}, 
},
{
    contentComponent: Slider,
    drawerWidth: DimScreen.widthScreen*0.75,   
})
export default RootStackNav