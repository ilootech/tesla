import React, { Component } from 'react'
import { View, Text, ImageBackground, TouchableOpacity } from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image'
import styles from "./styles"
import DimScreen from "../../configs/DimScreen"
import colors from '../../configs/colors'

class HeaderAuthen extends Component {

    render() {
        return (
            <View style={styles.container}>
                <ImageBackground
                    style={[styles.fondHeader, { flexDirection: "column" }]}
                    //    source={require("../../assets/images/Authent/imgfondAuthen.png")}>
                    source={require("../../assets/images/Authent/bgSlider.png")}>
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <TouchableOpacity onPress={() => { this.props._clickShowSLider() }}>
                            <AutoHeightImage
                                source={require('../../assets/images/Slider/imgMenueShow.png')}
                                width={DimScreen.widthScreen * 0.062}
                                style={styles.btnShowSlider}
                            />
                        </TouchableOpacity>
                        {/* <View></View> */}
                        <AutoHeightImage
                            style={{ marginLeft: -DimScreen.widthScreen * 0.062, marginTop:10 }}
                            width={DimScreen.widthScreen * 0.35}
                            source={require("../../assets/icones/logoSNTblanc.png")}
                        />
                        <View></View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-around", margin:10 }}>
                        <Text style={{ color: colors.white, fontSize: 14 }}>« Économisez la planète à 2€ le km avec SNT » </Text>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}
export default HeaderAuthen