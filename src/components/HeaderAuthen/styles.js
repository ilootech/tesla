import { StyleSheet } from 'react-native';
import DimScreen from "../../configs/DimScreen"
export default styles = StyleSheet.create({
    container: {
        height:DimScreen.heightScreen*0.3,
        width:"100%", 
    },
    fondHeader:
    {
        flex:1
        
    },
    btnShowSlider:
    {
        marginLeft:DimScreen.widthScreen*0.025,
        marginTop:DimScreen.widthScreen*0.03,
    }
})