import React from 'react';
import {StyleSheet,View,Text,ActivityIndicator} from 'react-native';
class Loading extends React.Component
{
    render ()
    {
        return (
        <View style={styles.loadingcontainer}>
                <ActivityIndicator size='large' color="#aaaaaa"/>
         </View>);
    }
}
const styles=StyleSheet.create(
{
      loadingcontainer:
      {
        position:'absolute',
        left:0,
        right:0,
        top:0,
        bottom:0,
        alignItems:'center',
        justifyContent:'center',
        zIndex:1000,
        backgroundColor:'rgba(0,0,0,0.6)'
      }
  })
  export default Loading;
