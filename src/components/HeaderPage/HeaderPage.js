import React, {Component}  from 'react'
import {View,Text,ImageBackground,TouchableOpacity} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image'
import styles from "./styles"
import  DimScreen from "../../configs/DimScreen"

class HeaderPage extends Component 
{

    render() 
    {
        return (
            <View style={styles.containerHeader}>
            <ImageBackground
                        style={styles.fondHeader}
                        source={require("../../assets/images/fond/bgFondHeader.png")}>
                            <View style={styles.header}>
                                <TouchableOpacity onPress={()=>{this.props._clickShowSLider()}}>
                                    <AutoHeightImage
                                        source={require('../../assets/images/Slider/imgMenueShow.png')}
                                        width={DimScreen.widthScreen * 0.062} 
                                        style={styles.btnShowSlider}
                                        />
                                </TouchableOpacity>
                            </View>
             </ImageBackground>
        </View>);
        }
}
export default HeaderPage