import settings from '../../configs/settings';
const ReservationURLs =
{
    listeTypesVoituresURL: settings.API_BASE_URL + 'ws-get-list-type-voiture/',
    reservationURL: settings.API_BASE_URL + 'ws-insert-reservation/',
    getListReservationURL: settings.API_BASE_URL + "ws-get-list-reservation",
    testUtilisateurURL: settings.API_BASE_URL + "ws-user-is-profil-completed/",
    getListeAdresseMapUrl: "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=" + settings.API_KEY_GOOGPLEMAP,
    getSearchLatLong_PlaceUrl: key = "https://maps.googleapis.com/maps/api/place/details/json?key=" + settings.API_KEY_GOOGPLEMAP,
    getDistanceURL: "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric",
    paiementInsertURL: settings.API_BASE_URL + "ws-paiement-insert/",
    getListReservationTerminerURL: settings.API_BASE_URL + "ws-get-list-reservation-finished/",
    getAPIStripeURL: settings.API_BASE_URL + "ws-get-stripe-api/",
    paiementInsertStripeURL: settings.API_BASE_URL + "ws-pay-stripe-card/",
    deleteReservationURL: settings.API_BASE_URL + "ws-delete-reservation/",
    getAPIPaypalURL: settings.API_BASE_URL + "ws-get-paypal-api/"

}
export default ReservationURLs