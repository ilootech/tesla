import settings from '../../configs/settings';
const UserURLs = 
{
    createAccountURL: settings.API_BASE_URL + 'ws-inscription/',
    updateAccountURL: settings.API_BASE_URL + 'ws-edition-profile-js-post/',
    connectLoginURL:settings.API_BASE_URL + 'ws-connexion-js-post/',
    mot2passOUblierURL:settings.API_BASE_URL +  "ws-lost-password/",
    updateProfilURL:settings.API_BASE_URL+"ws-edit-avatar/"
}
export default UserURLs