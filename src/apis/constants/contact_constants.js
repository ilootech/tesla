import settings from '../../configs/settings';
const ContactURLs = 
{
    semdMsgContactURL: settings.API_BASE_URL + 'ws-contact-us/'
}
export default ContactURLs