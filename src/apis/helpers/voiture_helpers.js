// import voiture_constants from '../constants/voiture_constants'
import VoitureURLs from '../constants/voiture_constants'
const VoitureHelpers =
{
    /*
    Get List Voiture
    */
    getListVoiture: async () => {
        let response = await fetch(VoitureURLs.getVoitureURL,
            {
                method: "GET",
                headers: {
                    "Accept": 'application/json',
                    'Content-Type': 'application/json',
                },
                // body: params
            }).then((response) => response.json())
        return response
    },
}
export default VoitureHelpers;