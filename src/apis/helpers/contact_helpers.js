import ContactURLs from '../constants/contact_constants'
const ContactHelpers = 
{
    /*
    Send Contact
    */
    sendContactHelpers: async ( email,objet,message) => 
    {

        let params= JSON.stringify({
            email:email,
            objet:objet,
            message:message
        })
        console.log("params::"+JSON.stringify(params))
        console.log("LIEN::"+ContactURLs.semdMsgContactURL)
        let response = await fetch(ContactURLs.semdMsgContactURL,
            {
                method: "POST",
                headers: {
                    "Accept": 'application/json',
                    'Content-Type': 'application/json',
                },
                body:params
            }).then((response) => response.json())
        return response
    },
}
export default ContactHelpers