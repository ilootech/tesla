import ReservationURLs from '../constants/reservation_constants'
import settings from '../../configs/settings'
const ReservationHelpers =
{
    /*
    Ws liste Type de voiture
    */
    listeTypesVoituresHelpers: async () => {
        let response = await fetch(ReservationURLs.listeTypesVoituresURL).
            then((response) => response.json())
        return response
    },
    /*
    Liste d'adresse disponilble
    */
    getListeAdresseMap: async (textAdresse) => {
        let param = "&input=" + textAdresse
        let URL = ReservationURLs.getListeAdresseMapUrl + param //+ "&key=AIzaSyB8df0j523DwrYKshz6ZolvO6MRW9KBfSg"//AIzaSyB8df0j523DwrYKshz6ZolvO6MRW9KBfSg"
        let response = await fetch(URL, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
        return response.json()
    },

    /**
     * Recuperation Longitude et latitude à l'aide du place id
     * */
    getSearchLatLong_Place: async (placeID) => {
        let param = "&place_id=" + placeID
        let URL = ReservationURLs.getSearchLatLong_PlaceUrl + param //AIzaSyB8df0j523DwrYKshz6ZolvO6MRW9KBfSg"
        console.log("URL place ID" + URL)
        let response = await fetch(URL)
        return response.json()
    },
    // https://maps.googleapis.com/maps/api/place/details/json?place_id=ChIJLTyNDYxv5kcRbtc9TZBbD_o&key=AIzaSyDD4YxTjvFh0Xiry6oH3TO5kvNnUFYnDOo

    setReservation: async (idUser, dateDepart, heure_minDepart, latDepart, longDepart, depart, arrivee, latArriver, longArriver, prix) => {
        let params = JSON.stringify({
            idUser: idUser,
            dateDepart: dateDepart,
            heure_minDepart: heure_minDepart,
            latDepart: latDepart,
            longDepart: longDepart,
            depart: depart,
            arrivee: arrivee,
            latArriver: latArriver,
            longArriver: longArriver,
            prix: prix,
            // type_voiture_id: type_voiture_id
        })
        let URL = ReservationURLs.reservationURL
        let response = await fetch(URL, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: params
        })
        return response.json()
    },
    getListReservation: async (idUser) => {
        let params = JSON.stringify({
            idUser: idUser
        })
        let URL = ReservationURLs.getListReservationURL
        let response = await fetch(URL, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: params
        })
        return response.json()
    },
    getListReservationTerminer: async (idUser) => {
        let params = JSON.stringify({
            idUser: idUser
        })
        let URL = ReservationURLs.getListReservationTerminerURL
        let response = await fetch(URL, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: params
        })
        return response.json()
    },
    testUtilisateur: async (idUser) => {
        let params = JSON.stringify({
            user_id: idUser
        })
        let URL = ReservationURLs.testUtilisateurURL
        let response = await fetch(URL, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: params
        })
        return response.json()
    },
    getDistance: async (origin, destination) => {
        let URL = ReservationURLs.getDistanceURL + "&origins=" + origin + "&destinations=" + destination + "&key=" + settings.API_KEY_GOOGPLEMAP
        let response = await fetch(URL, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            // body:params
        })
        return response.json()
    },
    paiementInsert: async (user_id, type, amount, reservation_id, datas) => {
        let params = JSON.stringify({
            user_id: user_id,
            type: type,
            amount: amount,
            reservation_id: reservation_id,
            datas: datas
        })
        let URL = ReservationURLs.paiementInsertURL
        let response = await fetch(URL, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: params
        })
        return response.json()
    },
    paiementInsertStripe: async (tokenId, user_id, reservation_id, amount) => {
        let params = JSON.stringify({
            tokenId: tokenId,
            reservation_id: reservation_id,
            user_id: user_id,
            amount: amount,
        })
        let URL = ReservationURLs.paiementInsertStripeURL
        let response = await fetch(URL, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: params
        })
        return response.json()
    },
    getAPIStripe: async () => {
        let URL = ReservationURLs.getAPIStripeURL
        let response = await fetch(URL, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            // body:params
        })
        return response.json()
    },
    deleteReservation:async(user_id,reservation_id)=>{
        let params = JSON.stringify({
            user_id: user_id,
            reservation_id: reservation_id
        })
        let URL = ReservationURLs.deleteReservationURL
        let response = await fetch(URL, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: params
        })
        return response.json()
    },
    getAPIPaypal: async () => {
        let URL = ReservationURLs.getAPIPaypalURL
        let response = await fetch(URL, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            // body:params
        })
        return response.json()
    }
}
export default ReservationHelpers