import UserURLs from '../constants/user_constants'
const UserHelpers =
{
    /*
    Inscription Page
    */
    createAccountHelpers: async (type_user, first_name, last_name, phone, email, password, siren) => {

        let params = JSON.stringify({
            type_user: type_user,
            last_name: last_name,
            first_name: first_name,
            phone: phone,
            email: email,
            password: password,
            siren: siren
        })
        console.log("params::" + JSON.stringify(params))
        console.log("LIEN::" + UserURLs.createAccountURL)
        let response = await fetch(UserURLs.createAccountURL,
            {
                method: "POST",
                headers: {
                    "Accept": 'application/json',
                    'Content-Type': 'application/json',
                },
                body: params
            }).then((response) => response.json())
        return response
    },

    /*
    Inscription Page
    */
    updateAccountHelpers: async (id_User, first_name, last_name, email, tel, new_password, confirmation_password) => {

        let params = JSON.stringify({
            id_User: id_User,
            last_name: last_name,
            first_name: first_name,
            email: email,
            tel: tel,
            new_password: new_password,
            old_password: confirmation_password

        })
        console.log("params::" + JSON.stringify(params))
        console.log("LIEN::" + UserURLs.updateAccountURL)
        let response = await fetch(UserURLs.updateAccountURL,
            {
                method: "POST",
                headers: {
                    "Accept": 'application/json',
                    'Content-Type': 'application/json',
                },
                body: params
            }).then((response) => response.json())
        return response
    },
    /*
    *LOGIN CONNECT
    */
    connectLoginHelpers: async (email, password) => {

        let params = JSON.stringify({
            email: email,
            password: password
        })
        console.log("params::" + JSON.stringify(params))
        console.log("LIEN::" + UserURLs.connectLoginURL)
        let response = await fetch(UserURLs.connectLoginURL,
            {
                method: "POST",
                headers: {
                    "Accept": 'application/json',
                    'Content-Type': 'application/json',
                },
                body: params
            }).then((response) => response.json())
        return response
    },
    /*
    *MOT DE PASSE OUBLIER
    */
    mot2passOUblierHelpers: async (email) => {

        let params = JSON.stringify({
            email: email,
        })
        console.log("params::" + JSON.stringify(params))
        console.log("LIEN::" + UserURLs.mot2passOUblierURL)
        let response = await fetch(UserURLs.mot2passOUblierURL,
            {
                method: "POST",
                headers: {
                    "Accept": 'application/json',
                    'Content-Type': 'application/json',
                },
                body: params
            }).then((response) => response.json())
        return response
    },
    /**
  * Change profil user
  * 
  */
    changeProfilUserHelpers: async (user_id, response) => {
        // const params = new FormData();
        // params.append('user_id', user_id)
        // params.append('file-avatar', {
        //     uri: response.uri,
        //     type: response.type,
        //     name: response.fileName
        // })
        const params = JSON.stringify({
            user_id: user_id,
            image_64: response.data
        })
        console.log("URL:" + UserURLs.updateProfilURL)
        let rps = await fetch(UserURLs.updateProfilURL,
            {
                method: 'POST',
                headers: {
                    "Accept": 'application/json',
                    'Content-Type': 'application/json',
                },
                body: params
            })
        return rps.json()
    },

}
export default UserHelpers