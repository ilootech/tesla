export default colors = 
{
    white:"#ffffff",
    gris1Font:"#606060",
    vertfonce:"#008644",
    transparent:"rgba(255,255,255,0)",
    vertBorderColor:"#005c4c",
    colorBtnStart:"#008544",
    colorBtnEnd:"#005c4c",
    colorFooterResaStart:"rgba(0,133,68,0.5)",
    colorFooterResaEnd:"rgba(0,92,75,0.5)",
    black:"#000000",
    transparent:"rgba(0,0,0,0)",
    vertTransparent:"rgba(0,113,72,0.8)"
}