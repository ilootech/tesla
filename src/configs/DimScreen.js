import { Dimensions} from 'react-native'
const win = Dimensions.get('window')
export default DimScreen = {
    widthScreen: win.width<win.height?win.width:win.height,
    heightScreen:win.width<win.height?win.height:win.width,
}