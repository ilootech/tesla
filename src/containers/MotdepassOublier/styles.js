import { StyleSheet } from 'react-native';
import DimScreen from "../../configs/DimScreen"
import colors from "../../configs/colors"
export default styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    scrollAuthloading:
    {
        justifyContent:"center",
        width:"100%",
    },
    bodyLogin:
    {
        backgroundColor:colors.white,
        width:"90%",
        position:"relative",
        marginTop:-DimScreen.heightScreen*0.08,
        marginBottom:DimScreen.heightScreen*0.03,
        marginLeft:DimScreen.widthScreen*0.1,
        paddingTop:DimScreen.heightScreen*0.06,
        paddingBottom:DimScreen.heightScreen*0.07,
        paddingLeft:DimScreen.widthScreen*0.05,
        paddingRight:DimScreen.widthScreen*0.05,
        marginRight:DimScreen.widthScreen*0.1,
        flex:1,
        borderWidth: 0,
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 4,
        alignItems:"center",
    },
    textConnectezVous:
    {
        fontFamily:"Lato-Black",
        color:colors.gris1Font,
        fontSize:DimScreen.widthScreen*0.06,
    },
    labelConnect:
    {
        color:colors.vertfonce,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Black",
    },
    inputPassword:
    {
        flexDirection:"row",
        alignItems:"center",
        marginRight:DimScreen.widthScreen*0.01,
    },
    inputText:
    {
        height:DimScreen.heightScreen*0.05,
        color:colors.gris1Font,
        fontSize:DimScreen.widthScreen*0.04,
        padding:0,
        flex:1,
        fontFamily:"Lato-Medium",
    },
    viewInputLogin:
    {
        marginTop:DimScreen.heightScreen*0.045,
        marginBottom:DimScreen.heightScreen*0.06,
        width:"100%",
        alignItems:"flex-start",
        borderBottomColor:colors.vertBorderColor,
        borderBottomWidth:DimScreen.widthScreen*0.003
    },
    viewInputPassword:
    {
        marginTop:DimScreen.heightScreen*0.05,
        width:"100%",
        alignItems:"flex-start",
        borderBottomColor:colors.vertBorderColor,
        borderBottomWidth:DimScreen.widthScreen*0.003
    },
    viewPassWordOublier:
    {
        width:"100%",
        alignItems:"flex-end",
        marginTop:DimScreen.heightScreen*0.009,
        marginBottom:DimScreen.heightScreen*0.07,
    },
    textPassWordOublier:
    {
        fontSize:DimScreen.widthScreen*0.038,
        fontFamily:"Lato-Medium",
        color:colors.gris1Font,
    },
    panelBarrOU:
    {
        flexDirection:"row",
        alignItems:"center",
        marginBottom:DimScreen.heightScreen*0.013,
        marginTop:DimScreen.heightScreen*0.013,
    },
    barrOU:
    {
        width:"30%",
        marginHorizontal:"2%",
        height:DimScreen.widthScreen*0.002,
        backgroundColor:colors.gris1Font,
    },
    textOU:
    {
        marginHorizontal:"2%",
        fontFamily:"Lato-Regular",   
        fontSize:DimScreen.widthScreen*0.03,
    },
    btnLogin:
    {
        width:"76%",
        height:DimScreen.heightScreen*0.06,
    },
    linearGradienLogin:
    {
        width:"100%",
        alignItems:"center",
        justifyContent:"center",
        height:"100%",
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 2,
    },
    textbtnLogin:
    {
        color:colors.white,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Bold",
    }
  

})