import React, {Component,Fragment}  from 'react'
import {
    Alert,
    SafeAreaView,
    Keyboard,
     StatusBar,View,Text,ScrollView,KeyboardAvoidingView,
     ActivityIndicator,
    TouchableOpacity,TextInput} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image';
import  LinearGradient from 'react-native-linear-gradient'
import NetInfo from "@react-native-community/netinfo"
import colors from "../../configs/colors"


import Loading from "../../components/Loading/Loading"
import constantMsg from  "../../configs/constantMsg"
import UserHelpers from "../../apis/helpers/user_helpers"
import HeaderAuthen from "../../components/HeaderAuthen/HeaderAuthen"
import styles from "./styles"
import DimScreen from '../../configs/DimScreen';
class MotdepassOublier extends Component 
{
    static navigationOptions = {
        header: null,
    };
    constructor(props){
        super(props)
        this.state={
            isloading:false,
            email:"",
            password:"",
        }
    }
    /** Show Slider barre */
    _clickShowSLider=()=>
    {
        this.props.navigation.openDrawer()  
    }

    
     /** 
     *Test Adresse email Valide
    **/
    _testEmail=async(value)=>
    {
        if (value.length <= 255 && /^[a-z0-9][a-z0-9-_\.]+@([a-z]|[a-z0-9]?[a-z0-9-]+[a-z0-9])\.[a-z0-9]{2,10}(?:\.[a-z]{2,10})?$/.test(value)) 
        {
            return true
        }else
        {
            return false
        }
    }
    /*
    CLick Button Mot de passe Oublier
    */ 
   _clickLInitPassWOrd=async()=>
   {
        Keyboard.dismiss()
       NetInfo.isConnected.fetch().then(async (isConnected) => 
       {
           if(isConnected)
           {
               if(this.state.email=="" )
               {
                   await Alert.alert(
                       constantMsg.app_name,
                       constantMsg.msg_entrerEmail,
                       [
                           {
                               text: "Ok",
                               onPress:async () =>
                               {
                                   this.refs.emailInput.focus()
                               }
                           }
                       ])
               }
               else if(!(this.state.email.length <= 255 && /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.email)))
               {
                   await  Alert.alert(
                       constantMsg.app_name,
                      constantMsg.msg_alertEmailInvalid,
                      [
                          {
                              text: "Ok",
                              onPress: () => 
                              {
                                  this.refs.emailInput.focus()
                              }
                          }
                      ]
                  )

               }
               else
               {
                  this.setState({isloading:true})
                   await UserHelpers.mot2passOUblierHelpers(this.state.email)
                   .then(async(data)=>
                   {
                        if(data.status==="ok")
                        { 
                            this.setState({isloading:false})
                            await  Alert.alert(
                                constantMsg.app_name,
                                constantMsg.msg_verifsucceReinitialiserPassword,
                                [
                                    {
                                        text: "Ok",
                                        onPress: () => 
                                        {

                                        }
                                    }
                                ]
                            )
                           
                            this.props.navigation.pop()
                        }else
                        {
                            this.setState({isloading:false})
                             await  Alert.alert(
                                constantMsg.app_name,
                                constantMsg.msg_EmailInvalid,
                                [
                                    {
                                        text: "Ok",
                                        onPress: () => 
                                        {
                                            this.refs.emailInput.focus()
                                        }
                                    }
                                ]
                            )
                        }
                   })
               } 
           }else
           {
               await Alert.alert(
                   constantMsg.app_name,
                   constantMsg.msg_offline,
                   [
                       {
                           text: "Ok",
                           onPress:async () =>
                           {
                               
                           }
                       }
                   ])
           }
       })
   }

    _displayLoading=()=>
    {
        if(this.state.isloading)
        {
            return <Loading/>
        }else
        {
            return null
        }
    }
    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView>
                    <ScrollView contentContainerStyle={styles.scrollAuthloading}  contentInsetAdjustmentBehavior="automatic" keyboardShouldPersistTaps='handled'>
                    <KeyboardAvoidingView behavior="position" enabled    keyboardVerticalOffset={-DimScreen.heightScreen*0.3}>
                        <View style={styles.container}>
                            <HeaderAuthen _clickShowSLider={this._clickShowSLider}/>
                            <View style={styles.bodyLogin}>
                                    <Text style={styles.textConnectezVous}>MOT DE PASS OUBLIÉ</Text>
                                    <View style={styles.viewInputLogin}>
                                        <Text style={styles.labelConnect}>Adresse email</Text>
                                        <TextInput ref="emailInput"  value={this.state.email} maxLength={50}   onChangeText={(text) => { this.setState({ email: text })}} keyboardType="email-address"   placeholder="Votre email"  style={styles.inputText}/>
                                    </View>
                                    <TouchableOpacity  style={styles.btnLogin} onPress={()=>{ this._clickLInitPassWOrd()}} >
                                        <LinearGradient
                                            colors={[colors.colorBtnStart,colors.colorBtnEnd]} 
                                            start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                                            style={styles.linearGradienLogin} >
                                                <Text style={styles.textbtnLogin}>Réinitialisation du mot de passe</Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                    <View style={styles.panelBarrOU}>
                                        <View style={styles.barrOU}></View>
                                        <Text style={styles.textOU} >OU</Text>
                                        <View style={styles.barrOU}></View>
                                    </View>
                                    <TouchableOpacity style={styles.btnLogin} onPress={()=>{this.props.navigation.pop()}}>
                                        <LinearGradient
                                                colors={[colors.colorBtnStart,colors.colorBtnEnd]} 
                                                start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                                                style={styles.linearGradienLogin}
                                                >
                                            <Text style={styles.textbtnLogin}>Retour</Text>
                                        </LinearGradient>    
                                    </TouchableOpacity>
                            </View>
                        </View>
                       </KeyboardAvoidingView> 
                    </ScrollView>   
                </SafeAreaView>
                {this._displayLoading()} 
            </Fragment>
        );
    }
}
export default MotdepassOublier