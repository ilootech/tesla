import { StyleSheet,Platform } from 'react-native';
import DimScreen from "../../configs/DimScreen"
import colors from "../../configs/colors"
export default styles = StyleSheet.create({

    containerSafeAreaView: 
    {
        flex: 1,
    },
    containerHeader: {
        height:DimScreen.heightScreen*0.3,
        width:"100%", 
    },
    bodyFormulaire:
    {
        backgroundColor:colors.white,
        width:"90%",
        position:"relative",
        marginTop:DimScreen.heightScreen*0.04,
        marginBottom:DimScreen.heightScreen*0.02,
        marginLeft:DimScreen.widthScreen*0.1,
      
        paddingBottom:DimScreen.heightScreen*0.02,
        paddingLeft:DimScreen.widthScreen*0.05,
        paddingRight:DimScreen.widthScreen*0.05,
        marginRight:DimScreen.widthScreen*0.1,
        flex:1,
        borderWidth: 0,
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 4,
        alignItems:"center",
    },
    bodyPanel:
    {
        position:"relative",zIndex:20,flex:1, marginTop:-DimScreen.heightScreen*0.23
    },
    viewInput:
    {
        marginTop:DimScreen.heightScreen*0.045,
        width:"100%",
        alignItems:"flex-start",
        justifyContent:"flex-start",
        borderBottomColor:colors.vertBorderColor,
        borderBottomWidth:DimScreen.widthScreen*0.003,
        flexDirection:"column"
    },
    labelFormulaire:
    {
        color:colors.vertfonce,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Black",
    },
    inputText:
    {
        height:DimScreen.heightScreen*0.05,
        color:colors.gris1Font,
        fontSize:DimScreen.widthScreen*0.04,
        padding:0,
        flex:1,
        fontFamily:"Lato-Medium",
    },
    inputTextMessage:
    {
        height:DimScreen.heightScreen*0.15,
        color:colors.gris1Font,
        fontSize:DimScreen.widthScreen*0.04,
        padding:0,
        flex:1,
        fontFamily:"Lato-Medium",
       textAlignVertical:"top"
        
    },
    btnEnvMsg:
    {
        width:"85%",
        height:DimScreen.heightScreen*0.06,
        marginTop:DimScreen.heightScreen*0.04,
        marginBottom:DimScreen.heightScreen*0.06,  
    },
    linearGradienEnvMsg:
    {
        width:"100%",
        alignItems:"center",
        justifyContent:"center",
        height:"100%",
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 2,
    },
    textbtnMsg:
    {
        color:colors.white,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Bold",
    },
    fondHeader:
    {
        flex:1,
        alignItems:"center"
    },

    textMonProfil:{
        fontFamily:"Lato-Black",
        fontSize:DimScreen.widthScreen*0.07,
        marginTop:DimScreen.heightScreen*0.034,
        marginBottom:DimScreen.heightScreen*0.038,
        color:colors.white,
    },
    btnMenueProfil:
    {
        flex:2,
        borderRadius: DimScreen.heightScreen*0.04,
       
    },
    btnMenueProfilLeft:
    {
        marginRight:DimScreen.widthScreen*0.024,
    },
    imgBgMenueProfil:
    {
        width:"100%",
        height:"100%",
        borderRadius: DimScreen.heightScreen*0.02,
       
    },
    textMenueProfil:
    {
        width:"100%",
        height:"100%",
        textAlignVertical:"center",
        textAlign:"center",
        fontFamily:"Lato-Black",
        fontSize:DimScreen.widthScreen*0.032,
        color:colors.white,
        backgroundColor:colors.vertTransparent,
        borderRadius: DimScreen.heightScreen*0.02,
        position:"absolute",
        zIndex:5,
        top:0,
        paddingTop:Platform.OS === 'ios'?DimScreen.heightScreen*0.045:"auto"
    },

    
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    header:
    {
        width:"100%",
        flexDirection:"row",
        justifyContent:"space-between"
    },
    btnShowSlider:
    {
        marginLeft:DimScreen.widthScreen*0.025,
        marginTop:DimScreen.widthScreen*0.03,
    },
    scrollAuthloading:
    {
       /* justifyContent:"center",
        width:"100%",*/
    },
    bodyProfil:
    {
        width:"90%",
        position:"relative",
        marginTop:-DimScreen.heightScreen*0.15,
       /* marginBottom:DimScreen.heightScreen*0.08,*/
        marginLeft:DimScreen.widthScreen*0.1,
       /* paddingBottom:DimScreen.heightScreen*0.07,*/
       /* paddingTop:DimScreen.heightScreen*0.06,
      
        paddingLeft:DimScreen.widthScreen*0.05,
        paddingRight:DimScreen.widthScreen*0.05,*/
        marginRight:DimScreen.widthScreen*0.1,
        flex:1,
       /* borderWidth: 0,
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 4,*/
        alignItems:"center",
    },






    btnDeconnexion:
    {
        width:"75%",
        height:DimScreen.heightScreen*0.06,
        marginTop:DimScreen.heightScreen*0.021,
    },
    linearGradienDeconnexion:
    {
        width:"100%",
        alignItems:"center",
        justifyContent:"center",
        height:"100%",
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 2,
    },
    textbtnDeconnexion:
    {
        color:colors.white,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Bold",
    },



    btnPartage:
    {
        width:"65%",
        height:DimScreen.heightScreen*0.06,
        marginTop:DimScreen.heightScreen*0.04,
        marginBottom:DimScreen.heightScreen*0.2,
    },
    linearGradienPartage:
    {
        width:"100%",
        alignItems:"center",
        justifyContent:"center",
        height:"100%",
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 2,
    },
    textbtnPartage:
    {
        color:colors.white,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Bold",
    }
  

})