import React, { Component, Fragment } from 'react'
import {
    SafeAreaView,
    Keyboard,
    Alert,
    KeyboardAvoidingView, StatusBar, View, Text, ScrollView, ActivityIndicator,
    TouchableOpacity, TextInput, Image, ImageBackground
} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image'
import LinearGradient from 'react-native-linear-gradient'
import NetInfo from "@react-native-community/netinfo"
import { connect } from 'react-redux'

import Loading from "../../components/Loading/Loading"
import constantMsg from "../../configs/constantMsg"
import ContactHelpers from "../../apis/helpers/contact_helpers"
import colors from "../../configs/colors"
import HeaderPage from "../../components/HeaderPage/HeaderPage"
import styles from "./styles"
import DimScreen from '../../configs/DimScreen';
class ContactezNous extends Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props)
        this.state = {
            isloading: false,
            email: "",
            sujet: "",
            message: ""
        }
    }
    /** Show Slider barre */
    _clickShowSLider = () => {
        this.props.navigation.openDrawer()
    }
    _cickEnvoyerMsgCOntact = async () => {
        Keyboard.dismiss()
        NetInfo.isConnected.fetch().then(async (isConnected) => {
            if (isConnected) {
                if (this.state.email == "" && this.state.sujet == "" && this.state.message == "") {
                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_rempirChampsvide,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                }
                            }
                        ]
                    )
                }
                else if (this.state.email == "") {
                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_entrerEmail,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                    this.refs.emailInput.focus()
                                }
                            }
                        ]
                    )
                }
                else if (this.state.sujet == "") {
                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_entrerSujet,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                    this.refs.sujetInput.focus()
                                }
                            }
                        ]
                    )
                }
                else if (this.state.message == "") {
                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_entrerMessage,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                    this.refs.messageInput.focus()
                                }
                            }
                        ]
                    )
                } else if (!(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.email))) {
                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_alertEmailInvalid,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                    this.refs.emailInput.focus()
                                }
                            }
                        ]
                    )
                } else {
                    this.setState({ isloading: true })
                    await ContactHelpers.sendContactHelpers(this.state.email, this.state.sujet, this.state.message)
                        .then(async (data) => {
                            this.setState({
                                email: "",
                                sujet: "",
                                message: ""
                            })
                            this.setState({ isloading: false })
                            if (data.result == "success") {

                                await Alert.alert(
                                    constantMsg.app_name,
                                    constantMsg.msg_contactBienEnv,
                                    [
                                        {
                                            text: "Ok",
                                            onPress: () => {

                                            }
                                        }
                                    ]
                                )
                            } else {
                                await Alert.alert(
                                    constantMsg.app_name,
                                    constantMsg.msg_alertErreuContact,
                                    [
                                        {
                                            text: "Ok",
                                            onPress: () => {

                                            }
                                        }
                                    ]
                                )
                            }


                        })
                }
            } else {

                await Alert.alert(
                    constantMsg.app_name,
                    constantMsg.msg_offline,
                    [
                        {
                            text: "Ok",
                            onPress: () => {
                            }
                        }
                    ]
                )
            }
        })
    }

    _displayLoading = () => {
        if (this.state.isloading) {
            return <Loading />
        } else {
            return null
        }
    }

    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView style={styles.containerSafeAreaView}>
                    <HeaderPage _clickShowSLider={this._clickShowSLider} />
                    <View style={styles.bodyPanel}>
                        <ScrollView contentContainerStyle={styles.scrollAuthloading} keyboardShouldPersistTaps='handled' contentInsetAdjustmentBehavior="automatic">
                            <KeyboardAvoidingView behavior="position" enabled keyboardVerticalOffset={-DimScreen.heightScreen * 0.82}>
                                <View style={styles.container}>
                                    <Text style={styles.textMonProfil}>Contactez Nous</Text>
                                    <View style={styles.bodyFormulaire}>
                                        <View style={styles.viewInput}>
                                            <Text style={styles.labelFormulaire}>Sujet</Text>
                                            <TextInput ref="sujetInput" value={this.state.sujet} onChangeText={(text) => { this.setState({ sujet: text }) }} placeholder="Votre sujet" style={styles.inputText} />
                                        </View>
                                        <View style={styles.viewInput}>
                                            <Text style={styles.labelFormulaire}>Votre email</Text>
                                            <TextInput
                                                ref="emailInput"
                                                keyboardType={"email-address"}
                                                value={this.state.email}
                                                onChangeText={(text) => { this.setState({ email: text }) }}
                                                placeholder="Votre email"
                                                style={styles.inputText} />
                                        </View>
                                        <View style={styles.viewInput}>
                                            <Text style={styles.labelFormulaire}>Votre message</Text>
                                            <TextInput ref="messageInput" multiline={true} value={this.state.message} onChangeText={(text) => { this.setState({ message: text }) }} placeholder="Votre message" style={[styles.inputTextMessage]} />
                                        </View>
                                        <TouchableOpacity style={styles.btnEnvMsg} onPress={() => { this._cickEnvoyerMsgCOntact() }} >
                                            <LinearGradient
                                                colors={[colors.colorBtnStart, colors.colorBtnEnd]}
                                                start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                                style={styles.linearGradienEnvMsg}
                                            >
                                                <Text style={styles.textbtnMsg}>ENVOYER LE MESSAGE</Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </KeyboardAvoidingView>
                        </ScrollView>
                    </View>
                </SafeAreaView>
                {this._displayLoading()}
            </Fragment>
        );
    }
}
export default ContactezNous