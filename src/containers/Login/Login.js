import React, {Component,Fragment}  from 'react'
import {
    Alert,
    Keyboard,
    SafeAreaView,
     StatusBar,View,Text,ScrollView,KeyboardAvoidingView,
     ActivityIndicator,
    TouchableOpacity,TextInput} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image';
import  LinearGradient from 'react-native-linear-gradient'
import NetInfo from "@react-native-community/netinfo"
import AsyncStorage from "@react-native-community/async-storage"
import colors from "../../configs/colors"
import {connect} from 'react-redux'

import Loading from "../../components/Loading/Loading"
import constantMsg from  "../../configs/constantMsg"
import UserHelpers from "../../apis/helpers/user_helpers"
import HeaderAuthen from "../../components/HeaderAuthen/HeaderAuthen"
import styles from "./styles"
import DimScreen from '../../configs/DimScreen';
class Login extends Component 
{
    static navigationOptions = {
        header: null,
    };
    constructor(props){
        super(props)
        this.state={
            isloading:false,
            email:"",
            password:"",
            hidepassword:true,
            
        }
    }
    /** Show Slider barre */
    _clickShowSLider=()=>
    {
        this.props.navigation.openDrawer()  
    }

    /* Hidden Password
    **/
   _hiddenPassword=()=>
   {
       if(this.state.hidepassword)
       {
           this.setState({hidepassword:false})
       }else
       {
           this.setState({hidepassword:true})
       }
   }

     /** 
     *Test Adresse email Valide
    **/
    _testEmail=async(value)=>
    {
        if (value.length <= 255 && /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value)) 
        {
            return true
        }else
        {
            return false
        }
    }
    /*
    CLick Button LOgin
    */ 
    _clickLogin=async()=>
    {
        Keyboard.dismiss()
        NetInfo.isConnected.fetch().then(async (isConnected) => 
        {
            if(isConnected)
            {
                if(this.state.email=="" || this.state.password=="" )
                {
                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_rempirChampsvide,
                        [
                            {
                                text: "Ok",
                                onPress:async () =>
                                {
                                    
                                }
                            }
                        ])
                }
                else if(!(this.state.email.length <= 255 && /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.email)))
                {
                    await  Alert.alert(
                        constantMsg.app_name,
                       constantMsg.msg_alertEmailInvalid,
                       [
                           {
                               text: "Ok",
                               onPress: () => 
                               {
                                   this.refs.emailInput.focus()
                               }
                           }
                       ]
                   )

                }
                else if(this.state.password.length<8)
                {

                        await  Alert.alert(
                            constantMsg.app_name,
                            constantMsg.msg_invalidpwdMoins8caract,
                        [
                            {
                                text: "Ok",
                                onPress: () => 
                                {
                                    this.refs.passwordInput.focus()
                                }
                            }
                        ]
                    )
                }
                else
                {
                    
                    this.setState({isloading:true})
                    await UserHelpers.connectLoginHelpers(this.state.email,this.state.password)
                    .then(async(data)=>
                    {
                        if(data.etat_connexion==="connexion-ok")
                        { 
                            this.setState({isloading:false})
                            console.log("connectLoginHelpers"+JSON.stringify(data))
                            const action = { type: "ADD_USERCONNECT", value:data.info_utilisateur}
                            await  this.props.dispatch(action) 
                            await AsyncStorage.setItem('userInfos',JSON.stringify(data.info_utilisateur)) 
                            this.props.navigation.navigate("RootConnecteApp")
                        }else
                        {
                            this.setState({isloading:false})
                            await  Alert.alert(
                                constantMsg.app_name,
                                constantMsg.msg_EmailouPasswordfausse,
                            [
                                {
                                    text: "Ok",
                                    onPress: () => 
                                    {

                                    }
                                }
                            ]
                        )
                        }
                           
                    })
                }
              
            }else
            {
                await Alert.alert(
                    constantMsg.app_name,
                    constantMsg.msg_offline,
                    [
                        {
                            text: "Ok",
                            onPress:async () =>
                            {
                                
                            }
                        }
                    ])
            }
        })
    }
    _displayLoading=()=>
    {
        if(this.state.isloading)
        {
            return <Loading/>
        }else
        {
            return null
        }
    }
    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView>
                    <ScrollView contentContainerStyle={styles.scrollAuthloading} keyboardShouldPersistTaps='handled'  contentInsetAdjustmentBehavior="automatic">
                    <KeyboardAvoidingView behavior="position" enabled    keyboardVerticalOffset={-DimScreen.heightScreen*0.3}>
                        <View style={styles.container}>
                            <HeaderAuthen _clickShowSLider={this._clickShowSLider}/>
                            <View style={styles.bodyLogin}>
                                    <Text style={styles.textConnectezVous}>CONNECTEZ VOUS</Text>
                                    <View style={styles.viewInputLogin}>
                                        <Text style={styles.labelConnect}>Adresse email</Text>
                                        <TextInput ref="emailInput"  value={this.state.email} maxLength={50}   onChangeText={(text) => { this.setState({ email: text })}} keyboardType="email-address"   placeholder="Votre email"  style={styles.inputText}/>
                                    </View>
                                    <View style={styles.viewInputPassword}>
                                        <Text  style={styles.labelConnect}>Mot de passe</Text>
                                        <View style={styles.inputPassword}>
                                            <TextInput  ref="passwordInput"  value={this.state.password} maxLength={50}   onChangeText={(text) => { this.setState({ password: text })}} secureTextEntry={this.state.hidepassword} placeholder="Mot de passe"  style={styles.inputText}/>
                                           <TouchableOpacity onPress={()=>{this._hiddenPassword()}}>
                                                <AutoHeightImage
                                                source={require('../../assets/images/Authent/yeuxLogin.png')}
                                                width={DimScreen.widthScreen * 0.045} />
                                           </TouchableOpacity>

                                        </View>
                                    </View>
                                    <View style={styles.viewPassWordOublier}>
                                       <TouchableOpacity onPress={()=>{this.props.navigation.navigate("MotdepassOublierPage")}}>
                                            <Text style={styles.textPassWordOublier}>Mot de passe oublié ?</Text>
                                        </TouchableOpacity> 
                                    </View>

                                    <TouchableOpacity  style={styles.btnLogin} onPress={()=>{ this._clickLogin()/*this.props.navigation.navigate("MonProfilPage")*/}} >
                                        <LinearGradient
                                            colors={[colors.colorBtnStart,colors.colorBtnEnd]} 
                                            start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                                            style={styles.linearGradienLogin} >
                                                <Text style={styles.textbtnLogin}>{"Connexion".toUpperCase()}</Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                    <View style={styles.panelBarrOU}>
                                        <View style={styles.barrOU}></View>
                                        <Text style={styles.textOU} >OU</Text>
                                        <View style={styles.barrOU}></View>
                                    </View>
                                    <TouchableOpacity style={styles.btnLogin} onPress={()=>{this.props.navigation.navigate("InscriptionPage")}}>
                                        <LinearGradient
                                                colors={[colors.colorBtnStart,colors.colorBtnEnd]} 
                                                start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                                                style={styles.linearGradienLogin}
                                                >
                                            <Text style={styles.textbtnLogin}>{"Inscription".toUpperCase()}</Text>
                                        </LinearGradient>    
                                    </TouchableOpacity>
                            </View>
                        </View>
                       </KeyboardAvoidingView> 
                    </ScrollView>   
                </SafeAreaView>
                {this._displayLoading()} 
            </Fragment>
        );
    }
}

const mapStateProps= (state)=>
{
  return {
    userInfos:state.userInfos,
    }
}
export default connect(mapStateProps)(Login)