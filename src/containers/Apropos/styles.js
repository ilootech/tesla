import { StyleSheet,Platform } from 'react-native';
import DimScreen from "../../configs/DimScreen"
import colors from "../../configs/colors"
export default styles = StyleSheet.create({

    containerSafeAreaView: 
    {
        flex: 1,
    },
    containerHeader: {
        height:DimScreen.heightScreen*0.3,
        width:"100%", 
    },
    bodyFormulaire:
    {
        backgroundColor:colors.white,
        width:"90%",
        position:"relative",
        marginTop:DimScreen.heightScreen*0.04,
        marginBottom:DimScreen.heightScreen*0.02,
        marginLeft:DimScreen.widthScreen*0.1,
        paddingTop:DimScreen.heightScreen*0.05,
        paddingBottom:DimScreen.heightScreen*0.18,
        paddingLeft:DimScreen.widthScreen*0.05,
        paddingRight:DimScreen.widthScreen*0.05,
        marginRight:DimScreen.widthScreen*0.1,
        flex:1,
        borderWidth: 0,
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 4,
        alignItems:"center",
    },
    panelVotreCodeParrainage:
    {
        marginTop:DimScreen.heightScreen*0.045,
        marginBottom:DimScreen.heightScreen*0.03,
        width:"80%",
        alignItems:"center",
        borderBottomColor:colors.vertBorderColor,
        borderBottomWidth:DimScreen.widthScreen*0.003,
        flexDirection:"column"
    },
    labelVotreCodeParrainage:
    {
        color:colors.vertfonce,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Black",
    },
    textInputCodeParra:
    {
        height:DimScreen.heightScreen*0.05,
        color:colors.gris1Font,
        fontSize:DimScreen.widthScreen*0.04,
        padding:0,
        flex:1,
        fontFamily:"Lato-Medium",
    },
    textlist:
    {
        color:colors.gris1Font,
        fontSize:DimScreen.widthScreen*0.033,
        fontFamily:"Lato-Medium",
        marginLeft:DimScreen.widthScreen*0.03,
        width:"100%",

    },
    bodyPanel:
    {
        position:"relative",zIndex:20,flex:1, marginTop:-DimScreen.heightScreen*0.23
    },
    fondHeader:
    {
        flex:1,
        alignItems:"center"
    },
    btnShowSlider:
    {
        marginLeft:DimScreen.widthScreen*0.025,
        // marginTop:DimScreen.widthScreen*0.03,
        alignSelf:"center",
    },
    imgProfil:
    {
        borderColor:colors.white,
        borderWidth:DimScreen.widthScreen*0.01,
        borderRadius:(DimScreen.widthScreen * 0.28)/2
    },  
    textNomPrenom:
    {

        fontFamily:"Lato-Black",
        fontSize:DimScreen.widthScreen*0.06,
        marginTop:DimScreen.heightScreen*0.01,
        marginBottom:DimScreen.heightScreen*0.01,
        fontSize:DimScreen.heightScreen*0.03,
        color:colors.vertfonce,
    },
    textTelEmail:
    {
        fontSize:DimScreen.widthScreen*0.04,
        color:colors.gris1Font,
        fontFamily:"Lato-Regular"
    },

    labelCredit:
    {
        color:colors.vertfonce,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Black",
    }
    ,prixCredit:
    {
        color:colors.gris1Font,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Regular"
    },
    textMonProfil:{
        fontFamily:"Lato-Black",
        fontSize:DimScreen.widthScreen*0.07,
        marginTop:DimScreen.heightScreen*0.034,
        marginBottom:DimScreen.heightScreen*0.038,
        color:colors.white,
    },
    viewRowParrainage:
    {
        flexDirection:"row",
        // justifyContent:"center",
        alignItems:"center",
        marginLeft:DimScreen.widthScreen*0.02, 
        marginRight:DimScreen.widthScreen*0.05, 
    },
    viewCredits:
    {
        flexDirection:"row",
        alignItems:"center",
        marginTop:DimScreen.heightScreen*0.007,
        marginBottom:DimScreen.heightScreen*0.032,
    },
    viewMenueRow:
    {
        flexDirection:"row",
        height:DimScreen.heightScreen*0.11,
        width:"100%",
        marginBottom:DimScreen.widthScreen*0.024,
       
    },
    btnMenueProfil:
    {
        flex:2,
        borderRadius: DimScreen.heightScreen*0.04,
       
    },
    btnMenueProfilLeft:
    {
        marginRight:DimScreen.widthScreen*0.024,
    },
    imgBgMenueProfil:
    {
        width:"100%",
        height:"100%",
        borderRadius: DimScreen.heightScreen*0.02,
       
    },
    textMenueProfil:
    {
        width:"100%",
        height:"100%",
        textAlignVertical:"center",
        textAlign:"center",
        fontFamily:"Lato-Black",
        fontSize:DimScreen.widthScreen*0.032,
        color:colors.white,
        backgroundColor:colors.vertTransparent,
        borderRadius: DimScreen.heightScreen*0.02,
        position:"absolute",
        zIndex:5,
        top:0,
        paddingTop:Platform.OS === 'ios'?DimScreen.heightScreen*0.045:"auto"
    },

    
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    header:
    {
        width:"100%",
        flexDirection:"row",
        justifyContent:"space-between"
    },
    btnShowSlider:
    {
        marginLeft:DimScreen.widthScreen*0.025,
        marginTop:DimScreen.widthScreen*0.03,
    },
    scrollAuthloading:
    {
       /* justifyContent:"center",
        width:"100%",*/
    },
    bodyProfil:
    {
        width:"90%",
        position:"relative",
        marginTop:-DimScreen.heightScreen*0.15,
       /* marginBottom:DimScreen.heightScreen*0.08,*/
        marginLeft:DimScreen.widthScreen*0.1,
       /* paddingBottom:DimScreen.heightScreen*0.07,*/
       /* paddingTop:DimScreen.heightScreen*0.06,
      
        paddingLeft:DimScreen.widthScreen*0.05,
        paddingRight:DimScreen.widthScreen*0.05,*/
        marginRight:DimScreen.widthScreen*0.1,
        flex:1,
       /* borderWidth: 0,
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 4,*/
        alignItems:"center",
    },






    btnDeconnexion:
    {
        width:"75%",
        height:DimScreen.heightScreen*0.06,
        marginTop:DimScreen.heightScreen*0.021,
    },
    linearGradienDeconnexion:
    {
        width:"100%",
        alignItems:"center",
        justifyContent:"center",
        height:"100%",
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 2,
    },
    textbtnDeconnexion:
    {
        color:colors.white,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Bold",
    },



    btnPartage:
    {
        width:"65%",
        height:DimScreen.heightScreen*0.06,
        marginTop:DimScreen.heightScreen*0.04,
        marginBottom:DimScreen.heightScreen*0.2,
    },
    linearGradienPartage:
    {
        width:"100%",
        alignItems:"center",
        justifyContent:"center",
        height:"100%",
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 2,
    },
    textbtnPartage:
    {
        color:colors.white,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Bold",
    }
  

})