import React, { Component, Fragment } from 'react'
import {
    SafeAreaView, StatusBar, View, Text, ScrollView, ActivityIndicator,
    TouchableOpacity, TextInput, Image, ImageBackground
} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image'
import LinearGradient from 'react-native-linear-gradient'
import { connect } from 'react-redux'
import colors from "../../configs/colors"
import HeaderPage from "../../components/HeaderPage/HeaderPage"
import styles from "./styles"
import DimScreen from '../../configs/DimScreen';
class Apropos extends Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props)
    }
    /** Show Slider barre */
    _clickShowSLider = () => {
        this.props.navigation.openDrawer()
    }
    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView style={styles.containerSafeAreaView}>
                    <HeaderPage _clickShowSLider={this._clickShowSLider} />
                    <View style={styles.bodyPanel}>
                        <ScrollView contentContainerStyle={styles.scrollAuthloading} contentInsetAdjustmentBehavior="automatic">
                            <View style={styles.container}>
                                <Text style={styles.textMonProfil}>À propos</Text>
                                <View style={styles.bodyFormulaire}>
                                    {/* <View style={styles.viewRowParrainage}>
                                            <AutoHeightImage
                                                source={require('../../assets/images/Slider/parraignage.png')}
                                                width={DimScreen.widthScreen * 0.09} 
                                                style={styles.btnShowSlider}
                                                />
                                            <Text style={styles.textlist} >Notez l'application</Text> 
                                        </View>
                                        <View style={styles.viewRowParrainage}>
                                            <AutoHeightImage
                                                source={require('../../assets/icones/actus-vert-icon.png')}
                                                width={DimScreen.widthScreen * 0.09} 
                                                style={styles.btnShowSlider}
                                                />
                                            <Text  style={styles.textlist} >Suivez nos actus et bons plan</Text> 
                                        </View> */}
                                    <View style={styles.viewRowParrainage}>
                                        <AutoHeightImage
                                            source={require('../../assets/icones/web-vert-icon.png')}
                                            width={DimScreen.widthScreen * 0.09}
                                            style={styles.btnShowSlider}
                                        />
                                        <Text style={styles.textlist} >Visitez notre site web</Text>
                                    </View>
                                    <View style={styles.viewRowParrainage}>
                                        <AutoHeightImage
                                            source={require('../../assets/images/Slider/Apropos-vert.png')}
                                            width={DimScreen.widthScreen * 0.09}
                                            style={styles.btnShowSlider}
                                        />
                                        <Text style={styles.textlist} >Version 1.0.0</Text>
                                    </View>
                                    <View style={styles.viewRowParrainage}>
                                        <AutoHeightImage
                                            source={require('../../assets/icones/cgv-vert-icon.png')}
                                            width={DimScreen.widthScreen * 0.09}
                                            style={styles.btnShowSlider}
                                        />
                                        <Text style={styles.textlist} >Conditions générales d'utilisation</Text>
                                    </View>


                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }
}
export default Apropos