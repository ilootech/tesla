import { StyleSheet } from 'react-native';
import DimScreen from "../../configs/DimScreen"
import colors from "../../configs/colors"
export default styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
})