import React, {Component,Fragment}  from 'react'
import {SafeAreaView, StatusBar,View,Text,ScrollView, ActivityIndicator,
    TouchableOpacity,TextInput,Image,ImageBackground} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image'
import  LinearGradient from 'react-native-linear-gradient'
import {connect} from 'react-redux'
import colors from "../../configs/colors"
import HeaderPage from "../../components/HeaderPage/HeaderPage"
import styles from "./styles"
import DimScreen from '../../configs/DimScreen';
class Paiement extends Component 
{
    static navigationOptions = {
        header: null,
    };
    constructor(props){
        super(props)
    }
    /** Show Slider barre */
    _clickShowSLider=()=>
    {
        this.props.navigation.openDrawer()  
    }
    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView style={styles.containerSafeAreaView}>
                    <View style={styles.containerHeader}>
                        <ImageBackground
                                    style={styles.fondHeader}
                                    source={require("../../assets/images/fond/bgFondHeader.png")}>
                                        <View style={styles.header}>
                                            <TouchableOpacity onPress={()=>{this._clickShowSLider()}}>
                                                <AutoHeightImage
                                                    source={require('../../assets/images/Slider/imgMenueShow.png')}
                                                    width={DimScreen.widthScreen * 0.062} 
                                                    style={styles.btnShowSlider}
                                                    />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={styles.btnAdd} >
                                                <Text style={styles.textBtnAdd}>+</Text>
                                            </TouchableOpacity>
                                        </View>           
                         </ImageBackground>
                    </View>
                    <View style={styles.bodyPanel}>
                    <ScrollView contentContainerStyle={styles.scrollAuthloading}  contentInsetAdjustmentBehavior="automatic">
                        <View style={styles.container}>
                            <Text style={styles.textMonProfil}>PAIEMENT</Text>
                            <View style={styles.bodyFormulaire}>
                                <View style={styles.panelPerso}>
                                    <Text style={styles.labePerso}>Perso</Text>
                                </View>
                                <View style={styles.panelpersoPayement}>
                                    <TouchableOpacity style={styles.btnPayement} >
                                        <Text style={styles.textbtnPayement}>Paiement à bord</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.btnPayement}>
                                        <Text style={styles.textbtnPayement}>Défault</Text>
                                    </TouchableOpacity>
                                </View>
                                    
                            </View>            
                        </View>
                    </ScrollView>  
                    </View>  
                </SafeAreaView>
            </Fragment>
        );
    }
}
export default Paiement