import React, { Component, Fragment } from 'react'
import { SafeAreaView, StatusBar, View, ScrollView, ActivityIndicator, Dimensions } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux'

import ReservationHelpers from "../../apis/helpers/reservation_helpers"
import styles from './styles';
import colors from '../../configs/colors';

class Authloading extends Component {

    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props)

    }
    componentDidMount = async () => {
        await this._loadingCheckInfos()
        await this.getSession()
    }

    _loadingCheckInfos = async () => {
        console.log("Vo miditra")
        await ReservationHelpers.listeTypesVoituresHelpers()
            .then(async (data) => {
                const action = { type: "ADD_CHOIXVOITURE", value: data[0] }
                await this.props.dispatch(action)

                const _action = { type: "ADD_TYPEVOITURES", value: data }
                await this.props.dispatch(_action)
            })
        console.log("Azo le list")


    }

    getSession = async () => {
        console.log("get session")
        let sessionInfos = await AsyncStorage.getItem("userInfos")
        console.log(JSON.stringify(sessionInfos))
        if (sessionInfos == null) {
            await this.props.navigation.navigate("RootNav")
        } else {
            const action = { type: "ADD_USERCONNECT", value: JSON.parse(sessionInfos) }
            await this.props.dispatch(action)
            await this.props.navigation.navigate("RootConnecteApp")
        }
    }
    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView>
                    <ScrollView contentContainerStyle={styles.scrollAuthloading} contentInsetAdjustmentBehavior="automatic">
                        <View style={styles.container}>
                            <ActivityIndicator size={'large'} color={colors.yoblue} />
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </Fragment>
        );
    }
}

const mapStateProps = (state) => {
    return {
        userInfos: state.userInfos,
        choixLocVoiture: state.choixLocVoiture,
        listTypeVoiture: state.listTypeVoiture,
    }
}
export default connect(mapStateProps)(Authloading)