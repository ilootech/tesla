import { StyleSheet } from 'react-native';
export default styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    scrollAuthloading:
    {
        height:"100%",
        justifyContent:"center"
    },
})