import React, { Component, Fragment } from 'react'
import {
    SafeAreaView, Alert, StatusBar, View, Text, ScrollView, ActivityIndicator,
    TouchableOpacity, TextInput, ImageBackground
} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image';
import LinearGradient from 'react-native-linear-gradient'
//import GetLocation from 'react-native-get-location'
import Geolocation from '@react-native-community/geolocation';

import { connect } from 'react-redux'
import colors from "../../configs/colors"
import styles from "./styles"
import DimScreen from '../../configs/DimScreen';
class Slider extends Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props)
        this.onRegionChange()
    }


    onRegionChange = () => {
        Geolocation.getCurrentPosition(
            async (position) => {
                console.log("location::" + JSON.stringify(position.coords))
                const action = { type: "ADD_REGION", value: position.coords }
                await this.props.dispatch(action)

                /*const initialPosition = JSON.stringify(position);
                this.setState({initialPosition});*/
            },
            error => Alert.alert('Error', JSON.stringify(error)),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );

        /*GetLocation.getCurrentPosition({
        enableHighAccuracy: true,
        timeout: 15000,
            })
            .then(async(location) => 
            {
                console.log("location::"+JSON.stringify(location))
                const action = { type: "ADD_REGION", value: location}
                await  this.props.dispatch(action) 
            })
            .catch(error => {
                const { code, message } = error;
                console.warn(code, message);
            })*/
    }
    /** Click Menue Btn **/
    clickMenue = async (value) => {
        if (value == "ContactezNous" || value == "Apropos") {
            const _action = { type: "ADD_MENUE_ACTIF", value: value }
            await this.props.dispatch(_action)
            this._navigationPage(value+"Page")
        }
        else {
            if (this.props.userInfos == "") {
                Alert.alert("Information !", "Vous devez vous connecter avant de poursuivre.")
                this.props.navigation.navigate("LoginPage")
            }
            else {
                const _action = { type: "ADD_MENUE_ACTIF", value: value }
                await this.props.dispatch(_action)
                switch (value) {
                    case "newResa":
                        this._navigationPage("ReservationPage")
                        break;
                    case "monProfil":
                        this._navigationPage("MonProfilPage")
                        break;
                    case "mesResa":
                        this._navigationPage("MesreservationsPage")
                        break;
                    case "Parrainage":
                        this._navigationPage("ParrainagePage")
                        break;
                    case "Paiement":
                        this._navigationPage("PaiementPage")
                        break;
                    case "ContactezNous":
                        this._navigationPage("ContactezNousPage")
                        break;
                    case "Apropos":
                        this._navigationPage("AproposPage")
                        break;
                }
            }
        }


    }
    _navigationPage = (namePage) => {
        this.props.navigation.closeDrawer()
        this.props.navigation.navigate(namePage)


    }

    render() {
        return (<Fragment>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView>
                <ScrollView contentContainerStyle={styles.scrollBody} contentInsetAdjustmentBehavior="automatic">
                    <View style={styles.container}>
                        <View style={styles.header}>
                            <ImageBackground
                                style={styles.fondHeader}
                                source={require("../../assets/images/Authent/bgSlider.png")}>
                                {this.props.userInfos == "" ?
                                    <AutoHeightImage
                                        source={require('../../assets/images/Slider/imgHeaderSlider.png')}
                                        width={DimScreen.widthScreen * 0.087} />
                                    : null}
                                {this.props.userInfos == "" ?
                                    <View style={styles.headerSLider}>
                                        <TouchableOpacity onPress={() => { this._navigationPage("LoginPage") }}>
                                            <Text style={styles.textFondSlderHead} >Se connecter</Text>
                                        </TouchableOpacity>
                                        <Text style={styles.textFondSlderHead}> / </Text>
                                        <TouchableOpacity onPress={() => { this._navigationPage("InscriptionPage") }}>
                                            <Text style={styles.textFondSlderHead}>S’inscrire</Text>
                                        </TouchableOpacity>
                                    </View>
                                    :
                                    <View style={styles.headerSLiderConnecter}>
                                        <Text style={styles.textNomPrenom}>{this.props.userInfos.last_name} {this.props.userInfos.first_name} </Text>
                                        <AutoHeightImage
                                            source={{ uri: this.props.userInfos.avatar }}
                                            width={DimScreen.widthScreen * 0.19}
                                            style={styles.imgProfil} />
                                    </View>
                                }
                            </ImageBackground>
                        </View>
                        <View style={styles.bodyMenue}>
                            <TouchableOpacity style={styles.btnMenue} onPress={() => { this.clickMenue("newResa") }} >
                                <LinearGradient
                                    colors={[this.props.menueActif == "newResa" ? colors.colorBtnStart : colors.white, this.props.menueActif == "newResa" ? colors.colorBtnEnd : colors.white]}
                                    start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                    style={styles.linearGradienMenue} >
                                    <AutoHeightImage
                                        source={this.props.menueActif != "newResa" ? require('../../assets/images/Slider/newReservation-vert.png') : require('../../assets/images/Slider/newReservation-blanc.png')}
                                        width={DimScreen.widthScreen * 0.057} />
                                    <Text style={[styles.textFontMenue, { color: this.props.menueActif == "newResa" ? colors.white : colors.vertfonce }]} >{"Nouvelle réservation".toUpperCase()}</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                            {this.props.userInfos != "" ?
                                <TouchableOpacity style={styles.btnMenue} onPress={() => { this.clickMenue("monProfil") }}  >
                                    <LinearGradient
                                        colors={[this.props.menueActif == "monProfil" ? colors.colorBtnStart : colors.white, this.props.menueActif == "monProfil" ? colors.colorBtnEnd : colors.white]}
                                        start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                        style={styles.linearGradienMenue} >
                                        <AutoHeightImage
                                            source={this.props.menueActif != "monProfil" ? require('../../assets/images/Slider/profil-vert-icon.png') : require('../../assets/images/Slider/profil-blanc-icon.png')}
                                            width={DimScreen.widthScreen * 0.057} />
                                        <Text style={[styles.textFontMenue, { color: this.props.menueActif == "monProfil" ? colors.white : colors.vertfonce }]}>{"Mon Profil".toUpperCase()}</Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                                : null}
                            <TouchableOpacity style={styles.btnMenue} onPress={() => { this.clickMenue("mesResa") }}  >
                                <LinearGradient
                                    colors={[this.props.menueActif == "mesResa" ? colors.colorBtnStart : colors.white, this.props.menueActif == "mesResa" ? colors.colorBtnEnd : colors.white]}
                                    start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                    style={styles.linearGradienMenue} >
                                    <AutoHeightImage
                                        source={this.props.menueActif != "mesResa" ? require('../../assets/images/Slider/reservation-green.png') : require('../../assets/images/Slider/reservation-white.png')}
                                        width={DimScreen.widthScreen * 0.057} />
                                    <Text style={[styles.textFontMenue, { color: this.props.menueActif == "mesResa" ? colors.white : colors.vertfonce }]}>{"Mes réservations".toUpperCase()}</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                            {/* {this.props.userInfos!=""?
                                <TouchableOpacity style={styles.btnMenue}  onPress={()=>{this.clickMenue("Parrainage")}}  >
                                    <LinearGradient
                                        colors={[this.props.menueActif=="Parrainage"?colors.colorBtnStart:colors.white,this.props.menueActif=="Parrainage"?colors.colorBtnEnd:colors.white]} 
                                        start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                                        style={styles.linearGradienMenue} >
                                             <AutoHeightImage
                                                source={this.props.menueActif!="Parrainage"?require('../../assets/images/Slider/parraignage.png'):require('../../assets/images/Slider/parraignage-white.png')}
                                                width={DimScreen.widthScreen * 0.057} />
                                             <Text style={[styles.textFontMenue,{color:this.props.menueActif=="Parrainage"?colors.white:colors.vertfonce}]}>Parrainage</Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                                 :null} */}
                            {/* <TouchableOpacity style={styles.btnMenue}  onPress={()=>{this.clickMenue("Paiement")}}  >
                                    <LinearGradient
                                        colors={[this.props.menueActif=="Paiement"?colors.colorBtnStart:colors.white,this.props.menueActif=="Paiement"?colors.colorBtnEnd:colors.white]} 
                                        start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                                        style={styles.linearGradienMenue} >
                                             <AutoHeightImage
                                                source={this.props.menueActif!="Paiement"?require('../../assets/images/Slider/paiement-green.png'):require('../../assets/images/Slider/paiement-light.png')}
                                                width={DimScreen.widthScreen * 0.057} />
                                             <Text style={[styles.textFontMenue,{color:this.props.menueActif=="Paiement"?colors.white:colors.vertfonce}]}>Paiement</Text>
                                    </LinearGradient>
                                </TouchableOpacity> */}
                            <TouchableOpacity style={styles.btnMenue} onPress={() => { this.clickMenue("ContactezNous") }}  >
                                <LinearGradient
                                    colors={[this.props.menueActif == "ContactezNous" ? colors.colorBtnStart : colors.white, this.props.menueActif == "ContactezNous" ? colors.colorBtnEnd : colors.white]}
                                    start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                    style={styles.linearGradienMenue} >
                                    <AutoHeightImage
                                        source={this.props.menueActif != "ContactezNous" ? require('../../assets/images/Slider/user-vert.png') : require('../../assets/images/Slider/user-blanc.png')}
                                        width={DimScreen.widthScreen * 0.057} />
                                    <Text style={[styles.textFontMenue, { color: this.props.menueActif == "ContactezNous" ? colors.white : colors.vertfonce }]}>{"Contactez nous".toUpperCase()}</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.btnMenue} onPress={() => { this.clickMenue("Apropos") }}  >
                                <LinearGradient
                                    colors={[this.props.menueActif == "Apropos" ? colors.colorBtnStart : colors.white, this.props.menueActif == "Apropos" ? colors.colorBtnEnd : colors.white]}
                                    start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                    style={styles.linearGradienMenue} >
                                    <AutoHeightImage
                                        source={this.props.menueActif != "Apropos" ? require('../../assets/images/Slider/Apropos-vert.png') : require('../../assets/images/Slider/Apropos-blanc.png')}
                                        width={DimScreen.widthScreen * 0.057} />
                                    <Text style={[styles.textFontMenue, { color: this.props.menueActif == "Apropos" ? colors.white : colors.vertfonce }]}>{"À propos".toUpperCase()}</Text>
                                </LinearGradient>
                            </TouchableOpacity>

                        </View>
                        <View style={styles.footerSlider}>
                            <LinearGradient
                                colors={[colors.colorBtnStart, colors.colorBtnEnd]}
                                start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                style={styles.linearGradienFooter} >
                                <AutoHeightImage
                                    source={require('../../assets/images/Slider/imgsntSlider.png')}
                                    width={DimScreen.widthScreen * 0.1} />
                            </LinearGradient>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </Fragment>)
    }
}
const mapStateProps = (state) => {
    return {
        userInfos: state.userInfos,
        menueActif: state.menueActif,
        region: state.region
    }
}
export default connect(mapStateProps)(Slider)
