import { StyleSheet } from 'react-native';
import DimScreen from "../../configs/DimScreen"
import colors from "../../configs/colors"
export default styles = StyleSheet.create({
    scrollBody:
    {
        
    },
     textNomPrenom:
    {

        fontFamily:"Lato-Black",
        fontSize:DimScreen.widthScreen*0.06,
        marginTop:DimScreen.heightScreen*0.01,
        marginBottom:DimScreen.heightScreen*0.02,
        fontSize:DimScreen.heightScreen*0.024,
        color:colors.white,
    },
    imgProfil:
    {
        borderColor:colors.white,
        borderWidth:DimScreen.widthScreen*0.01,
        borderRadius:(DimScreen.widthScreen * 0.2)/2
    },  
    container: {
        height:"100%",
    },
    header: {
        height:DimScreen.heightScreen*0.3,
        width:"100%", 
    },
    fondHeader:
    {
        flex:1,
        alignItems:"center",
        paddingTop:DimScreen.heightScreen*0.06,
    },
    headerSLider:
    {
        paddingTop:DimScreen.heightScreen*0.015,
        flexDirection:"row",
    },
    headerSLiderConnecter:
    {
        alignItems:"center",
    },
    textFondSlderHead:
    {
        fontSize:DimScreen.widthScreen*0.037,
        color:colors.white,
        fontFamily:"Lato-Bold",
    },
    bodyMenue:
    {
        backgroundColor:colors.white,
        height:DimScreen.heightScreen*0.58,
        paddingTop:DimScreen.heightScreen*0.065
    },
    btnMenue:
    {
        height:DimScreen.heightScreen*0.07,
        width:"100%"
    },
    linearGradienMenue:
    {
        height:"100%",
        flex:1,
        flexDirection:"row",
        alignItems:"center",
        paddingLeft:DimScreen.widthScreen*0.025,
    },
    textFontMenue:
    {   
        paddingLeft:DimScreen.widthScreen*0.025,
        fontSize:DimScreen.widthScreen*0.034,
        fontFamily:"Lato-Bold"
    },
    footerSlider:
    {
        height:DimScreen.heightScreen*0.08,
        width:"100%",
        backgroundColor:"green"
    },
    linearGradienFooter:
    {
        flex:1,
        height:"100%",
        alignItems:"center",
        justifyContent:"center"
    }
})