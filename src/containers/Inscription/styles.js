import { StyleSheet } from 'react-native';
import DimScreen from "../../configs/DimScreen"
import colors from "../../configs/colors"
export default styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    scrollAuthloading:
    {
        justifyContent:"center",
        width:"100%",
    },
    bodyInscription:
    {
        backgroundColor:colors.white,
        width:"90%",
        position:"relative",
        marginTop:-DimScreen.heightScreen*0.08,
        marginBottom:DimScreen.heightScreen*0.08,
        marginLeft:DimScreen.widthScreen*0.1,
        paddingTop:DimScreen.heightScreen*0.06,
        paddingBottom:DimScreen.heightScreen*0.07,
        paddingLeft:DimScreen.widthScreen*0.05,
        paddingRight:DimScreen.widthScreen*0.05,
        marginRight:DimScreen.widthScreen*0.1,
        flex:1,
        borderWidth: 0,
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 4,
        alignItems:"center",
    },
    textTitreInscription:
    {
        fontFamily:"Lato-Black",
        color:colors.gris1Font,
        fontSize:DimScreen.widthScreen*0.06,
    },
    textvousetes:
    {
        fontFamily:"Lato-Heavy",
        fontSize:DimScreen.widthScreen*0.045,
        marginTop:DimScreen.heightScreen*0.025,
        marginBottom:DimScreen.heightScreen*0.02,
        color:"#606060"
    },
    choixSituation:
    {
        flexDirection:"row",
        borderColor:colors.vertfonce,
        borderRadius:DimScreen.widthScreen*0.02,
        borderWidth:DimScreen.widthScreen*0.005,
        height:DimScreen.heightScreen*0.06,
    },
    btnchoixSituation:
    {
        flex:2,
        alignItems:"center",
        justifyContent:"center",
        height:"100%",
       
    },
    textchoixSituation:
    {
        fontSize:DimScreen.widthScreen*0.03,
        fontFamily:"Lato-Heavy",
        textTransform:"uppercase",
        marginLeft:DimScreen.widthScreen*0.028,
    },
    linearGradienLogin:
    {
        width:"100%",
        alignItems:"center",
        justifyContent:"center",
        height:"100%",
        flexDirection:"row",
    },
    viewInput:
    {
        marginTop:DimScreen.heightScreen*0.045,
        width:"100%",
        alignItems:"flex-start",
        borderBottomColor:colors.vertBorderColor,
        borderBottomWidth:DimScreen.widthScreen*0.003,
        flexDirection:"column"
    },
    labelInsription:
    {
        color:colors.vertfonce,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Black",
    },
    inputPassword:
    {
        flexDirection:"row",
        alignItems:"center",
        marginRight:DimScreen.widthScreen*0.01,
    },
    inputText:
    {
        height:DimScreen.heightScreen*0.05,
        color:colors.gris1Font,
        fontSize:DimScreen.widthScreen*0.04,
        padding:0,
        flex:1,
        fontFamily:"Lato-Medium",
    },
    btnInscri:
    {
        marginTop:DimScreen.heightScreen*0.045,
        width:"75%",
        height:DimScreen.heightScreen*0.06,
    },
    btnRetour:
    {
        marginTop:DimScreen.heightScreen*0.035,
        width:"75%",
        height:DimScreen.heightScreen*0.06,
    },
    linearGradienInscri:
    {
        width:"100%",
        alignItems:"center",
        justifyContent:"center",
        height:"100%",
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 2,
    },
    textbtnInscri:
    {
        color:colors.white,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Bold",
    }
    

})