import React, { Component, Fragment } from 'react'
import {
    Alert,
    Platform,
    Keyboard,
    SafeAreaView, StatusBar, View, Text, ScrollView, KeyboardAvoidingView, ActivityIndicator,
    TouchableOpacity, TextInput
} from 'react-native'
import { connect } from 'react-redux'
import AutoHeightImage from 'react-native-auto-height-image';
import LinearGradient from 'react-native-linear-gradient'
import NetInfo from "@react-native-community/netinfo"

import Loading from "../../components/Loading/Loading"
import colors from "../../configs/colors"
import constantMsg from "../../configs/constantMsg"
import UserHelpers from "../../apis/helpers/user_helpers"
import HeaderAuthen from "../../components/HeaderAuthen/HeaderAuthen"
import DimScreen from "../../configs/DimScreen"
import styles from "./styles"
class Inscription extends Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props)
        this.state = {
            isloading: false,
            choixInsription: 0,
            nom: "",
            prenom: "",
            email: "",
            password: "",
            confirmpassword: "",
            typeUser: "particuler",
            hidepassword: true,
            siren: "",
            tel:""
        }
    }
    _getInitialize = () => {
        this.setState({ nom: "" })
        this.setState({ prenom: "" })
        this.setState({ tel: "" })
        this.setState({ email: "" })
        this.setState({ password: "" })
        this.setState({ confirmpassword: "" })
        this.setState({ siren: "" })
    }
    /**
     * ***** Fonction de test si le nom et prenom ne contient pas un donne NUMERIQUE ******
     * **/
    _testNonnum = async (value) => {
        //=/[a-zA-Z]\d+/ 
        console.log("_testNonnum")
        var reg_alphab = /[a-zA-Z]\d+/ ///^[A-Za-z]+$/;
        if (await reg_alphab.test(value)) return true
        return false

    }
    /** 
     *Test Adresse email Valide
    **/
    _testEmail = async (value) => {
        if (value.length <= 255 && /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value)) {
            return true
        } else {
            return false
        }
    }
    /** Show Slider barre */
    _clickShowSLider = () => {
        this.props.navigation.openDrawer()
    }
    /* Hidden Password
   **/
    _hiddenPassword = () => {
        if (this.state.hidepassword) {
            this.setState({ hidepassword: false })
        } else {
            this.setState({ hidepassword: true })
        }
    }
    /**** click Inscription ****/
    _clickBtnInscription = async () => {
        console.log("prenom" + this.state.prenom)
        Keyboard.dismiss()
        NetInfo.isConnected.fetch().then(async (isConnected) => {
            if (isConnected) {
                this.setState({ typeUser: this.state.choixInsription == 0 ? "particuler" : "professionnel" })
                if (this.state.choixInsription == 1) {
                    this.setState({ password: "snt123456" })
                    this.setState({ confirmpassword: "snt123456" })
                }
                if (this.state.nom == "" && this.state.prenom == "" && this.state.email == "" && this.state.password == "" && this.state.confirmpassword == "" && this.state.tel == "") {
                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_rempirChampsvide,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                }
                            }
                        ]
                    )
                }
                else if (this.state.nom == "") {
                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_entrerNom,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                    this.refs.nomInput.focus()
                                }
                            }
                        ]
                    )
                }
                else if (this.state.prenom == "") {
                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_entrerPrenom,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                    this.refs.prenomInput.focus()
                                }
                            }
                        ]
                    )
                }
                else if (this.state.tel == "") {
                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_entrerTel,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                    this.refs.telInput.focus()
                                }
                            }
                        ]
                    )
                }
                else if (this.state.email == "") {
                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_entrerEmail,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                    this.refs.emailInput.focus()
                                }
                            }
                        ]
                    )
                }
                else if (this.state.password == "") {
                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_entrerPassword,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                    this.refs.passwordInput.focus()
                                }
                            }
                        ]
                    )
                }
                else if (this.state.confirmpassword == "") {
                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_entrerconfirmPassword,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                    this.refs.confirmpasswordInput.focus()
                                }
                            }
                        ]
                    )
                }
                else if (!(this.state.email.length <= 255 && /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.email))) {
                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_alertEmailInvalid,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                    this.refs.emailInput.focus()
                                }
                            }
                        ]
                    )
                }
                else if (this.state.password.length < 8) {

                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_invalidpwdMoins8caract,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                    this.refs.passwordInput.focus()
                                }
                            }
                        ]
                    )
                }
                else if (this.state.confirmpassword.length < 8) {

                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_invalidpwdMoins8caract,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                    this.refs.confirmpasswordInput.focus()
                                }
                            }
                        ]
                    )
                }
                else if (this.state.password != this.state.confirmpassword) {
                    await Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_invalidPwdIdentique,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                    this.refs.passwordInput.focus()
                                }
                            }
                        ]
                    )
                }
                else {
                    this.setState({ isloading: true })
                    await UserHelpers.createAccountHelpers(this.state.typeUser, this.state.nom, this.state.prenom, this.state.tel, this.state.email, this.state.password, this.state.siren)
                        .then(async (data) => {
                            this.setState({ isloading: false })
                            console.log("INSCRIPTION:::" + JSON.stringify(data))
                            if (data.etat_inscription == "inscription-ok") {
                                await Alert.alert(
                                    constantMsg.app_name,
                                    constantMsg.msg_felicidationInscri,
                                    [
                                        {
                                            text: "Ok",
                                            onPress: async () => {
                                                this._getInitialize()
                                                // await this.props.navigation.navigate("Reservation")
                                            }
                                        }
                                    ]
                                )
                            } else {
                                Alert.alert(
                                    constantMsg.app_name,
                                    data.error_name,
                                    [
                                        {
                                            text: "Ok",
                                            onPress: async () => {

                                            }
                                        }
                                    ]
                                )
                            }

                        })
                }
            } else {

                await Alert.alert(
                    constantMsg.app_name,
                    constantMsg.msg_offline,
                    [
                        {
                            text: "Ok",
                            onPress: async () => {

                            }
                        }
                    ]
                )
            }
        })
    }

    _displayLoading = () => {
        if (this.state.isloading) {
            return <Loading />
        } else {
            return null
        }
    }
    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView>
                    <ScrollView pointerEvents={Platform.OS === 'ios' ? 'auto' : 'none'} keyboardShouldPersistTaps='handled' contentContainerStyle={styles.scrollAuthloading} contentInsetAdjustmentBehavior="automatic">
                        <KeyboardAvoidingView behavior="position" enabled keyboardVerticalOffset={-DimScreen.heightScreen * 0.82}>
                            <View style={styles.container}>
                                <HeaderAuthen _clickShowSLider={this._clickShowSLider} />
                                <View style={styles.bodyInscription}>
                                    <Text style={styles.textTitreInscription}>INSCRIVEZ-VOUS</Text>
                                    <Text style={styles.textvousetes}>Vous êtes ?</Text>
                                    <View style={styles.choixSituation}>
                                        <TouchableOpacity style={styles.btnchoixSituation} onPress={() => { this.setState({ choixInsription: 0 }) }} >
                                            <LinearGradient
                                                colors={[this.state.choixInsription == 0 ? colors.colorBtnStart : colors.transparent, this.state.choixInsription == 0 ? colors.colorBtnEnd : colors.transparent]}
                                                start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                                style={styles.linearGradienLogin} >
                                                <AutoHeightImage
                                                    source={this.state.choixInsription == 0 ? require('../../assets/images/Authent/personnal-white.png') : require('../../assets/images/Authent/personnal-green.png')}
                                                    width={DimScreen.widthScreen * 0.05} />
                                                <Text style={[styles.textchoixSituation, { color: this.state.choixInsription == 0 ? colors.white : colors.vertfonce }]}>Particulier</Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.btnchoixSituation} onPress={() => { this.setState({ choixInsription: 1 }) }}>
                                            <LinearGradient
                                                colors={[this.state.choixInsription == 1 ? colors.colorBtnEnd : colors.transparent, this.state.choixInsription == 1 ? colors.colorBtnStart : colors.transparent]}
                                                start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                                style={styles.linearGradienLogin} >
                                                <AutoHeightImage
                                                    source={this.state.choixInsription == 0 ? require('../../assets/images/Authent/professionnel-green.png') : require('../../assets/images/Authent/professionnel-white.png')}
                                                    width={DimScreen.widthScreen * 0.05} />
                                                <Text style={[styles.textchoixSituation, { color: this.state.choixInsription == 1 ? colors.white : colors.vertfonce }]} >Professionnel</Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.viewInput}>
                                        <Text style={styles.labelInsription}>Prénom</Text>
                                        <TextInput ref="prenomInput" value={this.state.prenom} maxLength={50} onChangeText={(text) => { this.setState({ prenom: text }) }} placeholder="Votre prénom" style={styles.inputText} />
                                    </View>
                                    <View style={styles.viewInput}>
                                        <Text style={styles.labelInsription}>Nom</Text>
                                        <TextInput ref="nomInput" value={this.state.nom} maxLength={50} onChangeText={(text) => { this.setState({ nom: text }) }} placeholder="Votre nom" style={styles.inputText} />
                                    </View>
                                    <View style={styles.viewInput}>
                                        <Text style={styles.labelInsription}>Téléphone</Text>
                                        <TextInput ref="telInput" value={this.state.tel} maxLength={50} onChangeText={(text) => { this.setState({ tel: text }) }} placeholder="Votre téléphone" style={styles.inputText} keyboardType="numeric"/>
                                    </View>
                                    <View style={styles.viewInput}>
                                        <Text style={styles.labelInsription}>Email</Text>
                                        <TextInput
                                            ref="emailInput"
                                            value={this.state.email}
                                            maxLength={50}
                                            onChangeText={(text) => { this.setState({ email: text }) }}
                                            onBlur={() => this.setState({ email: this.state.email.toLowerCase() })}
                                            keyboardType="email-address"
                                            placeholder="Votre adresse email"
                                            style={styles.inputText} />

                                    </View>
                                    {this.state.choixInsription == 0 ?

                                        <View style={styles.viewInput}>
                                            <Text style={styles.labelInsription}>Mot de passe</Text>
                                            <View style={styles.inputPassword}>
                                                <TextInput ref="passwordInput" value={this.state.password} maxLength={50} onChangeText={(text) => { this.setState({ password: text }) }} secureTextEntry={this.state.hidepassword} placeholder="*******" style={styles.inputText} />
                                                <TouchableOpacity onPress={() => { this._hiddenPassword() }}>
                                                    <AutoHeightImage
                                                        source={require('../../assets/images/Authent/yeuxLogin.png')}
                                                        width={DimScreen.widthScreen * 0.045} />
                                                </TouchableOpacity>

                                            </View>
                                        </View> : null
                                    }
                                    {this.state.choixInsription == 0 ?
                                        <View style={styles.viewInput}>
                                            <Text style={styles.labelInsription}>Confirmation Mot de passe</Text>
                                            <TextInput ref="confirmpasswordInput" value={this.state.confirmpassword} maxLength={50} onChangeText={(text) => { this.setState({ confirmpassword: text }) }} secureTextEntry={true} placeholder="*******" style={styles.inputText} />
                                        </View> : null
                                    }
                                    {this.state.choixInsription == 1 ?
                                        <View style={styles.viewInput}>
                                            <Text style={styles.labelInsription}>Siren</Text>
                                            <TextInput ref="sirenInput" value={this.state.siren} maxLength={50} onChangeText={(text) => { this.setState({ siren: text }) }} placeholder="Siren de l'entreprise" style={styles.inputText} />
                                        </View> : null
                                    }
                                    <TouchableOpacity onPress={() => { this._clickBtnInscription() }} style={styles.btnInscri} >
                                        <LinearGradient
                                            colors={[colors.colorBtnStart, colors.colorBtnEnd]}
                                            start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                            style={styles.linearGradienInscri} >
                                            <Text style={styles.textbtnInscri}>S'inscrire</Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.btnRetour} onPress={() => { this.props.navigation.pop() }} >
                                        <LinearGradient
                                            colors={[colors.colorBtnStart, colors.colorBtnEnd]}
                                            start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                            style={styles.linearGradienInscri} >
                                            <Text style={styles.textbtnInscri}>Retour</Text>
                                        </LinearGradient>
                                    </TouchableOpacity>

                                </View>
                            </View>
                        </KeyboardAvoidingView>
                    </ScrollView>
                </SafeAreaView>
                {this._displayLoading()}
            </Fragment>
        );
    }
}
export default Inscription