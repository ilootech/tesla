import React, {Component,Fragment}  from 'react'
import {
    Platform,
    Keyboard,
    Alert,
    SafeAreaView, StatusBar,View,Text,ScrollView, ActivityIndicator,
    TouchableOpacity,TextInput,Image,ImageBackground,KeyboardAvoidingView} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image';
import  LinearGradient from 'react-native-linear-gradient'
import ImagePicker from 'react-native-image-picker';
import {connect} from 'react-redux'
import NetInfo from "@react-native-community/netinfo"
import AsyncStorage from "@react-native-community/async-storage"

import UserHelpers from "../../../apis/helpers/user_helpers"
import constantMsg from "../../../configs/constantMsg"
import colors from "../../../configs/colors"
import Loading from "../../../components/Loading/Loading"
import HeaderAuthen from "../../../components/HeaderAuthen/HeaderAuthen"
import styles from "./styles"
import DimScreen from '../../../configs/DimScreen';
class ModifierProfil extends Component 
{
    static navigationOptions = {
        header: null,
    };
    constructor(props){
        super(props)
         this.state={
            keybordTopIOS:0,
            isloading:false,
            choixInsription:0,

            nom:this.props.userInfos.first_name,
            prenom:this.props.userInfos.last_name,
            email:this.props.userInfos.mail,
            tel:this.props.userInfos.phone,
            password:"",
            newpassword:"",
            hidepassword:true
        
        }

        this.options = 
        {
            title: 'Choisissez votre photo de profil',
            takePhotoButtonTitle:'Prendre une photo',
            chooseFromLibraryButtonTitle:'Choisir dans la galerie',
            cancelTitle:'Annuler',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
    }

     /**
     * ***** Fonction de test si le nom et prenom ne contient pas un donne NUMERIQUE ******
     * **/
    _testNonnum=async(value)=>
    {
        console.log("_testNonnum")
        var reg_alphab =/[a-zA-Z]\d+/ ///^[A-Za-z]+$/;
        if (await reg_alphab.test(value)) return true
        return false 

    }
     /** 
     *Test Adresse email Valide
    **/
    _testEmail=async(value)=>
    {
        if (value.length <= 255 && /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value)) 
        {
            return true
        }else
        {
            return false
        }
    }
    /** Show Slider barre */
    _clickShowSLider=()=>
    {
        this.props.navigation.openDrawer()  
    }
     /*
    CLick update Profil
    */ 
    _clickUpdateProfil=async()=>
    {

        NetInfo.isConnected.fetch().then(async (isConnected) => 
        {
            if(isConnected)
            {

                if(this.state.nom=="" && this.state.prenom==""  && this.state.email==""  )
                {
                    await  Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_rempirChampsvide,
                        [
                            {
                                text: "Ok",
                                onPress: () => {
                                }
                            }
                        ]
                    )
                }
                else if(this.state.nom=="")
                {
                    await  Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_entrerNom,
                        [
                            {
                                text: "Ok",
                                onPress: () =>
                                {
                                    this.refs.nomInput.focus()
                                }
                            }
                        ]
                    )
                }
                else if(this.state.prenom=="")
                {
                    await  Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_entrerPrenom,
                    [
                        {
                            text: "Ok",
                            onPress: () => 
                            {
                               this.refs.prenomInput.focus()
                            }
                        }
                    ]
                    )
                }
                else if(this.state.email=="")
                {
                    await  Alert.alert(
                        constantMsg.app_name,
                        constantMsg.msg_entrerEmail,
                       [
                           {
                               text: "Ok",
                               onPress: () => 
                               {
                                 this.refs.emailInput.focus()
                               }
                           }
                       ]
                   )
                }     
                else if(!(this.state.email.length <= 255 && /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.email)))
                {
                     await  Alert.alert(
                        constantMsg.app_name,
                       constantMsg.msg_alertEmailInvalid,
                       [
                           {
                               text: "Ok",
                               onPress: () => 
                               {
                                 this.refs.emailInput.focus()
                               }
                           }
                       ]
                   )
                }
                else if( this.state.password!="" &&  this.state.newpasswor!="" && this.state.password == this.state.newpassword) 
                {
                       await  Alert.alert(
                       constantMsg.app_name,
                       constantMsg.msg_PwdIdentique,
                       [
                           {
                               text: "Ok",
                               onPress: () =>
                               {
                                this.refs.passwordInput.focus()
                               }
                           }
                       ]
                   )
                }
                else
                {
                    if(this.state.password!="" && this.state.password.length<8)
                    {

                        await  Alert.alert(
                            constantMsg.app_name,
                            constantMsg.msg_invalidpwdMoins8caract,
                        [
                            {
                                text: "Ok",
                                onPress: () => 
                                {
                                    this.refs.passwordInput.focus()
                                }
                            }
                        ]
                        )
                    }
                    else if(this.state.newpassword!="" && this.state.newpassword.length<8)
                    {

                            await  Alert.alert(
                                constantMsg.app_name,
                                constantMsg.msg_invalidpwdMoins8caract,
                                [
                                {
                                    text: "Ok",
                                    onPress: () => 
                                    {
                                        this.refs.confirmpasswordInput.focus()
                                    }
                                }
                                ]
                            )
                    }else
                    {
                      
                        this.setState({isloading:true})
                        await  UserHelpers.updateAccountHelpers(this.props.userInfos.id_User,this.state.nom,this.state.prenom,this.state.email,this.state.tel,this.state.newpassword,this.state.password)
                        .then(async(data)=>
                        {
                                this.setState({isloading:false})
                                console.log("res mise a jrs::::"+JSON.stringify(data))
                                const action = { type: "ADD_USERCONNECT", value:data.info_utilisateur}
                                await  this.props.dispatch(action) 
                                await AsyncStorage.setItem('userInfos',JSON.stringify(data.info_utilisateur)) 
                                if(data.etat_edition="profile-a-jour")
                                {
                                    await  Alert.alert(constantMsg.app_name,constantMsg.msg_profilAjrs,
                                    [
                                    {
                                        text: "Ok",
                                        onPress: () => 
                                        {
                                        }
                                    }
                                    ]
                                    ) 
                                }else
                                {

                                }
                                  
                        })
                    }
            
                }
            }
            else
            {
                await Alert.alert(
                    constantMsg.app_name,
                    constantMsg.msg_offline,
                    [
                        {
                            text: "Ok",
                            onPress:async () =>
                            {
                                
                            }
                        }
                    ])
            }
        })
    }

     _displayLoading=()=>
    {
        if(this.state.isloading)
        {
            return <Loading/>
        }else
        {
            return null
        }
    }
      /* Hidden Password
    **/
   _hiddenPassword=()=>
   {
       if(this.state.hidepassword)
       {
           this.setState({hidepassword:false})
       }else
       {
           this.setState({hidepassword:true})
       }
   }
   /** Btn upload Image-piker */
    _clickImagePiker=()=>
    {
        NetInfo.isConnected.fetch().then(async(isConnected) => 
        {
            if(isConnected)
            {
                console.log("Image PIKER")
                ImagePicker.showImagePicker(this.options, (response) => {
                    // console.log('Response = ', response);
                
                    if (response.didCancel) {
                    console.log('User cancelled image picker');
                    } else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                    } else if (response.customButton) {
                    console.log('User tapped custom button: ', response.customButton);
                    } else 
                    {
                        

                    this.setState({isloading:true});
                    UserHelpers.changeProfilUserHelpers(this.props.userInfos.id_User,response)
                    .then(async(data)=>
                    {
                        console.log("PROFIL USER CHANGE INFO:"+JSON.stringify(data))
                        if(data.status=="success"){
                            UserHelpers.updateAccountHelpers(this.props.userInfos.id_User).then(async(dataInfo)=>
                            {
                                    console.log("res mise a jrs::::"+JSON.stringify(dataInfo))
                                    const action = { type: "ADD_USERCONNECT", value:dataInfo.info_utilisateur}
                                    await  this.props.dispatch(action) 

                            })
                            this.setState({isloading:false})
                        }
                        this.setState({isloading:false});
                    })
                    }
                });
            }else
            {
               await  Alert.alert(constantMsg.app_name,constantMsg.msg_offline,
                [
                {
                    text: "Ok",
                    onPress: () => 
                    {
                    }
                }])
            }
        })
    }

    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView style={styles.SafeAreaViewBody}>
                    <View style={styles.containerHeader}>
                        <ImageBackground
                                style={styles.fondHeader}
                                source={require("../../../assets/images/fond/bgFondHeader.png")}>
                                    <View style={styles.header}>
                                        <TouchableOpacity style={styles.btnPrecedent} onPress={()=>{this.props.navigation.pop()}} >
                                            <AutoHeightImage
                                                source={require('../../../assets/images/profil/precedent2.png')}
                                                width={DimScreen.widthScreen * 0.08} 
                                                        style={styles.imgmapImgRue}/>
                                        </TouchableOpacity>
                                    </View>
                                    
                        </ImageBackground>
                    </View>
                    <View style={styles.bodyPanel}>
                    <ScrollView keyboardShouldPersistTaps='handled'  contentContainerStyle={styles.scrollAuthloading}  contentInsetAdjustmentBehavior="automatic">
                    <KeyboardAvoidingView behavior="position" enabled     keyboardVerticalOffset={Platform.OS==="ios"?this.state.keybordTopIOS:-DimScreen.heightScreen*0.58 }>
                        <View style={styles.container}>
                                <Text style={styles.textMonProfil}>MON PROFIL</Text>
                                <View style={styles.bodyProfil}>
                                    
                                   <TouchableOpacity 
                                   style={styles.imgProfil}
                                   onPress={()=>{this._clickImagePiker()}} >
                                        <AutoHeightImage
                                             source={{uri:this.props.userInfos.avatar}} 
                                            width={DimScreen.widthScreen * 0.28} 
                                            // width={100} 
                                            style={[styles.imgProfil],{borderRadius:50}}/>
                                   </TouchableOpacity>
                                    <View style={styles.bodyFormulaire}>
                                        <View style={styles.viewInput}>
                                            <Text style={styles.labelInsription}>Nom</Text>
                                            <TextInput ref="nomInput"  value={this.state.nom}  placeholder="Votre nom"   onChangeText={(text) => { this.setState({ nom: text })}}  onFocus={()=>{ this.setState({keybordTopIOS:-DimScreen.heightScreen*0.58}) }} style={styles.inputText}/>
                                        </View>
                                        <View style={styles.viewInput}>
                                            <Text style={styles.labelInsription}>Prénom</Text>
                                            <TextInput  ref="prenomInput" placeholder="Votre prénom" value={this.state.prenom}   onChangeText={(text) => { this.setState({ prenom: text })}}  onFocus={()=>{ this.setState({keybordTopIOS:-DimScreen.heightScreen*0.58}) }} style={styles.inputText}/>
                                        </View>
                                        <View style={styles.viewInput}>
                                            <Text style={styles.labelInsription}>Email</Text>
                                            <TextInput 
                                            ref="emailInput"
                                            editable={false}
                                            placeholder="Votre adresse email" 
                                            value={this.state.email}   
                                            onChangeText={(text) => { this.setState({ email: text })}}  
                                            onFocus={()=>{ this.setState({keybordTopIOS:-DimScreen.heightScreen*0.42}) }}  
                                            style={styles.inputText}/>
                                        </View>
                                        <View style={styles.viewInput}>
                                            <Text style={styles.labelInsription}>Tel</Text>
                                            <TextInput ref="telInput"  placeholder="Votre tel" value={this.state.tel}  keyboardType="phone-pad"   onChangeText={(text) => { this.setState({ tel: text })}}  onFocus={()=>{ this.setState({keybordTopIOS:-DimScreen.heightScreen*0.3}) }}  style={styles.inputText}/>
                                        </View>
                                        <View style={styles.viewInput}>
                                            <Text style={styles.labelInsription}>Mot de passe</Text>
                                            <View style={styles.inputPassword}>
                                                    <TextInput ref="passwordInput"    placeholder="*******"  secureTextEntry={this.state.hidepassword}  onChangeText={(text) => { this.setState({ password: text })}}  onFocus={()=>{ this.setState({keybordTopIOS:-DimScreen.heightScreen*0.18}) }} style={styles.inputText}/>
                                                   <TouchableOpacity onPress={()=>{this._hiddenPassword()}}>
                                                    <AutoHeightImage
                                                        source={require('../../../assets/images/Authent/yeuxLogin.png')}
                                                        width={DimScreen.widthScreen * 0.045} />
                                                   </TouchableOpacity>
                                                   
                                            </View>
                                        </View> 
                                        <View style={styles.viewInput}>
                                            <Text style={styles.labelInsription}>Nouveau Mot de passe</Text>
                                            <TextInput ref="newpasswordInput"  placeholder="*******"  onChangeText={(text) => { this.setState({ newpassword: text })}}  onFocus={()=>{ this.setState({keybordTopIOS:-DimScreen.heightScreen*0.17}) }}  style={styles.inputText}/>
                                        </View> 

                                
                                        <TouchableOpacity style={styles.btnDeconnexion} onPress={()=>{this._clickUpdateProfil()}} >
                                            <LinearGradient
                                                    colors={[colors.colorBtnStart,colors.colorBtnEnd]} 
                                                    start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                                                    style={styles.linearGradienDeconnexion}
                                                    >
                                                <Text style={styles.textbtnDeconnexion}>APPLIQUEZ LE CHANGEMENT</Text>
                                            </LinearGradient>    
                                        </TouchableOpacity>
                                    </View>            
                                </View>
                        </View>
                       </KeyboardAvoidingView>    
                    </ScrollView>  
                   </View>   
                </SafeAreaView>
                 {this._displayLoading()} 
            </Fragment>
        );
    }
}

const mapStateProps= (state)=>
{
  return {
    userInfos:state.userInfos,
    }
}
export default connect(mapStateProps)(ModifierProfil)