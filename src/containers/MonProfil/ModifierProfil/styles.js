import { StyleSheet,Platform } from 'react-native';
import DimScreen from "../../../configs/DimScreen"
import colors from "../../../configs/colors"
export default styles = StyleSheet.create({

    containerHeader: {
        height:DimScreen.heightScreen*0.3,
        width:"100%", 
    },
    fondHeader:
    {
        flex:1,
        alignItems:"center"
    },
    btnShowSlider:
    {
        marginLeft:DimScreen.widthScreen*0.025,
        marginTop:DimScreen.widthScreen*0.03,
    },
    textBtnModifier:
    {
        fontFamily:"Lato-Black",
        fontSize:DimScreen.widthScreen*0.032,
    },
    imgProfil:
    {
        borderColor:colors.white,
        borderWidth:DimScreen.widthScreen*0.01,
        borderRadius:(DimScreen.widthScreen * 0.28)/2
    },  
    bodyFormulaire:
    {
        backgroundColor:colors.white,
        width:"90%",
        position:"relative",
        marginTop:DimScreen.heightScreen*0.04,
        marginBottom:DimScreen.heightScreen*0.08,
        marginLeft:DimScreen.widthScreen*0.1,   
        paddingBottom:DimScreen.heightScreen*0.02,
        paddingLeft:DimScreen.widthScreen*0.05,
        paddingRight:DimScreen.widthScreen*0.05,
        marginRight:DimScreen.widthScreen*0.1,
        flex:1,
        borderWidth: 0,
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 4,
        alignItems:"center",
    },
    textMonProfil:{
        fontFamily:"Lato-Black",
        fontSize:DimScreen.widthScreen*0.07,
        marginTop:DimScreen.heightScreen*0.015,
        marginBottom:DimScreen.heightScreen*0.038,
        color:colors.white,
    },

    
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    keybordViewProfil:
    {
        flex: 1,
    },
    header:
    {
        width:"100%"
    },
    btnPrecedent:
    {
        marginRight:DimScreen.widthScreen*0.025,
        marginTop:DimScreen.widthScreen*0.03,
    },
    btnShowSlider:
    {
        marginLeft:DimScreen.widthScreen*0.03,
        marginTop:DimScreen.widthScreen*0.03,
    },
    SafeAreaViewBody:
    {
        flex:1,
    },
    bodyPanel:
    {
        position:"relative",zIndex:20,flex:1, marginTop:-DimScreen.heightScreen*0.23
    },
   
    scrollAuthloading:
    {
       /* justifyContent:"center",
        width:"100%",*/
       /* position:'absolute',*/
       width:"100%",
       marginLeft:0,
       marginRight:0,
    },
    bodyProfil:
    {  
        marginLeft:DimScreen.widthScreen*0.05,
        marginRight:DimScreen.widthScreen*0.05,
        width:"95%",
        flex:1,
        alignItems:"center",
    },
    viewInput:
    {
        marginTop:DimScreen.heightScreen*0.045,
        width:"100%",
        alignItems:"flex-start",
        borderBottomColor:colors.vertBorderColor,
        borderBottomWidth:DimScreen.widthScreen*0.003,
        flexDirection:"column"
    },
    labelInsription:
    {
        color:colors.vertfonce,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Black",
    },
    inputPassword:
    {
        flexDirection:"row",
        alignItems:"center",
        marginRight:DimScreen.widthScreen*0.01,
    },
    inputText:
    {
        height:DimScreen.heightScreen*0.05,
        color:colors.gris1Font,
        fontSize:DimScreen.widthScreen*0.04,
        padding:0,
        flex:1,
        fontFamily:"Lato-Medium",
    },
    btnInscri:
    {
        marginTop:DimScreen.heightScreen*0.045,
        width:"75%",
        height:DimScreen.heightScreen*0.06,
    },







    btnDeconnexion:
    {
        width:"85%",
        height:DimScreen.heightScreen*0.06,
        marginTop:DimScreen.heightScreen*0.024,
        marginBottom:DimScreen.heightScreen*0.024,
    },
    linearGradienDeconnexion:
    {
        width:"100%",
        alignItems:"center",
        justifyContent:"center",
        height:"100%",
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 2,
    },
    textbtnDeconnexion:
    {
        color:colors.white,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Bold",
    }
  

})