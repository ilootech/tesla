import React, { Component, Fragment } from 'react'
import {
    Alert,
    SafeAreaView, StatusBar, View, Text, ScrollView, ActivityIndicator,
    TouchableOpacity, TextInput, Image, ImageBackground
} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image'
import LinearGradient from 'react-native-linear-gradient'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage';

import constantMsg from "../../configs/constantMsg"
import colors from "../../configs/colors"
import HeaderAuthen from "../../components/HeaderAuthen/HeaderAuthen"
import styles from "./styles"
import DimScreen from '../../configs/DimScreen';
class MonProfil extends Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props)

    }
    /** 
     * Show Slider barre
     **/
    _clickShowSLider = () => {
        this.props.navigation.openDrawer()
    }
    /** 
   * Deconnection app Tesla
   **/
    _logout = async () => {
        Alert.alert(constantMsg.app_name, constantMsg.msg_AlertLogout,
            [
                {
                    text: 'Oui',
                    onPress: async () => {
                        await AsyncStorage.clear();
                        const action = { type: "INIT", value: "" }
                        await this.props.dispatch(action)
                        await this.props.navigation.navigate("RootNav")
                    }
                },
                {
                    text: 'Non',
                    onPress: () => {

                    }
                },
            ],
            { cancelable: false }
        )
    }

    /** Click Menue Btn **/
    clickMenue = async (value) => {

        const _action = { type: "ADD_MENUE_ACTIF", value: value }
        await this.props.dispatch(_action)
        switch (value) {
            case "mesResa":
                this.props.navigation.navigate("MesreservationsPage")
                break;
            case "Parrainage":
                this.props.navigation.navigate("ParrainagePage")
                break;
            case "Paiement":
                this.props.navigation.navigate("PaiementPage")
                break;
        }
    }
    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView style={styles.containerSafeAreaView}>
                    <View style={styles.containerHeader}>
                        <ImageBackground
                            style={styles.fondHeader}
                            source={require("../../assets/images/fond/bgFondHeader.png")}>
                            <View style={styles.header}>
                                <TouchableOpacity onPress={() => { this._clickShowSLider() }}>
                                    <AutoHeightImage
                                        source={require('../../assets/images/Slider/imgMenueShow.png')}
                                        width={DimScreen.widthScreen * 0.062}
                                        style={styles.btnShowSlider}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate("ModifierProfilPage") }}>
                                    <Text style={styles.textBtnModifier}>Modifier</Text>
                                </TouchableOpacity>
                            </View>
                        </ImageBackground>
                    </View>
                    <View style={styles.bodyPanel}>
                        <ScrollView contentContainerStyle={styles.scrollAuthloading} contentInsetAdjustmentBehavior="automatic">
                            <View style={styles.container}>

                                <Text style={styles.textMonProfil}>MON PROFIL</Text>
                                <View style={styles.bodyProfil}>
                                    <AutoHeightImage
                                        source={{ uri: this.props.userInfos.avatar }}
                                        width={DimScreen.widthScreen * 0.28}
                                        style={styles.imgProfil} />

                                    <Text style={styles.textNomPrenom}>{this.props.userInfos.last_name} {this.props.userInfos.first_name} </Text>
                                    <Text style={styles.textTelEmail}>{(this.props.userInfos.phone == undefined || this.props.userInfos.phone == "") ? this.props.userInfos.mail : this.props.userInfos.phone + " | " + this.props.userInfos.mail}</Text>
                                    {/* <View style={styles.viewCredits}>
                                        <Text style={styles.labelCredit}>Crédits: </Text><Text style={styles.prixCredit}>20,00 €</Text>
                                    </View> */}
                                    {/* <View style={styles.viewMenueRow} >
                                        <TouchableOpacity onPress={() => { this.clickMenue("mesResa") }} style={[styles.btnMenueProfil, styles.btnMenueProfilLeft]} >
                                            <Image style={styles.imgBgMenueProfil} source={require('../../assets/images/profil/reservation.png')} />
                                            <Text style={styles.textMenueProfil} >MES RESERVATIONS</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => {
                                            // this.clickMenue("Parrainage")
                                        }} style={styles.btnMenueProfil} >
                                            <Image style={styles.imgBgMenueProfil} source={require('../../assets/images/profil/parrainage.png')} />
                                            <Text style={styles.textMenueProfil} >PARRAINAGE</Text>
                                        </TouchableOpacity>
                                    </View> */}
                                    {/* <View style={styles.viewMenueRow} >
                                        <TouchableOpacity style={[styles.btnMenueProfil, styles.btnMenueProfilLeft]} >
                                            <Image style={styles.imgBgMenueProfil} source={require('../../assets/images/profil/mes_favories.png')} />
                                            <Text style={styles.textMenueProfil} >MES PASSAGERS</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.btnMenueProfil} >
                                            <Image style={styles.imgBgMenueProfil} source={require('../../assets/images/profil/mes_favories.png')} />
                                            <Text style={styles.textMenueProfil} >MES FAVORIS</Text>
                                        </TouchableOpacity>
                                    </View> */}
                                    {/* <View style={styles.viewMenueRow} >
                                        <TouchableOpacity onPress={() => {
                                        }} style={[styles.btnMenueProfil, styles.btnMenueProfilLeft]} >
                                            <Image style={styles.imgBgMenueProfil} source={require('../../assets/images/profil/paiement.png')} />
                                            <Text style={styles.textMenueProfil} >PAIEMENT</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.btnMenueProfil} >
                                        </TouchableOpacity>
                                    </View> */}




                                    <TouchableOpacity style={styles.btnDeconnexion} onPress={() => { this._logout() }} >
                                        <LinearGradient
                                            colors={[colors.colorBtnStart, colors.colorBtnEnd]}
                                            start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                            style={styles.linearGradienDeconnexion}
                                        >
                                            <Text style={styles.textbtnDeconnexion}>{"Déconnexion".toUpperCase()}</Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                    </View>

                </SafeAreaView>
            </Fragment>
        );
    }
}
const mapStateProps = (state) => {
    return {
        userInfos: state.userInfos,
    }
}
export default connect(mapStateProps)(MonProfil)