import React, {Component,Fragment}  from 'react'
import {SafeAreaView, StatusBar,View,Text,ScrollView, ActivityIndicator,
    TouchableOpacity,TextInput,Image,ImageBackground} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image'
import  LinearGradient from 'react-native-linear-gradient'
import {connect} from 'react-redux'
import colors from "../../configs/colors"
import HeaderPage from "../../components/HeaderPage/HeaderPage"
import styles from "./styles"
import DimScreen from '../../configs/DimScreen';
class Parrainage extends Component 
{
    static navigationOptions = {
        header: null,
    };
    constructor(props){
        super(props)
    }
    /** Show Slider barre */
    _clickShowSLider=()=>
    {
        this.props.navigation.openDrawer()  
    }
    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView style={styles.containerSafeAreaView}>
                    <HeaderPage _clickShowSLider={this._clickShowSLider}/>
                    <View style={styles.bodyPanel}>
                    <ScrollView contentContainerStyle={styles.scrollAuthloading}  contentInsetAdjustmentBehavior="automatic">
                        <View style={styles.container}>
                                <Text style={styles.textMonProfil}>PARRAINAGE</Text>
                                <View style={styles.bodyFormulaire}>
                                        <View style={styles.panelVotreCodeParrainage}>
                                            <Text style={styles.labelVotreCodeParrainage}>Votre code parrainage :</Text>
                                            <TextInput style={styles.textInputCodeParra} />
                                        </View>
                                        
                                        <View style={styles.viewRowParrainage}>
                                            <AutoHeightImage
                                                source={require('../../assets/images/Slider/parraignage.png')}
                                                width={DimScreen.widthScreen * 0.09} 
                                                style={styles.btnShowSlider}
                                                />
                                            <Text style={styles.textlist} >Invitez vos amis à essayer VTC avec 4.00 € de reduction</Text> 
                                        </View>
                                        <View style={styles.viewRowParrainage}>
                                            <AutoHeightImage
                                                source={require('../../assets/images/Slider/paiement-green.png')}
                                                width={DimScreen.widthScreen * 0.09} 
                                                style={styles.btnShowSlider}
                                                />
                                            <Text  style={styles.textlist} >Recevez 4.00 € de crédit après leur première course.</Text> 
                                        </View>
                                        <View style={styles.viewRowParrainage}>
                                            <AutoHeightImage
                                                source={require('../../assets/icones/course-vert-icon.png')}
                                                width={DimScreen.widthScreen * 0.09} 
                                                style={styles.btnShowSlider}
                                                />
                                            <Text  style={styles.textlist} >Uniquement si vous avez effectué et terminé au moins une 1ère course</Text> 
                                        </View>
                                        <TouchableOpacity style={styles.btnPartage} onPress={()=>{this.props.navigation.navigate("Inscription")}} >
                                            <LinearGradient
                                                    colors={[colors.colorBtnStart,colors.colorBtnEnd]} 
                                                    start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                                                    style={styles.linearGradienPartage}
                                                    >
                                                <Text style={styles.textbtnPartage}>PARTAGER</Text>
                                            </LinearGradient>    
                                        </TouchableOpacity>

                                </View> 
                        </View>
                    </ScrollView>  
                    </View>  
                </SafeAreaView>
            </Fragment>
        );
    }
}
export default Parrainage