import React, { Component, Fragment } from 'react'
import {
    StatusBar, View, Text, ScrollView,
    TouchableOpacity, TextInput, ImageBackground
} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image'
import { connect } from 'react-redux'
import MapView, { AnimatedRegion, Animated } from 'react-native-maps';
import LinearGradient from 'react-native-linear-gradient'
import colors from "../../../configs/colors"

import styles from "./styles"
import DimScreen from '../../../configs/DimScreen';
import VoitureHelpers from '../../../apis/helpers/voiture_helpers';
import Loading from '../../../components/Loading/Loading';
class ChoixVoiture extends Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props)
        this.state = {
            optionVoiture: true,
            listVoiture: [],
            show: false,
        }
    }
    _clickAddChoixVoiture = async (item) => {


        this.setState({ show: true })
        // let response = await VoitureHelpers.getListVoiture()
        // this.setState({ listVoiture: response })
        const action = { type: "ADD_CHOIXVOITURE", value: item }
        await this.props.dispatch(action)
        this.setState({ show: false })
        this.props.clickCloseModalVoiture()
    }
    componentDidMount = async () => {
        this.setState({ show: true })
        let response = await VoitureHelpers.getListVoiture()
        this.setState({ listVoiture: response })
        this.setState({ show: false })
    }
    render() {
        return (
            <TouchableOpacity style={styles.windowChoixVoiturees} onPress={() => { this.props.clickCloseModalVoiture() }}>
                <ScrollView contentContainerStyle={styles.scrollBody} contentInsetAdjustmentBehavior="automatic">
                    <View style={styles.panelVoitures}>
                        <View style={styles.panelTitre}>
                            <Text style={styles.textTitre}>SÉLECTIONNEZ</Text>
                            <Text style={styles.textTitre} >UN VÉHICULE</Text>
                        </View>
                        {this.state.show ?
                            <Loading /> : null
                        }
                        {
                            // this.props.listTypeVoiture.map((item, index) => 
                            this.state.listVoiture.map((item, index) => {
                                return ((item.id % 2) == 0 ?
                                    <TouchableOpacity style={styles.btnbgListesVoiture} onPress={() => { this._clickAddChoixVoiture(item)/*this.props.clickChoixVoiture(1)*/ }}>
                                        <ImageBackground style={styles.bgListesVoiture} source={require("../../../assets/images/reservation/bgVoiture1.jpg")}>
                                            <AutoHeightImage
                                                source={{ uri: item.image }}
                                                width={DimScreen.widthScreen * 0.5}
                                            />
                                            <View style={styles.viewdescription}>
                                                <Text style={styles.nameVoitures}>{item.type}</Text>
                                                <View style={styles.viewPlaceVoitures}>
                                                    <AutoHeightImage
                                                        source={require('../../../assets/images/reservation/chaise.png')}
                                                        width={DimScreen.widthScreen * 0.03}
                                                    />
                                                    <Text style={styles.nbrPlaceVoitures}>{item.places} PLACES</Text>
                                                </View>
                                            </View>
                                        </ImageBackground>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity style={styles.btnbgListesVoiture} onPress={() => { this._clickAddChoixVoiture(item) }}>
                                        <ImageBackground style={styles.bgListesVoiture} source={require("../../../assets/images/reservation/bgVoiture2.jpg")}>

                                            <View style={styles.viewdescription}>
                                                <Text style={styles.nameVoitures2}>{item.type}</Text>
                                                <View style={styles.viewPlaceVoitures}>
                                                    <AutoHeightImage
                                                        source={require('../../../assets/images/reservation/chaise-green.png')}
                                                        width={DimScreen.widthScreen * 0.03}
                                                    />
                                                    <Text style={styles.nbrPlaceVoitures2}>{item.places} PLACE</Text>
                                                </View>
                                            </View>
                                            <AutoHeightImage
                                                source={{ uri: item.image }}
                                                width={DimScreen.widthScreen * 0.5}
                                            />
                                        </ImageBackground>
                                    </TouchableOpacity>)
                            })
                        }
                    </View>
                </ScrollView>
            </TouchableOpacity>)
    }
}
const mapStateProps = (state) => {
    return {
        choixLocVoiture: state.choixLocVoiture,
        listTypeVoiture: state.listTypeVoiture,
    }
}
export default connect(mapStateProps)(ChoixVoiture)