import React, {Component,Fragment}  from 'react'
import {SafeAreaView, StatusBar,View,Text,ScrollView, ActivityIndicator,
    TouchableOpacity,TextInput,Image,ImageBackground} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image'
import  LinearGradient from 'react-native-linear-gradient'
import {connect} from 'react-redux'
import colors from "../../../configs/colors"
import HeaderPage from "../../../components/HeaderPage/HeaderPage"
import styles from "./styles"
import DimScreen from '../../../configs/DimScreen';
class VotreCourse extends Component 
{
    static navigationOptions = {
        header: null,
    };
    constructor(props){
        super(props)
    }
    /** Show Slider barre */
    _clickShowSLider=()=>
    {
        this.props.navigation.openDrawer()  
    }
    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView style={styles.containerSafeAreaView}>
                    <HeaderPage _clickShowSLider={this._clickShowSLider}/>
                    <View style={styles.bodyPanel}>
                    <ScrollView contentContainerStyle={styles.scrollAuthloading}  contentInsetAdjustmentBehavior="automatic">
                        <View style={styles.container}>
                                <Text style={styles.textTitrePage}>VOTRE COURSE</Text>
                                <View style={styles.bodyFormulaire}>                                
                               

                                </View> 
                        </View>
                    </ScrollView>  
                    </View>  
                </SafeAreaView>
            </Fragment>
        );
    }
}
export default VotreCourse