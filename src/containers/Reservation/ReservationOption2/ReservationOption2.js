import React, {Component,Fragment}  from 'react'
import {SafeAreaView, StatusBar,View,Text,ScrollView, ActivityIndicator,
    TouchableOpacity,TextInput} from 'react-native'
    import {connect} from 'react-redux'
import AutoHeightImage from 'react-native-auto-height-image'
import MapView ,{ AnimatedRegion, Animated } from 'react-native-maps';
import  LinearGradient from 'react-native-linear-gradient'
import colors from "../../../configs/colors"
import ChoixVoiture from "../ChoixVoiture/ChoixVoiture"
import ModalEntreeCode from "../ModalEntreeCode/ModalEntreeCode"
import styles from "./styles"
import DimScreen from '../../../configs/DimScreen';
class ReservationOption2 extends Component 
{
    static navigationOptions = {
        header: null,
    };
    constructor(props){
        super(props)
        this.state={
            optionVoiture:true,
            modalChoixVoiture:false,
            modalValidation:false,
            showModalCode:false,
        }
    }
    clickCloseModalCode=()=>
    {
        this.setState({showModalCode:false})
    }
    _clickOptionVoiture=(value)=>
    {
        this.setState({
            optionVoiture:value
        })
    }
    clickCloseModalVoiture=(value)=>
    {
        this.setState({modalChoixVoiture:false})
    }
    clickChoixVoiture=()=>
    {
        this.setState({modalChoixVoiture:false})
    }
    clickCloseModalValidation=()=>{
        this.setState({modalValidation:false})
    }
    _displayShowModalVoiture=()=>
    {
        if(this.state.modalChoixVoiture==true)
        {
            return  <ChoixVoiture clickChoixVoiture={this.clickChoixVoiture} clickCloseModalVoiture={this.clickCloseModalVoiture} />
        }else
        {
            return null
        }
    }
    _displayValidation=()=>
    {
        if(this.state.modalValidation)
        {
            return <Validation clickCloseModalValidation={this.clickCloseModalValidation} />
        }else
        {
            return null
        }
    }

    _displayModalEntreeCode=()=>
    {
        if(this.state.showModalCode)
        {
            return <ModalEntreeCode clickCloseModalCode={this.clickCloseModalCode}/> 
        }else
        {
            return null
        }
    }
    _clickOptionPromo=()=>
    {
        this.setState({
            optionVoiture:false,
            showModalCode:true,
        })
    }
    
    _clickDepart=async()=>
    {
        await this.props.navigation.navigate("LieuxPage",{optionChoix:"Départ"})
    }
    _clickArriver=async()=>
    {
        await this.props.navigation.navigate("LieuxPage",{optionChoix:"Arriver"})
    }
    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView>
                    <ScrollView contentContainerStyle={styles.scrollBody}  contentInsetAdjustmentBehavior="automatic">
                        {
                        this.props.region==null?
                         <View style={styles.mapResa}></View>
                        :
                        <MapView
                        initialRegion={{
                        latitude:this.props.region.latitude,
                        longitude:this.props.region.longitude,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                        }}
                        style={styles.mapResa}/>      
                            }
                        <View style={styles.viewBtnPrec}>
                            <TouchableOpacity onPress={()=>{this.props.navigation.pop()}} >
                                <AutoHeightImage
                                    source={require('../../../assets/images/reservation/btnprecedent.png')}
                                    width={DimScreen.widthScreen * 0.08} 
                                            style={styles.imgmapImgRue}/>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.bodyResa}>
                                    <View style={styles.panelProximiter}>
                                        <View style={{flex:1}}></View>
                                        <TouchableOpacity  style={styles.viewProximiter}> 
                                            <AutoHeightImage
                                                source={require('../../../assets/images/reservation/mapImgRue.png')}
                                                width={DimScreen.widthScreen * 0.052} 
                                                style={styles.imgmapImgRue}/>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.panelboxRecherche}>
                                        <View style={styles.viewLieuxboxRecherche}>
                                            <View style={styles.viewLieuxbox1}>
                                            <TouchableOpacity  onPress={()=>{this._clickDepart()}}>
                                                    <LinearGradient
                                                        colors={[colors.colorBtnStart,colors.colorBtnEnd]} 
                                                        start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                                                        style={styles.linearGradientInput}>
                                                        <AutoHeightImage
                                                            source={require('../../../assets/images/reservation/maps.png')}
                                                            width={DimScreen.widthScreen * 0.035} />
                                                        <Text  style={styles.textInput}  >{this.props.depart==null?"Départ":this.props.depart.description}</Text>
                                                    </LinearGradient>
                                                </TouchableOpacity>
                                               
                                                <TouchableOpacity onPress={()=>{this._clickArriver()}}>
                                                    <LinearGradient
                                                        colors={[colors.black,colors.black]} 
                                                        start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                                                        style={styles.linearGradientInputNoir}>
                                                        <AutoHeightImage
                                                            source={require('../../../assets/images/reservation/maps.png')}
                                                            width={DimScreen.widthScreen * 0.035} />   
                                                        <Text  style={styles.textInput}>{this.props.arriver==null?"Arrivée":this.props.arriver.description}</Text>
                                                    </LinearGradient>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={styles.viewLieuxbox2}>
                                                <TouchableOpacity>
                                                    <AutoHeightImage
                                                        source={require('../../../assets/images/reservation/addResa.png')}
                                                        width={DimScreen.widthScreen * 0.062} 
                                                        style={styles.imgmapImgRue}/>
                                                </TouchableOpacity>
                                            </View>
                                        </View>

                                        <View style={styles.boxOptions}>
                                                <TouchableOpacity  onPress={()=>{this._clickOptionVoiture(true)}} style={[styles.btnOption1,{borderTopColor:this.state.optionVoiture==true?colors.vertBorderColor:colors.transparent}]}><Text style={[styles.textOption1,{color:this.state.optionVoiture==true?colors.vertfonce:colors.gris1Font}]} >Options</Text></TouchableOpacity>
                                                <TouchableOpacity onPress={()=>{this._clickOptionPromo()}}  style={[styles.btnOption2,{borderTopColor:this.state.optionVoiture!=true?colors.vertBorderColor:colors.transparent}]}><Text  style={[styles.textOption2,{color:this.state.optionVoiture==true?colors.gris1Font:colors.vertfonce}]}  >Code promo</Text></TouchableOpacity>
                                        </View> 
                                        <Text style={styles.textSelectModepaie}>Sélectionnez votre mode de paiement</Text>

                                        <View style={styles.panelviewBerline}>
                                            <TouchableOpacity onPress={()=>{this.setState({modalChoixVoiture:true})}}>
                                                <LinearGradient
                                                    colors={[colors.colorBtnStart,colors.colorBtnEnd]} 
                                                    start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                                                    style={styles.linearGradienBerline}>
                                                    <AutoHeightImage
                                                          source={{uri:this.props.choixLocVoiture.image}} 
                                                        width={DimScreen.widthScreen * 0.13} />
                                                    <Text style={styles.textberline}>{this.props.choixLocVoiture.type}</Text>
                                                </LinearGradient>
                                            </TouchableOpacity>
                                            <View style={styles.prixBerline}>
                                                <Text style={styles.labelPrix}>Prix</Text>
                                                <Text  style={styles.textPrix}>{this.props.choixLocVoiture.prix} €</Text>
                                            </View>
                                        </View>
                                        <TouchableOpacity style={styles.btnValider} onPress={()=>{this.setState({modalValidation:true})}}>
                                            <LinearGradient
                                                    colors={[colors.colorBtnStart,colors.colorBtnEnd]} 
                                                    start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                                                    style={styles.linearGradienBerlineValider}>
                                                        <Text style={styles.textValider} >Valider</Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                    </View>
                            </View>
                            {this._displayShowModalVoiture()}
                            {this._displayModalEntreeCode()}
                    </ScrollView>
                </SafeAreaView>
        </Fragment>)
    }
}    
 
const mapStateProps= (state)=>
{
  return {
    arriver:state.arriver,
    depart:state.depart,
    choixLocVoiture:state.choixLocVoiture,
    region:state.region,
    }
}
export default connect(mapStateProps)(ReservationOption2);   