import { StyleSheet } from 'react-native';
import DimScreen from "../../configs/DimScreen"
import colors from "../../configs/colors"
export default styles = StyleSheet.create({
    container: {
        
        alignItems: 'center',
        flex:1,
    },
    scrollAuthloading:
    {
        width:"100%",
        height:DimScreen.heightScreen*0.96
    },
    viewInputRechRue:
    {
        width:"68%",
        flexDirection:"row",
        height:DimScreen.heightScreen*0.062,
        backgroundColor:colors.white,
        alignItems:"center",
        borderWidth: 0,
        borderRadius: DimScreen.heightScreen*0.02,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 4,
        marginTop:DimScreen.heightScreen*0.04,
        zIndex:50,
    },
    imgmapImgRue:
    {
        marginLeft:DimScreen.widthScreen*0.023,
        marginRight:DimScreen.widthScreen*0.015,
    },
    
    textInputRue:
    {
        padding:0,
        fontFamily:"Lato-Regular",  
        fontSize:DimScreen.heightScreen*0.02,
        color:colors.gris1Font,
        width:"82%"
    },
    panelResa:
    {
        backgroundColor:colors.white,
        width:"80%",
        height:DimScreen.heightScreen*0.2,
        zIndex:7,
        position:"absolute",
        marginTop:DimScreen.heightScreen*0.75,
        marginLeft:DimScreen.widthScreen*0.1,
        paddingTop:DimScreen.heightScreen*0.03,
        paddingBottom:DimScreen.heightScreen*0.03,
        paddingLeft:DimScreen.widthScreen*0.05,
        paddingRight:DimScreen.widthScreen*0.05,
        marginRight:DimScreen.widthScreen*0.1,
        borderWidth: 0,
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 4,
        alignItems:"center",
        justifyContent:"space-around"
    },
    mapResa:
    {
        width:"100%",
        // height:DimScreen.heightScreen*0.45,
        // zIndex:7,
        position:"absolute",
        marginTop:DimScreen.heightScreen*0.25,
        height:DimScreen.heightScreen*0.8,
    },
    panelFooterResa:
    {
        backgroundColor:colors.white,
        width:"100%",
        marginTop:DimScreen.heightScreen*0.43,
        height:DimScreen.heightScreen*0.13,
        zIndex:5,
    },
    linearGradienResa:
    {
        height:"100%",
        width:"100%"
    },
    textConnectezVous:
    {
        fontFamily:"Lato-Black",
        color:colors.gris1Font,
        fontSize:DimScreen.widthScreen*0.06,
    },
    labelConnect:
    {
        color:colors.vertfonce,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Black",
    },
    inputPassword:
    {
        flexDirection:"row",
        alignItems:"center",
        marginRight:DimScreen.widthScreen*0.01,
    },
    inputText:
    {
        color:colors.gris1Font,
        fontSize:DimScreen.widthScreen*0.04,
        padding:0,
        flex:1,
        fontFamily:"Lato-Medium",
    },
    viewInputLogin:
    {
        marginTop:DimScreen.heightScreen*0.045,
        width:"100%",
        alignItems:"flex-start",
        borderBottomColor:colors.vertBorderColor,
        borderBottomWidth:DimScreen.widthScreen*0.003
    },
    viewInputPassword:
    {
        marginTop:DimScreen.heightScreen*0.05,
        width:"100%",
        alignItems:"flex-start",
        borderBottomColor:colors.vertBorderColor,
        borderBottomWidth:DimScreen.widthScreen*0.003
    },
    viewPassWordOublier:
    {
        width:"100%",
        alignItems:"flex-end",
        marginTop:DimScreen.heightScreen*0.009,
        marginBottom:DimScreen.heightScreen*0.07,
    },
    textPassWordOublier:
    {
        fontSize:DimScreen.widthScreen*0.038,
        fontFamily:"Lato-Medium",
        color:colors.gris1Font,
    },
    panelBarrOU:
    {
        flexDirection:"row",
        alignItems:"center",
        marginBottom:DimScreen.heightScreen*0.013,
        // marginTop:DimScreen.heightScreen*0.013,
    },
    barrOU:
    {
        width:"30%",
        marginHorizontal:"2%",
        height:DimScreen.widthScreen*0.002,
        backgroundColor:colors.gris1Font,
    },
    textOU:
    {
        marginHorizontal:"2%",
        fontFamily:"Lato-Regular",   
        fontSize:DimScreen.widthScreen*0.03,
    },
    btnLogin:
    {
        width:"75%",
        height:DimScreen.heightScreen*0.06,
    },
    linearGradienLogin:
    {
        width:"100%",
        alignItems:"center",
        justifyContent:"center",
        height:"100%",
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 2,
    },
    textbtnLogin:
    {
        color:colors.white,
        fontSize:DimScreen.widthScreen*0.04,
        fontFamily:"Lato-Bold",
    }
  

})