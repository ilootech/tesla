import React, { Component, Fragment } from 'react'
import {
    SafeAreaView, StatusBar, View, Text, ScrollView, ActivityIndicator,
    TouchableOpacity, TextInput, Image, ImageBackground, Alert
} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image'
import LinearGradient from 'react-native-linear-gradient'
import colors from "../../../configs/colors"
import HeaderPage from "../../../components/HeaderPage/HeaderPage"
import styles from "./styles"
import DimScreen from '../../../configs/DimScreen';
import ReservationHelpers from '../../../apis/helpers/reservation_helpers'
import { connect } from 'react-redux'
import Loading from '../../../components/Loading/Loading'
import PayPal from 'react-native-paypal-wrapper';
import Stripe from "react-native-stripe-api";

import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input";
// import { SelectPayment, AddCard } from "react-native-stripe-checkout";

class PaiementChoice extends Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props)
        this.state = {
            choixReservation: 0,
            data: [],
            show: true,
            Choice: "paypal",
            item: this.props.navigation.getParam("item"),
            Loading: false,
            dataStripe: ""

        }
    }
    /** Show Slider barre */
    _clickShowSLider = () => {
        this.props.navigation.openDrawer()
    }
    componentDidMount = async () => {
        this.setState({ show: false })
        console.log("********************")
        console.log("ITEM ", this.state.item)
        console.log("********************")
    }
    _onChange = (formData) => {
        console.log(formData)
        this.setState({ dataStripe: formData })

    };
    // _onChange => form => console.log(form)

    _onValid = async () => {
        this.setState({ show: true })
        let idUser = this.props.userInfos.id_User
        let type = this.state.Choice
        let amount = this.state.item.prix
        let reservation_id = this.state.item.idResa

        if (this.state.Choice == "paypal") {
            // GET API PAYPAL
            let responseAPIPaypal = await ReservationHelpers.getAPIPaypal()
            let name = ''
            let clientID = ''
            if (responseAPIPaypal.status) {
                this.setState({ show: false })
                Alert.alert("Information", "Une erreur s'est produite lors de l'initialisation de votre Compte Paypal")
            }
            else {
                // Alert.alert("Information", "Une erreur s'est produite lors de l'initialisation de votre Compte Paypal")
                // name = responseAPIPaypal.name
                // clientID = responseAPIPaypal.client_id
                PayPal.initialize(PayPal.SANDBOX, 'AQpptdG2MsPcUToYGwha1ki82rnAVUFp8tVzlMOWrc_vgnUmVPcVf5RyQxddTJC88CBe8HNWOW0YSTr6');
                PayPal.initialize(name, clientID);
                PayPal.pay({
                    price: amount,
                    currency: 'EUR',
                    description: 'Paiement sur SNT:',
                }).then(async (confirm) => {
                    console.log(confirm)
                    let datas = confirm

                    let response = await ReservationHelpers.paiementInsert(idUser, type, amount, reservation_id, datas)
                    console.log("********************")
                    console.log(response)
                    console.log("********************")
                    this.setState({ Loading: false })
                    this.setState({ show: false })
                })
                    .catch(error => {
                        console.log(error)
                        this.setState({ Loading: false })
                        this.setState({ show: false })
                    });
            }

        }
        else {
            let responseAPIStripe = await ReservationHelpers.getAPIStripe()
            if (responseAPIStripe.status) {
                this.setState({ show: false })
                Alert.alert("Information", "Une erreur s'est produite lors de l'initialisation de votre Compte Stripe")
            }
            else {
                let key = responseAPIStripe.public_key_test
                const dataStripe = this.state.dataStripe
                if (this.state.dataStripe == "") {
                    this.setState({ show: false })
                    Alert.alert("Information", "Une erreur s'est produite lors de l'initialisation de votre Compte Stripe")
                }
                else {
                    let expiry = dataStripe.values.expiry.split('/')
                    const apiKey = key;
                    const client = new Stripe(apiKey);
                    const token = await client.createToken({
                        number: dataStripe.values.number,
                        exp_month: expiry[0],
                        exp_year: expiry[1],
                        cvc: dataStripe.values.cvc,
                    });
                    if (token.id) {
                        let tokenId = token.id
                        const responsePaiement = await ReservationHelpers.paiementInsertStripe(tokenId, idUser, reservation_id, amount)
                        console.log(responsePaiement)
                        if (responsePaiement.status == "success") {
                            Alert.alert("Information !", "Paiement effectué", [
                                {
                                    text: "Ok",
                                    onPress: () => {
                                        this.props.navigation.navigate("MesreservationsPage")
                                    }
                                }
                            ])
                        }
                        this.setState({ show: false })
                    }
                    else {
                        Alert.alert("Information !", "Une erreur s'est prosuite lors de la validation de votre carte. Veuillez vérifier et réessayer plus tard")
                        this.setState({ show: false })

                    }
                }

            }
        }

    }
    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                {this.state.show ?
                    <View style={{ flex: 1, alignItems: "center", alignSelf: "center", justifyContent: "center" }}>
                        <Loading />
                    </View> :
                    <SafeAreaView style={styles.containerSafeAreaView}>
                        <HeaderPage _clickShowSLider={this._clickShowSLider} />
                        {/* {this.state.show ?
                            <Loading /> : null
                        } */}
                        <View style={styles.bodyPanel}>
                            <ScrollView contentContainerStyle={styles.scrollAuthloading} contentInsetAdjustmentBehavior="automatic">
                                <View style={styles.container}>
                                    <Text style={styles.textTitre}>PAIEMENT</Text>
                                    <View style={[styles.bodyFormulaire, { justifyContent: "space-around" }]}>
                                        <View style={{ margin: 15, flex: 1 }}>
                                            <Text style={[styles.aucuneCourseAvenir, { color: colors.vertfonce }]}>{"choisissez le mode de paiement".toUpperCase()}</Text>
                                        </View>
                                        {/* <View style={{ flexDirection: "column", justifyContent: "space-around" }}> */}
                                            {/* <AutoHeightImage
                                                width={DimScreen.widthScreen * 0.25}
                                                source={require("../../../assets/icones/paypal.png")}
                                            /> */}
                                            <TouchableOpacity
                                                onPress={() => this.setState({ Choice: "paypal" })}
                                                style={{ flexDirection: "row", width: DimScreen.widthScreen * 0.4, justifyContent: "space-around" }}>
                                                {this.state.Choice == "paypal" ?
                                                    <AutoHeightImage
                                                        width={15}
                                                        style={{alignSelf:"center"}}
                                                        source={require("../../../assets/icones/check_plein.png")}
                                                    /> :
                                                    <AutoHeightImage
                                                        width={15}
                                                        style={{alignSelf:"center"}}
                                                        source={require("../../../assets/icones/check_vide.png")}
                                                    />}
                                                <AutoHeightImage
                                                    width={DimScreen.widthScreen * 0.25}
                                                    style={{alignSelf:"center"}}
                                                    source={require("../../../assets/icones/paypal.png")}
                                                />
                                                {/* <Text style={styles.aucuneCourseAvenir}>Paypal</Text> */}
                                            </TouchableOpacity>

                                        {/* </View> */}
                                        <View style={{margin:10}}></View>
                                        {/* <View style={{ margin: 10, borderBottomColor: "#000", borderBottomWidth: 1, width: "45%", alignItems: "center" }}></View> */}
                                        {/* <LiteCreditCardInput /> */}
                                        {/* <View style={{ margin: 10, borderTopColor: "#000", borderTopWidth: 1, width: "45%", alignItems: "center" }}></View> */}
                                        {/* <View style={{ flexDirection: "column", justifyContent: "space-around" }}> */}
                                            {/* <AutoHeightImage
                                                width={DimScreen.widthScreen * 0.25}
                                                source={require("../../../assets/icones/stripe.png")}
                                            /> */}

                                            <TouchableOpacity
                                                onPress={() => this.setState({ Choice: "stripe" })}
                                                style={{ flexDirection: "row", width: DimScreen.widthScreen * 0.4, justifyContent: "space-around" }}>
                                                {this.state.Choice == "stripe" ?
                                                    <AutoHeightImage
                                                        width={15}
                                                        style={{alignSelf:"center"}}
                                                        source={require("../../../assets/icones/check_plein.png")}
                                                    /> :
                                                    <AutoHeightImage
                                                        width={15}
                                                        style={{alignSelf:"center"}}
                                                        source={require("../../../assets/icones/check_vide.png")}
                                                    />}
                                                {/* <Text style={styles.aucuneCourseAvenir}>Stripe</Text> */}
                                                <AutoHeightImage
                                                    width={DimScreen.widthScreen * 0.25}
                                                    style={{alignSelf:"center"}}
                                                    source={require("../../../assets/images/reservation/CB.png")}
                                                />
                                            </TouchableOpacity>
                                        {/* </View> */}
                                        <View style={styles.panelListeResa}>
                                            <Text style={styles.aucuneCourseAvenir}></Text>
                                        </View>
                                        {
                                            this.state.Choice == "stripe" ?
                                                <LiteCreditCardInput onChange={(form) => this._onChange(form)} /> : null
                                            // LiteCreditCardInput 
                                            // CreditCardInput
                                        }
                                        {/* {this.state.dataStripe.valid ?
                                            <View style={{ flexDirection: "row", marginTop:-20 }}>
                                                <AutoHeightImage
                                                    width={50}
                                                    source={require("../../../assets/icones/iconCheckCertif.png")}
                                                />
                                                <View style={{ alignSelf: "center" }}>
                                                    <Text style={styles.aucuneCourseAvenir}>Compte Stripe Validé</Text>
                                                </View>
                                            </View> : null
                                        } */}
                                        <View style={{ margin: 10 }}></View>
                                        <TouchableOpacity
                                            style={[styles.btnLogin, { marginBottom: 50, alignItems: "center", justifyContent: "space-around" }]}
                                            onPress={() => {
                                                // this.props.navigation.navigate("ReservationOption1Page") 
                                                this._onValid()
                                            }} >
                                            <LinearGradient
                                                colors={[colors.colorBtnStart, colors.colorBtnEnd]}
                                                start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                                style={styles.linearGradienLogin} >
                                                <Text style={styles.textbtnLogin}>{"Continuer".toUpperCase()}</Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </ScrollView>
                        </View>
                    </SafeAreaView>
                }
            </Fragment>
        );
    }
}
const mapStateProps = (state) => {
    return {
        userInfos: state.userInfos
    }
}
export default connect(mapStateProps)(PaiementChoice);