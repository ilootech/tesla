import React, {Component,Fragment}  from 'react'
import { StatusBar,View,Text,ScrollView,
    TouchableOpacity,TextInput,ImageBackground} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image'
import {connect} from 'react-redux'
import MapView ,{ AnimatedRegion, Animated } from 'react-native-maps';
import  LinearGradient from 'react-native-linear-gradient'
import colors from "../../../configs/colors"

import styles from "./styles"
import DimScreen from '../../../configs/DimScreen';
class ModalEntreeCode extends Component 
{
    static navigationOptions = {
        header: null,
    };
    constructor(props){
        super(props)
        this.state={
            optionVoiture:true
        }
    }
    _clickAddChoixVoiture=async(item)=>
    {
        const action = { type: "ADD_CHOIXVOITURE", value: item}
        await  this.props.dispatch(action) 
        this.props.clickCloseModalVoiture()
        
    }

    render() {
        return (
           <TouchableOpacity style={styles.windowChoixVoiturees} onPress={()=>{this.props.clickCloseModalCode()}}>
                <ScrollView contentContainerStyle={styles.scrollBody}  contentInsetAdjustmentBehavior="automatic">
                    <View style={styles.panelVoitures}>
                        <View style={styles.panelTitre}>
                            <Text style={styles.textTitre}>Entrez votre code</Text>
                        </View>
                        <TextInput style={styles.textInputCode}/>
                        <View style={styles.panelButton}>
                            <TouchableOpacity  style={styles.btnEntreeCode} onPress={()=>{this.props.clickCloseModalCode()}} >
                                <Text style={styles.textButton}>ANNULER</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.btnEntreeCode} onPress={()=>{this.props.clickCloseModalCode()}}>
                                <Text style={styles.textButton}>OK</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
           </TouchableOpacity>)
        }            
}
const mapStateProps= (state)=>
{
  return {
    choixLocVoiture:state.choixLocVoiture,
    listeVoitures:state.listeVoitures,
    }
}
export default connect(mapStateProps)(ModalEntreeCode)