import React, { Component, Fragment } from 'react'
import {
    SafeAreaView, StatusBar, View, Text, ScrollView, ActivityIndicator,
    TouchableOpacity, TextInput, Image, ImageBackground, Alert, RefreshControl
} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image'
import LinearGradient from 'react-native-linear-gradient'
import colors from "../../../configs/colors"
import HeaderPage from "../../../components/HeaderPage/HeaderPage"
import styles from "./styles"
import DimScreen from '../../../configs/DimScreen';
import ReservationHelpers from '../../../apis/helpers/reservation_helpers'
import { connect } from 'react-redux'
import Loading from '../../../components/Loading/Loading'

class Mesreservations extends Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props)
        this.state = {
            choixReservation: 0,
            data: [],
            dataTerminer: [],
            show: true,
            refreshing: false
        }
    }
    /** Show Slider barre */
    _clickShowSLider = () => {
        this.props.navigation.openDrawer()
    }
    componentDidMount = async () => {
        this._initList()
    }
    _initList = async () => {
        let idUser = this.props.userInfos.id_User
        let response = await ReservationHelpers.getListReservation(idUser)
        // this.setState({ data: response })
        const action = { type: "RESERVATION", value: response }
        await this.props.dispatch(action)
        // console.warn("List tsotra")
        let responseTerminer = await ReservationHelpers.getListReservationTerminer(idUser)
        // this.setState({ dataTerminer: responseTerminer })
        const _action = { type: "RESERVATION_TERMINER", value: responseTerminer }
        await this.props.dispatch(_action)
        // console.warn("List Terminer")
        this.setState({ show: false })
    }
    _onValid = async (item) => {
        this.setState({ show: true })
        let idUser = this.props.userInfos.id_User
        let response = await ReservationHelpers.testUtilisateur(idUser)
        // console.warn(response.status)
        this.setState({ show: false })

        // if (response.status == 1) {
        // Alert.alert("Information !", "Choix Paiement")
        this.props.navigation.navigate("PaiementChoice", { item: item })

        // }
        // else {
        //     Alert.alert("Information !", "Veuillez compléter vos informations avant de continuer votre réservation", [
        //         {
        //             text: "Ok",
        //             onPress: () => {
        //                 this.props.navigation.navigate("ModifierProfilPage")
        //             }
        //         }
        //     ])
        // }
    }
    _onAnnuler = async (item) => {
        this.setState({ show: true })
        let idUser = this.props.userInfos.id_User
        let id_reservation = item.idResa
        const response = await ReservationHelpers.deleteReservation(idUser, id_reservation)
        if (response) {
            this._initList()
        }
        else {
            Alert.alert("Information !", "Une erreur s'est produite lors de l'annulation de votre réservation. Veuillez réessayer.")
        }
        this.setState({ show: false })

    }
    _getList = () => {
        return this.props.dataListReservation.map((item, index) => {
            return <View key={index} style={{ flexDirection: "column", justifyContent: "space-around", borderBottomColor: colors.vertfonce, borderBottomWidth: 1, marginBottom: 15, paddingBottom: 15 }}>
                <View style={{ width: DimScreen.widthScreen * 0.8, flexDirection: "row", justifyContent: "space-around", }}>
                    <View style={{ width: DimScreen.widthScreen * 0.5 }}>
                        <Text style={{ color: colors.vertfonce, fontWeight: "bold" }}>Le {item.dateDepart} à {item.heure_minDepart}</Text>
                        {/* <Text>{item.heure_minDepart}</Text> */}
                        <Text>{item.depart}</Text>
                        {/* <AutoHeightImage
                            width={50}
                            source={{ uri: item.type_voiture.image }}
                        /> */}
                    </View>
                    <View style={{ width: DimScreen.widthScreen * 0.25, alignItems: "center", justifyContent: 'center' }}>
                        <Text style={{ fontSize: 16, color: colors.vertfonce, fontWeight: "bold" }}>{item.prix} €</Text>
                    </View>
                </View>
                <View style={{ width: DimScreen.widthScreen * 0.8, flexDirection: "row", justifyContent: "space-around", }}>
                    <TouchableOpacity
                        style={{ paddingTop: 10, paddingBottom: 10, width: DimScreen.widthScreen * 0.25 }}
                        onPress={() => this._onAnnuler(item)}
                    >
                        <LinearGradient
                            colors={[colors.colorBtnEnd, colors.colorBtnEnd]}
                            start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                            style={{
                                width: "100%",
                                alignItems: "center",
                                justifyContent: "center",
                                // height: "100%",
                                borderRadius: DimScreen.heightScreen * 0.04,
                                borderColor: '#fff',
                                borderBottomWidth: 0,
                                shadowColor: '#000',
                                shadowOffset: { width: 0, height: 2 },
                                shadowOpacity: 0.8,
                                shadowRadius: 5,
                                elevation: 2,
                                paddingTop: 5, paddingBottom: 5

                            }} >
                            <Text style={{ fontSize: 14, color: colors.white }}>{"Annuler".toUpperCase()}</Text>
                        </LinearGradient>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{ paddingTop: 10, paddingBottom: 10, width: DimScreen.widthScreen * 0.25 }}
                        onPress={() => this._onValid(item)}
                    >
                        <LinearGradient
                            colors={[colors.colorBtnStart, colors.colorBtnEnd]}
                            start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                            style={{
                                width: "100%",
                                alignItems: "center",
                                justifyContent: "center",
                                // height: "100%",
                                borderRadius: DimScreen.heightScreen * 0.04,
                                borderColor: '#fff',
                                borderBottomWidth: 0,
                                shadowColor: '#000',
                                shadowOffset: { width: 0, height: 2 },
                                shadowOpacity: 0.8,
                                shadowRadius: 5,
                                elevation: 2,
                                paddingTop: 5, paddingBottom: 5
                            }} >
                            <Text style={{ fontSize: 14, color: colors.white }}>{"Confirmer".toUpperCase()}</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </View >
        }

        )
    }
    _getListTerminer = () => {
        return this.props.dataListReservationTerminer.map((item, index) => {
            return <View key={index} style={{ flexDirection: "column", justifyContent: "space-around", borderBottomColor: colors.vertfonce, borderBottomWidth: 1, marginBottom: 15, paddingBottom: 15 }}>
                <View style={{ width: DimScreen.widthScreen * 0.8, flexDirection: "row", justifyContent: "space-around", }}>
                    <View style={{ width: DimScreen.widthScreen * 0.5 }}>
                        <Text style={{ color: colors.vertfonce, fontWeight: "bold" }}>Le {item.dateDepart} à {item.heure_minDepart}</Text>
                        {/* <Text>{item.heure_minDepart}</Text> */}
                        <Text>{item.depart}</Text>
                        {/* <AutoHeightImage
                            width={50}
                            source={{ uri: item.type_voiture.image }}
                        /> */}
                    </View>
                    <View style={{ width: DimScreen.widthScreen * 0.25, alignItems: "center", justifyContent: 'center' }}>
                        <Text style={{ fontSize: 16, color: colors.vertfonce, fontWeight: "bold" }}>{item.prix} €</Text>
                    </View>
                </View>
            </View >
        }

        )
    }
    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView style={styles.containerSafeAreaView}>
                    <HeaderPage _clickShowSLider={this._clickShowSLider} />
                    {this.state.show ?
                        <Loading /> : null
                    }
                    <View style={styles.bodyPanel}>
                        <ScrollView contentContainerStyle={styles.scrollAuthloading} contentInsetAdjustmentBehavior="automatic">
                            <View style={styles.container}>
                                <Text style={styles.textTitre}>MES RÉSERVATIONS</Text>
                                <View style={styles.bodyFormulaire}>
                                    <View style={styles.choixResa}>
                                        <TouchableOpacity style={styles.btnchoixResa} onPress={() => { this.setState({ choixReservation: 0 }) }} >
                                            <LinearGradient
                                                colors={[this.state.choixReservation == 0 ? colors.colorBtnStart : colors.transparent, this.state.choixReservation == 0 ? colors.colorBtnEnd : colors.transparent]}
                                                start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                                style={styles.linearGradienResa} >
                                                {/* <Text style={[styles.textchoixResa, { color: this.state.choixReservation == 0 ? colors.white : colors.vertfonce }]}>À VENIR</Text> */}
                                                <Text style={[styles.textchoixResa, { color: this.state.choixReservation == 0 ? colors.white : colors.vertfonce }]}>{"programmées".toUpperCase()}</Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.btnchoixResa} onPress={() => { this.setState({ choixReservation: 1 }) }}>
                                            <LinearGradient
                                                colors={[this.state.choixReservation == 1 ? colors.colorBtnEnd : colors.transparent, this.state.choixReservation == 1 ? colors.colorBtnStart : colors.transparent]}
                                                start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                                style={styles.linearGradienResa} >
                                                {/* <Text style={[styles.textchoixResa, { color: this.state.choixReservation == 1 ? colors.white : colors.vertfonce }]} >TERMINÉES</Text> */}
                                                <Text style={[styles.textchoixResa, { color: this.state.choixReservation == 1 ? colors.white : colors.vertfonce }]} >{"à confirmer".toUpperCase()}</Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                    </View>
                                    {/* A venir */}
                                    {this.state.choixReservation == 0 ?
                                        <ScrollView
                                            contentInsetAdjustmentBehavior="automatic"
                                            contentContainerStyle={[styles.contentContainerStyle]}
                                            refreshControl={
                                                <RefreshControl
                                                    refreshing={this.state.refreshing}
                                                    onRefresh={() => {
                                                        console.log("Refreshing...")
                                                        this.setState({ refreshing: true })
                                                        this._initList()
                                                        this.setState({ refreshing: false })
                                                    }}
                                                // tintColor={colors.green}
                                                // colors={[colors.green]}
                                                />
                                            }
                                        >
                                            {this.props.dataListReservationTerminer.length == 0 ?
                                                <View style={[styles.panelListeResa, { height: DimScreen.heightScreen * 0.5 }]}>
                                                    <Text style={styles.aucuneCourseAvenir}>Aucune course à venir</Text>
                                                </View> :
                                                <View style={{ margin: 10 }}>
                                                    {/* {this._getList()} */}
                                                    {this._getListTerminer()}
                                                </View>
                                            }
                                        </ScrollView> : null
                                    }
                                    {/* Terminé */}
                                    {this.state.choixReservation == 1 ?
                                        <ScrollView contentInsetAdjustmentBehavior="automatic"
                                            contentContainerStyle={[styles.contentContainerStyle]}
                                            refreshControl={
                                                <RefreshControl
                                                    refreshing={this.state.refreshing}
                                                    onRefresh={() => {
                                                        console.log("Refreshing...")
                                                        this.setState({ refreshing: true })
                                                        this._initList()
                                                        this.setState({ refreshing: false })
                                                    }}
                                                // tintColor={colors.green}
                                                // colors={[colors.green]}
                                                />
                                            }>
                                            {this.props.dataListReservation.length == 0 ?
                                                <View style={[styles.panelListeResa, { height: DimScreen.heightScreen * 0.5 }]}>
                                                    <Text style={styles.aucuneCourseAvenir}>Aucune course à confirmer</Text>
                                                </View> :
                                                <View style={{ margin: 10 }}>
                                                    {/* {this._getListTerminer()} */}
                                                    {this._getList()}
                                                </View>
                                            }
                                        </ScrollView> : null
                                    }
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }
}
const mapStateProps = (state) => {
    return {
        userInfos: state.userInfos,
        dataListReservation: state.dataListReservation,
        dataListReservationTerminer: state.dataListReservationTerminer,
    }
}
export default connect(mapStateProps)(Mesreservations);