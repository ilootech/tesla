import { StyleSheet,Platform } from 'react-native';
import DimScreen from "../../../configs/DimScreen"
import colors from "../../../configs/colors"
export default styles = StyleSheet.create({

    containerSafeAreaView: 
    {
        flex: 1,
    },
    containerHeader: {
        height:DimScreen.heightScreen*0.3,
        width:"100%", 
    },
    bodyFormulaire:
    {
        backgroundColor:colors.white,
        width:"90%",
        height:DimScreen.heightScreen*0.7,
        position:"relative",
        marginTop:DimScreen.heightScreen*0.04,
        marginBottom:DimScreen.heightScreen*0.02,
        marginLeft:DimScreen.widthScreen*0.1,
      
        paddingBottom:DimScreen.heightScreen*0.02,
        paddingTop:DimScreen.heightScreen*0.025,
        paddingLeft:DimScreen.widthScreen*0.05,
        paddingRight:DimScreen.widthScreen*0.05,
        marginRight:DimScreen.widthScreen*0.1,
        flex:1,
        borderWidth: 0,
        borderRadius: DimScreen.heightScreen*0.04,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 4,
        alignItems:"center",
        
    },
    textlist:
    {
        color:colors.gris1Font,
        fontSize:DimScreen.widthScreen*0.033,
        fontFamily:"Lato-Medium",
        marginLeft:DimScreen.widthScreen*0.024,
        width:"100%"
    },
    bodyPanel:
    {
        position:"relative",zIndex:20,flex:1, marginTop:-DimScreen.heightScreen*0.23
    },
    fondHeader:
    {
        flex:1,
        alignItems:"center"
    },
    btnShowSlider:
    {
        marginLeft:DimScreen.widthScreen*0.025,
        marginTop:DimScreen.widthScreen*0.03,
    }, 
    textTitre:{
        fontFamily:"Lato-Black",
        fontSize:DimScreen.widthScreen*0.07,
        marginTop:DimScreen.heightScreen*0.034,
        marginBottom:DimScreen.heightScreen*0.038,
        color:colors.white,
    },
    viewMenueRow:
    {
        flexDirection:"row",
        height:DimScreen.heightScreen*0.11,
        width:"100%",
        marginBottom:DimScreen.widthScreen*0.024,
       
    },  
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    header:
    {
        width:"100%",
        flexDirection:"row",
        justifyContent:"space-between"
    },
    btnShowSlider:
    {
        marginLeft:DimScreen.widthScreen*0.025,
        marginTop:DimScreen.widthScreen*0.03,
    },
    scrollAuthloading:
    {
       /* justifyContent:"center",
        width:"100%",*/
    },
    linearGradienResa:
    {
        width:"100%",
        alignItems:"center",
        justifyContent:"center",
        height:"100%",
        flexDirection:"row",
    },
    choixResa:
    {
        flexDirection:"row",
        borderColor:colors.vertfonce,
        borderRadius:DimScreen.widthScreen*0.02,
        borderWidth:DimScreen.widthScreen*0.005,
        height:DimScreen.heightScreen*0.06,
    },
    panelListeResa:
    {
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    },
    btnchoixResa:
    {
        flex:2,
        alignItems:"center",
        justifyContent:"center",
        height:"100%",
       
    },
    textchoixResa:
    {
        fontSize:DimScreen.widthScreen*0.03,
        fontFamily:"Lato-Heavy",
        textTransform:"uppercase",
        marginLeft:DimScreen.widthScreen*0.028,
    },
    aucuneCourseAvenir:
    {
        fontSize:DimScreen.widthScreen*0.04,
        color:colors.gris1Font,
        fontFamily:"Lato-Black",
    },
    
})