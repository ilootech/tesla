import React, {Component,Fragment}  from 'react'
import { StatusBar,View,Text,ScrollView,
    TouchableOpacity,TextInput,ImageBackground} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image'
import MapView ,{ AnimatedRegion, Animated } from 'react-native-maps';
import  LinearGradient from 'react-native-linear-gradient'
import colors from "../../../configs/colors"
import {connect} from 'react-redux'
import styles from "./styles"
import DimScreen from '../../../configs/DimScreen';
class Validation extends Component 
{
    static navigationOptions = {
        header: null,
    };
    constructor(props){
        super(props)
        this.state={
            optionVoiture:true
        }
    }
    render() {
        return (
           <TouchableOpacity style={styles.windowValisationVoiturees} onPress={()=>{this.props.clickCloseModalValidation()}}>
                <ScrollView contentContainerStyle={styles.scrollBody}  contentInsetAdjustmentBehavior="automatic">
                    <View style={styles.panelVoitures}>
                        <View style={styles.panelTitre}>
                            <Text style={styles.textTitre}>Validation du numéro</Text>
                            <Text style={styles.textTitre} >de téléphone</Text>
                        </View>
                        <View style={styles.viewVotreNum}>
                            <Text style={styles.labelVotreNum}>Votre numéro de téléphone : </Text>
                            <Text  style={styles.votreNum}>06 51 91 55 08.</Text>
                        </View>
                        <View style={styles.viewVotreNumModif}>
                            <Text style={styles.labelMauvaisenum}>Mauvais numéros ?</Text><Text style={styles.textModifier}> Modifiez</Text>
                        </View>
                        <TextInput style={styles.textInputCodeValifation} placeholder="CODE DE VALIDATION" />
                        <TouchableOpacity  style={styles.btnValider}>
                            <LinearGradient
                                    colors={[colors.colorBtnStart,colors.colorBtnEnd]} 
                                    start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                                    style={styles.linearGradienBerlineValider}>
                                <Text style={styles.textValider}>Valider mon numéro de téléphone</Text>
                            </LinearGradient>    
                        </TouchableOpacity>
                        <TouchableOpacity  style={styles.btnValider}>
                            <LinearGradient
                                colors={[colors.colorBtnStart,colors.colorBtnEnd]} 
                                start={{x: 0.0, y: 1.0}} end={{x: 1.0, y: 1.0}}
                                style={styles.linearGradienBerlineValider}>
                                <Text style={styles.textValider}>Renvoyez-moi un nouveau code</Text>
                            </LinearGradient>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.btnAnuller} onPress={()=>{this.props.clickCloseModalValidation()}} >
                            <Text style={styles.textAnuller} >Annuler</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
           </TouchableOpacity>)
        }            
}
export default Validation