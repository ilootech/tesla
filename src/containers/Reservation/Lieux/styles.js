import { StyleSheet } from 'react-native';
import DimScreen from "../../../configs/DimScreen"
import colors from "../../../configs/colors"
export default styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    scrollAuthloading:
    {
        width:"100%",
        height:DimScreen.heightScreen*0.96
    },
    headerAddLieux:
    {
        
        flexDirection:"row",
        height:DimScreen.heightScreen*0.075,
        alignItems:"center",
        marginRight:DimScreen.widthScreen*0.04,
        marginLeft:DimScreen.widthScreen*0.02,
        borderBottomColor:colors.vertBorderColor,
        borderBottomWidth:DimScreen.widthScreen*0.003,
    },
    listLieux:
    {
       marginTop:DimScreen.heightScreen*0.02,
       marginLeft:DimScreen.widthScreen*0.05,
       marginRight:DimScreen.widthScreen*0.05,
       width:"100%",
    },
    textInputRech:
    {
        flex:1,
        color:colors.black,
        fontFamily:"Lato-Medium",
        fontSize:DimScreen.widthScreen*0.045,
        padding:0,
        paddingLeft:DimScreen.widthScreen*0.02,
        marginLeft:DimScreen.widthScreen*0.01,
    },
    
})