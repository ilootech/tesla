import { StyleSheet } from 'react-native';
import DimScreen from "../../../../configs/DimScreen"
import colors from "../../../../configs/colors"
export default styles = StyleSheet.create(
{
    listItemlieux:
    {
        flexDirection:"row",
        justifyContent:"flex-start",
        paddingBottom:DimScreen.heightScreen*0.017,
        paddingTop:DimScreen.heightScreen*0.017,
        borderBottomColor:colors.vertfonce,
        borderBottomWidth:DimScreen.heightScreen*0.0015,
        marginLeft:DimScreen.widthScreen*0.05,
        marginRight:DimScreen.widthScreen*0.05,
    },
    textNameLieux:
    {
        color:colors.gris1Font,
        fontSize:DimScreen.widthScreen*0.04,
        marginLeft:DimScreen.widthScreen*0.05,

    }
})