import React, {Component,Fragment}  from 'react'
import {View,Text,
    TouchableOpacity} from 'react-native'
import {connect} from 'react-redux'
import AutoHeightImage from 'react-native-auto-height-image'
import MapView ,{ AnimatedRegion, Animated } from 'react-native-maps';
import  LinearGradient from 'react-native-linear-gradient'


import colors from "../../../../configs/colors"
import styles from "./styles"
import DimScreen from '../../../../configs/DimScreen';
import ReservationHelpers from '../../../../apis/helpers/reservation_helpers';
class ItemsLieux extends Component 
{
    constructor(props)
    {
        super(props)
    }
    _clickBtnLieux=async(value)=>
    {
        if(this.props.optionChoix=="Arriver")
        {
            const action = { type: "ADD_ARRIVEE", value: value}
            await  this.props.dispatch(action) 


        }else
        {
            const action = { type: "ADD_DEPART", value: value}
            await  this.props.dispatch(action) 
        }
        // this._testDistance()
        this.props._previus()
        
    }
    // _testDistance = async () => {
    //     if (this.props.depart == null) {

    //     }
    //     else if (this.props.arriver == null) {

    //     }
    //     else {
    //         let response = await ReservationHelpers.getDistance(this.props.depart, this.props.arriver)
    //         console.warn(response.rows.elements)
    //     }
    // }
    /*
    <AutoHeightImage
                 source={this.props.listLieux.type=="plane"?require('../../../../assets/images/reservation/plane.png'):require('../../../../assets/images/reservation/gasoline-pump.png')}
                  width={DimScreen.widthScreen * 0.06} />
    */
    render() {
        return (
        <TouchableOpacity onPress={()=>{this.props.clickChoixLieux(this.props.itemlieux)}} style={styles.listItemlieux} >  
            
            <Text style={styles.textNameLieux}>{this.props.itemlieux.description}</Text>
        </TouchableOpacity>
        );
    }
}

const mapStateProps= (state)=>
{
  return {
    arriver:state.arriver,
    depart:state.depart,
    }
}
export default connect(mapStateProps)(ItemsLieux);