import React, { Component, Fragment } from 'react'
import {
    SafeAreaView, FlatList, StatusBar, View, Text, ScrollView, ActivityIndicator,
    TouchableOpacity, TextInput
} from 'react-native'
import { connect } from 'react-redux'
import AutoHeightImage from 'react-native-auto-height-image'
import MapView, { AnimatedRegion, Animated } from 'react-native-maps'
import RNGooglePlaces from 'react-native-google-places';
import LinearGradient from 'react-native-linear-gradient'

import ReservationHelpers from "../../../apis/helpers/reservation_helpers"
import colors from "../../../configs/colors"
import HeaderAuthen from "../../../components/HeaderAuthen/HeaderAuthen"


import ItemsLieux from "./ItemsLieux/ItemsLieux"
import Lieuxdata from "../../../helpers/Lieuxdata"
import DimScreen from '../../../configs/DimScreen';

import styles from "./styles"
import Loading from '../../../components/Loading/Loading'
class Lieux extends Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props)
        this.state =
            {
                adresseRueListes: [],
                loading: false,
            }

    }
    componentDidMount() {
        // this.ref1.focus()
    }
    openSearchModal() {
        RNGooglePlaces.openAutocompleteModal()
            .then((place) => {
                console.log(place);

                // place represents user's selection from the
                // suggestions and it is a simplified Google Place object.
            })
            .catch(error => console.log(error.message));  // error is a Javascript Error object
    }
    /** Show Slider barre */
    _clickShowSLider = () => {
        this.props.navigation.openDrawer()
    }
    _previus = () => {
        this.props.navigation.pop()
    }
    _textSearchAdresse = async (value) => {
        if (value.length >= 4) {
            await ReservationHelpers.getListeAdresseMap(value)
                .then((data) => {
                    console.log("Adresse" + JSON.stringify(data))
                    if (data.predictions.length > 0) {
                        this.setState({ adresseRueListes: data.predictions })
                    }

                })
        }
    }

    /**
     * Choix Lieux de depart et Arriver
     */
    clickChoixLieux = async (itemLieux) => {
        this.setState({ loading: true })
        if (this.props.navigation.state.params.optionChoix == "Départ") {
            // console.log("Lieux depart::"+JSON.stringify(itemLieux))
            const _action = { type: "ADD_DEPART", value: itemLieux }
            await this.props.dispatch(_action)
            await ReservationHelpers.getSearchLatLong_Place(itemLieux.place_id)
                .then(async (data) => {
                    let coordonateDepart = {
                        latitude: data.result.geometry.location.lat,
                        longitude: data.result.geometry.location.lng,
                    }
                    const action = { type: "ADD_COORDONATE_DEPART", value: coordonateDepart }
                    await this.props.dispatch(action)
                    console.log("**************************")
                    console.log(this.props.coordonateDepart)
                    console.log("**************************")

                })
            this.setState({ loading: false })
            this.props.navigation.pop()
        } else {
            // console.log("Lieux Arriver::"+JSON.stringify(itemLieux))
            const _action = { type: "ADD_ARRIVEE", value: itemLieux }
            await this.props.dispatch(_action)
            await ReservationHelpers.getSearchLatLong_Place(itemLieux.place_id)
                .then(async (data2) => {
                    let coordonateArriver = {
                        latitude: data2.result.geometry.location.lat,
                        longitude: data2.result.geometry.location.lng,
                    }
                    const action = { type: "ADD_COORDONATE_ARRIVER", value: coordonateArriver }
                    await this.props.dispatch(action)
                    console.log("**************************")
                    console.log(this.props.coordonateArriver)
                    console.log("**************************")
                })
            this.setState({ loading: false })
            this.props.navigation.pop()
        }
    }

    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                {this.state.loading ?
                    <View style={{ flex: 1, alignItems: "center", alignSelf: "center", justifyContent: "center" }}>
                        <Loading />
                    </View> : 
                <SafeAreaView>
                    <View style={styles.container}>

                        <View style={styles.headerAddLieux}>
                            <TouchableOpacity onPress={() => { this.props.navigation.pop() }} >
                                <AutoHeightImage
                                    source={require('../../../assets/images/reservation/btnprecedent.png')}
                                    width={DimScreen.widthScreen * 0.08}
                                    style={styles.imgmapImgRue} />
                            </TouchableOpacity>
                            <TextInput
                                onChangeText={(text) => { this._textSearchAdresse(text) }}
                                style={styles.textInputRech} placeholderTextColor={colors.gris1Font}
                                placeholder="Rechercher une ville "
                                autoFocus={true}
                            />
                            <TouchableOpacity onPress={() => { this.props.navigation.pop() }}>
                                <AutoHeightImage
                                    source={require('../../../assets/images/reservation/deleteResa.png')}
                                    width={DimScreen.widthScreen * 0.04} />
                            </TouchableOpacity>
                        </View>
                        {
                            this.state.adresseRueListes.length > 0 ?
                                <FlatList
                                    style={styles.listLieux}
                                    data={this.state.adresseRueListes}
                                    keyExtractor={(item) => item.id.toString()}
                                    renderItem={({ item }) => <ItemsLieux clickChoixLieux={this.clickChoixLieux} itemlieux={item} />}
                                    onEndReachedThreshold={0.5}
                                    onEndReached={() => {
                                    }}
                                /> : null
                        }

                    </View>
                </SafeAreaView>
                }
            </Fragment>
        );
    }
}
const mapStateProps = (state) => {
    return {
        arriver: state.arriver,
        depart: state.depart,
        region: state.region,
        coordonateDepart: state.coordonateDepart,
        coordonateArriver: state.coordonateArriver,
    }
}
export default connect(mapStateProps)(Lieux);