import { StyleSheet } from 'react-native';
import DimScreen from "../../../configs/DimScreen"
import colors from "../../../configs/colors"
export default styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    mapResa:
    {
        width:"100%",
        flex:1,
    },
    scrollBody:
    {
        
        height:"100%",
        alignItems:"center",
    },
    viewBtnPrec:
    {
        position:"absolute",
        zIndex:5,
        width:"97%",
        alignItems:"flex-start",
        top:DimScreen.heightScreen*0.02,
    },
    bodyResa:
    {
        width:"90%",
        position:"relative",
        marginTop:-DimScreen.heightScreen*0.5,
        zIndex:5,
    },
    panelProximiter:
    {
        flexDirection:"row",
        height:DimScreen.widthScreen*0.096,
        marginBottom:DimScreen.heightScreen*0.02
    },
    
    durrationMn:
    {
        height:"100%",
        backgroundColor:colors.white,
        alignItems:"center",
        borderWidth: 0,
        borderRadius: DimScreen.heightScreen*0.01,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 4,
        justifyContent:"center",
        paddingLeft:DimScreen.widthScreen*0.02,
        paddingRight:DimScreen.widthScreen*0.02,
    },

    viewProximiter:
    {
        height:"100%",
        width:DimScreen.widthScreen*0.096,
        backgroundColor:colors.white,
        alignItems:"center",
        borderWidth: 0,
        borderRadius: DimScreen.heightScreen*0.01,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },

        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 4,
        justifyContent:"center"
    },
    textdurrationMn:
    {
        fontSize:DimScreen.widthScreen*0.042,
        fontFamily:"Lato-SemiBold",
        color:colors.gris1Font,
    },
    panelboxRecherche:
    {
        /*height:DimScreen.heightScreen*0.062,*/
          /*borderRadius: DimScreen.heightScreen*0.02,*/
        backgroundColor:colors.white,
        // alignItems:"center",
        borderWidth: 0,
        borderTopLeftRadius:DimScreen.heightScreen*0.02,
        borderTopRightRadius:DimScreen.heightScreen*0.02,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 4,
        paddingTop:DimScreen.widthScreen*0.04,
        paddingBottom:DimScreen.widthScreen*0.04,
    },
    viewLieuxboxRecherche:
    {
        /*height:DimScreen.heightScreen*0.2,*/
        width:"100%",
        flexDirection:"row",
        paddingLeft:DimScreen.widthScreen*0.02,
        paddingRight:DimScreen.widthScreen*0.02,
    },
    linearGradientInput:
    {
        width:"100%",
        flexDirection:"row",
        alignItems:"center",
        borderRadius:DimScreen.widthScreen*0.04,
        height:DimScreen.heightScreen*0.057,
        marginBottom:DimScreen.heightScreen*0.025,
        paddingLeft:DimScreen.widthScreen*0.02,
    },
    textInput:
    {
        color:colors.white,
        fontFamily:"Lato-Medium",
        fontSize:DimScreen.widthScreen*0.035,
        padding:0,
        marginLeft:DimScreen.widthScreen*0.01,
    },
    linearGradientInputNoir:
    {
        width:"100%",
        flexDirection:"row",
        alignItems:"center",
        borderRadius:DimScreen.widthScreen*0.04,
        height:DimScreen.heightScreen*0.057,
        paddingLeft:DimScreen.widthScreen*0.02,
    },
    viewLieuxbox1:
    {
        flex:1
    },
    viewLieuxbox2:
    {
       paddingLeft:DimScreen.widthScreen*0.04,
       backgroundColor:colors.white,
       alignItems:"flex-end",
       justifyContent:"center",
       /* width:DimScreen.widthScreen*0.1,*/
    },
    boxOptions:
    {
        flexDirection:"row",
        marginTop:DimScreen.heightScreen*0.034,
    },
    btnOption1:
    {
        alignItems:"center",
        flex:2,
        borderTopWidth:DimScreen.widthScreen*0.01,
        paddingTop:DimScreen.heightScreen*0.018,
        paddingBottom:DimScreen.heightScreen*0.018,
    },
    btnOption2:
    {
        flex:2,
        alignItems:"center",
        borderTopWidth:DimScreen.widthScreen*0.01,
        paddingTop:DimScreen.heightScreen*0.012,
        paddingBottom:DimScreen.heightScreen*0.018,
    },
    textOption1:
    {
        fontFamily:"Lato-Bold",
        fontSize:DimScreen.widthScreen*0.045,
        fontStyle:"normal",
        // fontWeight:"Bold"
    },
    textOption2:
    {
        fontFamily:"Lato-Bold",
        fontSize:DimScreen.widthScreen*0.04,
    },
    textSelectModepaie:
    {
        fontFamily:"Lato-Bold",
        // fontWeight:"Bold",
        fontSize:DimScreen.widthScreen*0.04,
        color:colors.gris1Font,
        marginTop:DimScreen.heightScreen*0.025,
        marginBottom:DimScreen.heightScreen*0.034
    },

    panelviewBerline:
    {
        // flexDirection:"row",
        width:"50%",
        justifyContent:"center",
    },
    linearGradienBerline:
    {
        flexDirection:"row",
        alignItems:"center",
        borderRadius:DimScreen.heightScreen*0.02,
        justifyContent:"center",
        paddingRight:DimScreen.widthScreen*0.02,
        paddingLeft:DimScreen.widthScreen*0.02,
        height:DimScreen.heightScreen*0.058,
    },
    prixBerline:
    {
        // width:"100%",
        alignItems:"center",
        // marginLeft:DimScreen.widthScreen*0.03,
    },
    labelPrix:
    {
        color:colors.gris1Font,
        fontFamily:"Lato-SemiBold"
    },
    textPrix:
    {
        color:colors.gris1Font,
        fontFamily:"Lato-Bold",
    },
    textberline:
    {
        color:colors.white,
       /* fontFamily:"MyriadProRegular"*/
    },
    linearGradienBerlineValider:
    {
        height:"100%",
        width:"100%",
        borderRadius:DimScreen.heightScreen*0.05,
        alignItems:"center",
        alignItems:"center",
        justifyContent:"center",
    },
    textValider:
    {
        fontSize:DimScreen.widthScreen*0.04,
        color:colors.white,
        fontFamily:"Lato-Bold",
    },
    btnValider:
    {
        marginTop:DimScreen.heightScreen*0.02,
        height:DimScreen.heightScreen*0.062,
        width:"78%",
        alignItems:"center",
        justifyContent:"center",
    },

})