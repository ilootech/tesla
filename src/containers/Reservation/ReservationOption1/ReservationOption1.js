import React, { Component, Fragment } from 'react'
import {
    SafeAreaView, StatusBar, View, Text, ScrollView, ActivityIndicator,
    TouchableOpacity, TextInput, Alert
} from 'react-native'
import { connect } from 'react-redux'
import AutoHeightImage from 'react-native-auto-height-image'
import MapView, { AnimatedRegion, Animated, Marker } from 'react-native-maps';
import LinearGradient from 'react-native-linear-gradient'
import colors from "../../../configs/colors"
import ChoixVoiture from "../ChoixVoiture/ChoixVoiture"
import ModalEntreeCode from "../ModalEntreeCode/ModalEntreeCode"
import Validation from "../Validation/Validation"
import styles from "./styles"
import DimScreen from '../../../configs/DimScreen';
import UserHelpers from '../../../apis/helpers/user_helpers';
import ReservationHelpers from '../../../apis/helpers/reservation_helpers';
import DateTimePicker from '@react-native-community/datetimepicker';
// import getDirections from 'react-native-google-maps-directions'
import Loading from '../../../components/Loading/Loading';
import Geolocation from '@react-native-community/geolocation';


class ReservationOption1 extends Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props)
        this.state = {
            optionVoiture: true,
            modalChoixVoiture: false,
            modalValidation: false,
            showModalCode: false,
            showDate: false,
            showHeure: false,
            date: new Date(),
            dateFin: null,
            heureFin: null,
            heure: new Date(),
            mode: 'date',
            depart: "Aéroport Lyon Saint Exupéry",
            arriver: "Arrivée (Optionnel)",
            duration: null,
            prixTotal: null
        }
    }
    componentDidMount() {
        // console.warn("reservation")
        console.log('=======================')
        console.log(this.props.choixLocVoiture)
        console.log('=======================')
    }
    componentWillReceiveProps() {
        this._testDistance()
    }
    _testDistance = async () => {
        if (this.props.depart == null) {

        }
        else if (this.props.arriver == null) {

        }
        else {
            let response = await ReservationHelpers.getDistance(this.props.depart.description, this.props.arriver.description)
            this.setState({ duration: response.rows[0].elements[0].distance.text })

            // Directions
            const data = {
                source: {
                    latitude: this.props.coordonateDepart.latitude,//-33.8356372,
                    longitude: this.props.coordonateDepart.longitude//18.6947617
                },
                destination: {
                    latitude: this.props.coordonateArriver.latitude,//-33.8600024,
                    longitude: this.props.coordonateArriver.longitude//18.697459
                },
                params: [
                    {
                        key: "travelmode",
                        value: "driving"        // may be "walking", "bicycling" or "transit" as well
                    },
                    {
                        key: "dir_action",
                        value: "navigate"       // this instantly initializes navigation using the given travel mode
                    }
                ],
                waypoints: [
                    //  {
                    //    latitude: -33.8600025,
                    //    longitude: 18.697452
                    //  },
                    //  {
                    //    latitude: -33.8600026,
                    //    longitude: 18.697453
                    //  },
                    //     {
                    //    latitude: -33.8600036,
                    //    longitude: 18.697493
                    //  }
                ]
            }
            //  fait appel au URL qui renvoie les directions de départ vers l'arrivée
            // getDirections(data)

            // Calcul le prix Total du trajet 
            // let prixVoiture = this.props.choixLocVoiture.prix
            let prixVoiture = 2
            let distance = response.rows[0].elements[0].distance.text
            console.log("prix", prixVoiture)
            console.log("distance", distance.split(" ", 1))
            let total = parseFloat(prixVoiture) * parseFloat(distance.split(" ", 1))
            console.log(total.toFixed(2))
            this.setState({ prixTotal: total.toFixed(2) })
        }
    }
    _setDate = async (event, date) => {
        date = date
        if (date == undefined) {
            console.log("Event: ", "vide")
        }
        let test = false
        let action = { type: 'SHOW_DATE', value: test }
        await this.props.dispatch(action)
        if (date != undefined) {
            let dateDay = date.getDate()
            let dateMonth = date.getMonth()
            let dateYears = date.getFullYear()
            let datenow = new Date().setHours(0, 0, 0, 0)
            let dateSelected = new Date(date).setHours(0, 0, 0, 0)
            if (dateSelected < datenow) {
                Alert.alert("Information !", "Vous avez choisi une date passée. Veuillez corriger.")
                let _action = { type: 'SELECTION_DATE', value: "" }
                await this.props.dispatch(_action)
            }
            else {
                let _action = { type: 'SELECTION_DATE', value: dateDay + "/" + dateMonth + "/" + dateYears }
                await this.props.dispatch(_action)
            }
        }


    }
    _setHeure = async (event, heure) => {
        heure = heure
        if (heure == undefined) {
            console.log("Event: ", "vide")
        }
        let test = false
        let action = { type: 'SHOW_HEURE', value: test }
        await this.props.dispatch(action)
        if (heure != undefined) {
            let heureH = heure.getHours()
            let heureM = heure.getMinutes()
            let _action = { type: 'SELECTION_HEURE', value: heureH + "h:" + heureM + "mn" }
            await this.props.dispatch(_action)
        }

    }
    _clickOptionVoiture = (value) => {
        this.setState({
            optionVoiture: value
        })
    }
    _clickOptionPromo = () => {

        this.setState({
            optionVoiture: false,
            showModalCode: true,
        })
    }
    clickCloseModalCode = () => {
        this.setState({ showModalCode: false })
    }
    clickCloseModalVoiture = () => {
        this.setState({ modalChoixVoiture: false })
    }
    clickChoixVoiture = () => {
        this.setState({ modalChoixVoiture: false })
    }
    clickCloseModalValidation = () => {
        this.setState({ modalValidation: false })
    }
    _displayShowModalVoiture = () => {
        if (this.state.modalChoixVoiture) {
            return <ChoixVoiture clickChoixVoiture={this.clickChoixVoiture} clickCloseModalVoiture={this.clickCloseModalVoiture} />
        } else {
            return null
        }
    }
    _displayModalEntreeCode = () => {
        if (this.state.showModalCode) {
            return <ModalEntreeCode clickCloseModalCode={this.clickCloseModalCode} />
        } else {
            return null
        }
    }
    _displayValidation = () => {
        if (this.state.modalValidation) {
            return <Validation clickCloseModalValidation={this.clickCloseModalValidation} />
        } else {
            return null
        }
    }
    _onValid = async () => {
        this.setState({ loading: true })
        let idUser = this.props.userInfos.id_User
        let dateDepart = this.props.selectionDate
        let heure_minDepart = this.props.selectionHeure
        let latDepart = this.props.coordonateDepart.latitude
        let longDepart = this.props.coordonateDepart.longitude
        let depart = this.props.depart.description
        let arrivee = this.props.arriver.description
        let latArriver = this.props.coordonateArriver.latitude
        let longArriver = this.props.coordonateArriver.longitude
        let prix = this.state.prixTotal
        // let type_voiture_id = this.props.choixLocVoiture.id
        if (dateDepart == "") {
            this.setState({ loading: false })
            Alert.alert("Information", "Veuillez sélectionner la date")
        }
        else if (heure_minDepart == "") {
            this.setState({ loading: false })
            Alert.alert("Information", "Veuillez sélectionner l'heure")
        }
        else if (depart == "") {
            this.setState({ loading: false })
            Alert.alert("Information", "Veuillez sélectionner votre position de Départ")
        }
        else if (arrivee == "") {
            this.setState({ loading: false })
            Alert.alert("Information", "Veuillez sélectionner votre position de d'Arrivée")
        }
        else {
            let response = await ReservationHelpers.setReservation(idUser, dateDepart, heure_minDepart, latDepart, longDepart, depart, arrivee, latArriver, longArriver, prix)
            // console.log(response)
            if (response.success) {
                this.setState({ loading: false })
                Alert.alert("Information", "Votre réservation a été prise en compte. Veuillez confirmer votre réservation dans la liste.", [
                    {
                        text: "Confirmer la prise en charge ici",
                        onPress: () => {
                            this.props.navigation.navigate("MesreservationsPage")
                        }
                    }
                ],
                    { cancelable: false })
            }
            else {
                this.setState({ loading: false })
                Alert.alert("Information", "Une erreur s'est produite lors de votre réservation, veuillez réessayer")
            }
        }


    }
    _clickDepart = async () => {
        await this.props.navigation.navigate("LieuxPage", { optionChoix: "Départ" })
        // this._testDistance()

    }
    _clickArriver = async () => {
        await this.props.navigation.navigate("LieuxPage", { optionChoix: "Arriver" })
        // this._testDistance()

    }

    _clickLieuxDepart = (value) => {
        this.setState({ depart: value })
    }
    _clickLieux = (value) => {
        this.setState({ depart: value })
    }

    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                {this.state.loading ?
                    <View style={{ flex: 1, alignItems: "center", alignSelf: "center", justifyContent: "center" }}>
                        <Loading />
                    </View> :
                    <SafeAreaView>
                        <ScrollView contentContainerStyle={styles.scrollBody} contentInsetAdjustmentBehavior="automatic">
                            {
                                this.props.region == null ?
                                    <View style={styles.mapResa}></View>
                                    :
                                    <MapView
                                        initialRegion={{
                                            latitude: this.props.region.latitude,
                                            longitude: this.props.region.longitude,
                                            latitudeDelta: 0.0922,
                                            longitudeDelta: 0.0421,
                                        }}

                                        style={styles.mapResa}>
                                        {/* <MapViewDirections
                                        origin={this.props.coordonateDepart == null ? origin : this.props.coordonateDepart}
                                        destination={this.props.coordonateArriver == null ? destination : this.props.coordonateArriver.longitude}
                                        apikey={"AIzaSyBuvAMPr0k-IrxLWiwA9zF_j06zFUJLb3I"}
                                    /> */}
                                        <Marker
                                            coordinate={

                                                {
                                                    latitude: this.props.coordonateDepart == null ? this.props.region.latitude : this.props.coordonateDepart.latitude,
                                                    longitude: this.props.coordonateDepart == null ? this.props.region.longitude : this.props.coordonateDepart.longitude,
                                                }
                                            }
                                            title={"départ"}
                                        />
                                        <Marker
                                            coordinate={

                                                {
                                                    latitude: this.props.coordonateArriver == null ? this.props.region.latitude : this.props.coordonateArriver.latitude,
                                                    longitude: this.props.coordonateArriver == null ? this.props.region.longitude : this.props.coordonateArriver.longitude,
                                                }
                                            }
                                            title={"arrivée"}
                                        />

                                    </MapView>
                            }
                            <View style={styles.viewBtnPrec}>
                                <TouchableOpacity onPress={() => { this.props.navigation.pop() }} >
                                    <AutoHeightImage
                                        source={require('../../../assets/images/reservation/btnprecedent.png')}
                                        width={DimScreen.widthScreen * 0.08} />
                                </TouchableOpacity>
                            </View>

                            <View style={styles.bodyResa}>
                                <View style={styles.panelProximiter}>
                                    <View style={styles.durrationMn}>
                                        {this.state.duration == null ?
                                            <Text style={styles.textdurrationMn}> ... </Text> :
                                            <Text style={styles.textdurrationMn}>{this.state.duration}</Text>
                                        }
                                    </View>
                                    <View style={{ flex: 1 }}></View>
                                    <TouchableOpacity
                                        // onPress={()=> }
                                        style={styles.viewProximiter}>
                                        <AutoHeightImage
                                            source={require('../../../assets/images/reservation/mapImgRue.png')}
                                            width={DimScreen.widthScreen * 0.052}
                                            style={styles.imgmapImgRue} />
                                    </TouchableOpacity>
                                </View>
                                <View style={styles.panelboxRecherche}>
                                    <View style={styles.viewLieuxboxRecherche}>
                                        <View style={styles.viewLieuxbox1}>
                                            <TouchableOpacity onPress={() => { this._clickDepart() }}>
                                                <LinearGradient
                                                    colors={[colors.colorBtnStart, colors.colorBtnEnd]}
                                                    start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                                    style={styles.linearGradientInput}>
                                                    <AutoHeightImage
                                                        source={require('../../../assets/images/reservation/maps.png')}
                                                        width={DimScreen.widthScreen * 0.035} />
                                                    <Text numberOfLines={1} style={styles.textInput}   >{this.props.depart == null ? "Départ" : this.props.depart.description}</Text>
                                                </LinearGradient>
                                            </TouchableOpacity>
                                            <View style={{ margin: 5 }}></View>
                                            <TouchableOpacity onPress={() => { this._clickArriver() }}>
                                                <LinearGradient
                                                    colors={[colors.black, colors.black]}
                                                    start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                                    style={styles.linearGradientInputNoir}>
                                                    <AutoHeightImage
                                                        source={require('../../../assets/images/reservation/maps.png')}
                                                        width={DimScreen.widthScreen * 0.035} />
                                                    <Text numberOfLines={1} style={styles.textInput}>{this.props.arriver == null ? "Arrivée" : this.props.arriver.description}</Text>
                                                </LinearGradient>
                                            </TouchableOpacity>

                                        </View>
                                    </View>

                                    {/* <View style={styles.boxOptions}>
                                        <TouchableOpacity
                                            onPress={() => { this._clickOptionVoiture(true) }}
                                            style={[styles.btnOption1,
                                            {
                                                borderTopColor: this.state.optionVoiture == true ?
                                                    colors.vertBorderColor : colors.transparent
                                            }]}>
                                            <Text style={[styles.textOption1,
                                            {
                                                color: this.state.optionVoiture == true ?
                                                    colors.vertfonce : colors.gris1Font
                                            }]} >
                                                OPTIONS
                                                    </Text>
                                        </TouchableOpacity>
                                    </View> */}
                                    <TouchableOpacity
                                        style={{ margin: 10 }}
                                        onPress={async () => {
                                            let test = true
                                            let action = { type: 'SHOW_DATE', value: test }
                                            await this.props.dispatch(action)
                                        }
                                        }>
                                        {this.props.selectionDate == "" ?
                                            <View style={{ flexDirection: "row", width: "60%", justifyContent: "flex-start" }}>
                                                <AutoHeightImage
                                                    source={require("../../../assets/icones/calendar.png")}
                                                    width={DimScreen.widthScreen * 0.13} />
                                                <Text style={styles.textSelectModepaie}>Sélectionnez la date</Text>
                                                <View></View>
                                            </View> :
                                            <View style={{ flexDirection: "row", width: "60%", justifyContent: "flex-start" }}>
                                                <AutoHeightImage
                                                    source={require("../../../assets/icones/calendar.png")}
                                                    width={DimScreen.widthScreen * 0.13} />
                                                <Text style={styles.textSelectModepaie}>{this.props.selectionDate}</Text>
                                            </View>
                                            // <View style={{ width: DimScreen.widthScreen * 0.45 }}>
                                            //     <LinearGradient
                                            //         colors={[colors.colorBtnStart, colors.colorBtnEnd]}
                                            //         start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                            //         style={styles.linearGradienBerline}>
                                            //         <Text style={[styles.textSelectModepaie, { color: colors.white }]}>
                                            //             {"Date: " + this.props.selectionDate}
                                            //         </Text>
                                            //     </LinearGradient>
                                            // </View>
                                        }

                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={{ margin: 10 }}
                                        onPress={async () => {
                                            let test = true
                                            let actionH = { type: "SHOW_HEURE", value: test }
                                            await this.props.dispatch(actionH)
                                        }}
                                    >
                                        {this.props.selectionHeure == "" ?
                                            <View style={{ flexDirection: "row", width: "60%", justifyContent: "flex-start" }}>
                                                <AutoHeightImage
                                                    source={require("../../../assets/icones/time.png")}
                                                    width={DimScreen.widthScreen * 0.13} />
                                                <Text style={styles.textSelectModepaie}>Sélectionnez l'heure</Text>
                                            </View> :
                                            <View style={{ flexDirection: "row", width: "60%", justifyContent: "flex-start" }}>
                                            <AutoHeightImage
                                                source={require("../../../assets/icones/time.png")}
                                                width={DimScreen.widthScreen * 0.13} />
                                            <Text style={styles.textSelectModepaie}>{this.props.selectionHeure}</Text>
                                        </View>
                                            // <View style={[styles.panelviewBerline, { width: DimScreen.widthScreen * 0.45 }]}>
                                            //     <LinearGradient
                                            //         colors={[colors.colorBtnStart, colors.colorBtnEnd]}
                                            //         start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                            //         style={styles.linearGradienBerline}>
                                            //         <Text style={[styles.textSelectModepaie, { color: colors.white }]}>
                                            //             {"Heure: " + this.props.selectionHeure}
                                            //         </Text>
                                            //     </LinearGradient>
                                            // </View>
                                        }
                                        {/* <Text style={styles.textSelectModepaie}>
                                            {this.props.selectionHeure == "" ?
                                                "Sélectionnez l'heure" : "Heure: " + this.props.selectionHeure
                                            }
                                        </Text> */}
                                    </TouchableOpacity>
                                    {this.props.showDate ?
                                        <DateTimePicker value={this.state.date}
                                            mode={'date'}
                                            is24Hour={true}
                                            display="calendar"
                                            onChange={(event, date) => {
                                                this._setDate(event, date)
                                            }}
                                        /> : null
                                    }
                                    {this.props.showHeure ?
                                        <DateTimePicker value={this.state.heure}
                                            mode={'time'}
                                            is24Hour={true}
                                            display="clock"
                                            onChange={(event, heure) => this._setHeure(event, heure)}
                                        /> : null
                                    }
                                    <View style={[styles.panelviewBerline, {alignItems:"center", width:"100%" }]}>
                                        {/* <TouchableOpacity onPress={() => { this.setState({ modalChoixVoiture: true }) }}>
                                            <LinearGradient
                                                colors={[colors.colorBtnStart, colors.colorBtnEnd]}
                                                start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                                style={styles.linearGradienBerline}>
                                                <AutoHeightImage
                                                    source={{ uri: this.props.choixLocVoiture.image }}
                                                    width={DimScreen.widthScreen * 0.13} />
                                                <Text style={styles.textberline}>{this.props.choixLocVoiture.type}</Text>
                                            </LinearGradient>
                                        </TouchableOpacity> */}

                                        <View style={[{ flexDirection: "row", alignItems:"center", justifyContent:"center", alignSelf:"center" }]}>
                                            <Text style={[styles.labelPrix, { color: colors.vertfonce, fontWeight: "bold", fontSize:16 }]}>Prix: </Text>
                                            {this.state.duration == null ?
                                                <Text style={[styles.textPrix, { fontWeight: "bold", fontSize:16 }]}>2€</Text> :
                                                <Text style={[styles.textPrix, { fontWeight: "bold", fontSize:16 }]}>{this.state.prixTotal} €</Text>
                                            }
                                        </View>
                                    </View>
                                    <TouchableOpacity
                                        style={[styles.btnValider,{alignSelf:"center"}]}
                                        onPress={() => {
                                            this._onValid()
                                            // this.setState({ modalValidation: true }) 
                                        }}>
                                        <LinearGradient
                                            colors={[colors.colorBtnStart, colors.colorBtnEnd]}
                                            start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                            style={styles.linearGradienBerlineValider}>
                                            <Text style={styles.textValider} >{"Valider".toUpperCase()}</Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {this._displayShowModalVoiture()}
                            {this._displayValidation()}
                            {this._displayModalEntreeCode()}

                        </ScrollView>
                    </SafeAreaView>
                }
            </Fragment >)
    }
}

const mapStateProps = (state) => {
    return {
        arriver: state.arriver,
        depart: state.depart,
        choixLocVoiture: state.choixLocVoiture,
        region: state.region,
        coordonateDepart: state.coordonateDepart,
        coordonateArriver: state.coordonateArriver,
        userInfos: state.userInfos,
        showDate: state.showDate,
        showHeure: state.showHeure,
        selectionDate: state.selectionDate,
        selectionHeure: state.selectionHeure
    }
}
export default connect(mapStateProps)(ReservationOption1);