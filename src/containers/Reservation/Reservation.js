import React, { Component, Fragment } from 'react'
import {
    SafeAreaView, StatusBar, View, Text, ScrollView, ActivityIndicator,
    TouchableOpacity, TouchableWithoutFeedback, TextInput,Alert
} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image'
import MapView, { AnimatedRegion, Animated, Marker } from 'react-native-maps';

import LinearGradient from 'react-native-linear-gradient'
import colors from "../../configs/colors"
import { connect } from 'react-redux'
import HeaderAuthen from "../../components/HeaderAuthen/HeaderAuthen"
import styles from "./styles"
import DimScreen from '../../configs/DimScreen';
class Reservation extends Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props)
        this.state = {
            region: null,
            coordinate: null
        }
    }
    /** Show Slider barre */
    _clickShowSLider = () => {
        this.props.navigation.openDrawer()
    }
    _clickDepart = async () => {
        await this.props.navigation.navigate("LieuxPage", { optionChoix: "Départ" })
    }
    render() {
        return (
            <Fragment>
                <StatusBar barStyle="dark-content" />
                <SafeAreaView>
                    <ScrollView contentContainerStyle={styles.scrollAuthloading} contentInsetAdjustmentBehavior="automatic">
                        <View style={styles.container}>
                            {/* <HeaderAuthen _clickShowSLider={this._clickShowSLider} style={{zIndex:10}}/> */}
                            {
                                this.props.region == null ? null
                                    :
                                    <MapView
                                        initialRegion={{
                                            latitude: this.props.region.latitude,
                                            longitude: this.props.region.longitude,
                                            latitudeDelta: 0.0922,
                                            longitudeDelta: 0.0421,
                                        }}

                                        style={styles.mapResa}>
                                        <Marker coordinate={
                                            {
                                                latitude: this.props.region.latitude,
                                                longitude: this.props.region.longitude
                                            }
                                        } />
                                        {/* <MapView.Marker.Animated
                                            ref={marker => { this.marker = marker }}
                                            region={
                                                {
                                                    latitude: this.props.region.latitude,
                                                    longitude: this.props.region.longitude
                                                }
                                            }
                                        /> */}
                                    </MapView>
                            }
                            <HeaderAuthen _clickShowSLider={this._clickShowSLider} style={{ zIndex: 5, opacity: 0.5 }} />
                            <View style={styles.viewInputRechRue} >
                                <TouchableOpacity style={{ flex: 1, flexDirection: "row", alignItems: "center", }} onPress={() => { this._clickDepart() }}>
                                    <AutoHeightImage
                                        source={require('../../assets/images/reservation/mapImgRue.png')}
                                        width={DimScreen.widthScreen * 0.06}
                                        style={styles.imgmapImgRue}
                                    />
                                    <Text numberOfLines={1} style={styles.textInputRue} >{this.props.depart == null ? "Départ" : this.props.depart.description}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.panelResa}>
                                <TouchableOpacity
                                    style={[styles.btnLogin, { alignItems: "center", justifyContent: "space-around" }]}
                                    onPress={() => {
                                        if (this.props.userInfos == "") {
                                            Alert.alert("Information !", "Vous devez vous connecter avant de poursuivre.")
                                            this.props.navigation.navigate("LoginPage")
                                        }
                                        else{
                                            this.props.navigation.navigate("ReservationOption1Page")
                                        }
                                        
                                    }} >
                                    <LinearGradient
                                        colors={[colors.colorBtnStart, colors.colorBtnEnd]}
                                        start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                        style={styles.linearGradienLogin} >
                                        <Text style={styles.textbtnLogin}>{"Réserver".toUpperCase()}</Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                                {/* <View style={styles.panelBarrOU}>
                                    <View style={styles.barrOU}></View>
                                    <Text style={styles.textOU} >OU</Text>
                                    <View style={styles.barrOU}></View>
                                </View> */}
                            </View>

                            <View style={styles.panelFooterResa}>
                                <LinearGradient
                                    colors={[colors.colorFooterResaStart, colors.colorFooterResaEnd]}
                                    start={{ x: 0.0, y: 1.0 }} end={{ x: 1.0, y: 1.0 }}
                                    style={styles.linearGradienResa} />
                            </View>

                        </View>
                    </ScrollView>
                </SafeAreaView>
            </Fragment>
        );
    }
}

const mapStateProps = (state) => {
    return {
        depart: state.depart,
        region: state.region,
        userInfos: state.userInfos,
    }
}
export default connect(mapStateProps)(Reservation);