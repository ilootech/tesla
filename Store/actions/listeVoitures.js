const listeVoitures = [
    {
        id: 1,
        nom_voiture: "BÉRLINE",
        nombrePlaces:5,
        prix:"55.00",
        imgVoiture:require("../../src/assets/images/reservation/berline.png")
    },
    {
        id: 2,
        nom_voiture: "MONOSPACE",
        nombrePlaces:5,
        prix:"60.00",
        imgVoiture:require("../../src/assets/images/reservation/monospace-hd.png")
    },
    {
        id: 3,
        nom_voiture: "VAN",
        nombrePlaces:5,
        prix:"75.00",
        imgVoiture:require("../../src/assets/images/reservation/van-hd.png")
    }
]

export default listeVoitures