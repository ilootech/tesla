const initialeState =
{
    userInfos: "",
    menueActif: "newResa",
    userSession: "",
    arriver: null,
    depart: null,
    listeVoitures: "",
    choixLocVoiture: "",
    listTypeVoiture: [],
    region: null,
    coordonateDepart: null,
    coordonateArriver: null,
    showDate: false,
    selectionDate: "",
    showHeure: false,
    selectionHeure: "",
    dataListReservation:[],
    dataListReservationTerminer:[]

}
Reduction = (state = initialeState, action) => {
    let nextState;
    switch (action.type) {
        case 'INIT':
            nextState = {
                ...state,
                choixLocVoiture: "",
                userSession: "",
                userInfos: ""
            }
            return nextState
        /**
         * ADD REGION 
        **/
        case 'ADD_REGION':
            nextState = {
                ...state,
                region: action.value
            }
            return nextState
        /**
         * ADD COORDONATE DEPART
        **/
        case 'ADD_COORDONATE_DEPART':
            nextState = {
                ...state,
                coordonateDepart: action.value
            }
            return nextState
        /**
         * ADD COORDONATE ARRIVER 
        **/
        case 'ADD_COORDONATE_ARRIVER':
            nextState = {
                ...state,
                coordonateArriver: action.value
            }
            return nextState

        //    region
        case 'ADD_MENUE_ACTIF':
            nextState = {
                ...state,
                menueActif: action.value
            }
            return nextState

        case 'ADD_CHOIXVOITURE':
            nextState = {
                ...state,
                choixLocVoiture: action.value
            }
            return nextState
        case 'ADD_TYPEVOITURES':
            nextState = {
                ...state,
                listTypeVoiture: action.value
            }
            return nextState


        //user sessio 
        case 'FOOTER':
            nextState = {
                ...state,
                userSession: action.value
            }
            return nextState
        case 'ADD_ARRIVEE':
            nextState = {
                ...state,
                arriver: action.value
            }
            return nextState
        case 'ADD_DEPART':
            nextState = {
                ...state,
                depart: action.value
            }
            return nextState
        case 'ADD_USERCONNECT':
            nextState = {
                ...state,
                userInfos: action.value
            }
            return nextState
        case 'SHOW_DATE':
            nextState = {
                ...state,
                showDate: action.value
            }
            return nextState
        case 'SHOW_HEURE':
            nextState = {
                ...state,
                showHeure: action.value
            }
            return nextState
        case 'SELECTION_DATE':
            nextState = {
                ...state,
                selectionDate: action.value
            }
            return nextState
        case 'SELECTION_HEURE':
            nextState = {
                ...state,
                selectionHeure: action.value
            }
            return nextState
            case 'RESERVATION':
            nextState = {
                ...state,
                dataListReservation: action.value
            }
            return nextState
            case 'RESERVATION_TERMINER':
            nextState = {
                ...state,
                dataListReservationTerminer: action.value
            }
            return nextState

        default:
            return state
    }
}
export default Reduction;