import {createStore} from 'redux';
import Reduction from './Reducers/Reducers';

export default createStore(Reduction);