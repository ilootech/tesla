import React, { Component } from 'react';
import { createAppContainer } from "react-navigation";
import { Platform, StyleSheet, Text, View } from 'react-native';

import AppNavigator from './src/navigations/index';


import { Provider } from 'react-redux';
import ConfigureStore from './Store/ConfigureStore';
const AppContainer = createAppContainer(AppNavigator);
export default class App extends Component {
  render() {
    return (
      <Provider store={ConfigureStore}>
        <AppContainer />
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
